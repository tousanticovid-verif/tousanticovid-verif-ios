//
//  VerifierResult.swift
//  Anticovid Verify
//
//

import Foundation

struct VerifierResult {
    var certificateState: CertificateState
    var statInformation: StatInformation
    var certHash: String
}

struct StatInformation {
    var globalValidity: DataTypeValidation
    var controlType: String
    var ressourceType: String
    var result: String = ""
    var resultInfo: String = ""
}
