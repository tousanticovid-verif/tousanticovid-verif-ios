//
//  OnlineStatData.swift
//  Anticovid Verify
//
//

import Foundation

struct OnlineStatData {
    var globalValidity: DataTypeValidation
    var controlType: String
    var certificateAdminState: String = ""
    var siren: String = PremiumManager.shared.siren
    var ressourceType: String
    var msgException: String = ""
    var result: String
    var resultInfo: String
    
    func toString() -> String {
        let date = Date().roundingToBeginningOfDay() ?? Date()
        var stat = date.isoString
        stat.append(ConstStat.SEPARATOR)
        stat.append(globalValidity.rawValue)
        stat.append(ConstStat.SEPARATOR)
        stat.append(certificateAdminState)
        stat.append(ConstStat.SEPARATOR)
        stat.append(ConstStat.SEPARATOR)
        stat.append(controlType)
        stat.append(ConstStat.SEPARATOR)
        stat.append(siren)
        stat.append(ConstStat.SEPARATOR)
        stat.append(ConstStat.SEPARATOR)
        stat.append(ConstStat.SEPARATOR)
        stat.append(ConstStat.SEPARATOR)
        stat.append(ressourceType)
        stat.append(ConstStat.SEPARATOR)
        stat.append(msgException)
        stat.append(ConstStat.SEPARATOR)
        stat.append(result)
        stat.append(ConstStat.SEPARATOR)
        stat.append(resultInfo)
        return stat
    }
}
