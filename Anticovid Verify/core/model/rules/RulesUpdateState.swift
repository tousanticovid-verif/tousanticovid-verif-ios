//
//  RulesUpdateState.swift
//  Anticovid Verify
//

import Foundation

struct RulesUpdateState {
    let rulesToAdd: [RuleBaseResponse]
}
