//
//  ValidationConfig.swift
//  Anticovid Verify
//
//

import Foundation

enum CertConfigMode {
    case goToFrance(config: CertGoToFranceConfigMode), getOutOfFrance
}

enum CertGoToFranceConfigMode {
    case national, international
}

struct ValidationConfig {
    let date: Date
    let mode: CertConfigMode
    let destinations: [String]
    
    func getDestinationCountryCode() -> [String] {
        switch mode {
        case .getOutOfFrance:
            return destinations
        case .goToFrance(let config):
            switch config {
            case .national:
                return ["1A", "GU", ""]
            case .international:
                return ["GR", "G*", "OR", "O*"]
//            case .none:
//                return []
            }
        }
    }
}
