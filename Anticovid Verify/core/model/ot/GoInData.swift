//
//  GoInData.swift
//  Anticovid Verify
//
//

import Foundation

struct GoInData: Codable {
    
    var destinationCountry: Country
    var favoritesCountries: [Country]
    var currentCheckDate: DateState
    
    init(destinationCountry: Country, favoritesCountries: [Country], currentCheckDate: DateState) {
        self.destinationCountry = destinationCountry
        self.currentCheckDate = currentCheckDate
        self.favoritesCountries = favoritesCountries
    }
}
