//
//  FileLogger.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit


class FileLogger {
    public static let shared = FileLogger()

    let file = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).last?.appendingPathComponent("INLog.txt", isDirectory: false)
    
    func write(text: String) {
        DispatchQueue.main.async { [self] in
            if ConstConf.filelogEnabled {
                guard let logFile = file else { return }
                let textLog = "\n" + text
                if FileManager.default.fileExists(atPath:logFile.path) {
                    let fileHandle = try? FileHandle(forWritingTo: logFile)
                    fileHandle?.seekToEndOfFile()
                    fileHandle?.write(textLog.data(using: .utf8)!)
                    fileHandle?.closeFile()
                }
                else {
                    try? text.data(using: .utf8)?.write(to: logFile)
                }
            }
        }
    }
    
    func read() -> String {
        if ConstConf.filelogEnabled {
            guard let logFile = file else { return "" }
            guard let value = try? String(contentsOfFile: logFile.path) else { return ""}
            return value
        }
        return ""
    }
    
    func send(from : UIViewController){
        if ConstConf.filelogEnabled {
            let textToShare = [ read() ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = from.view // so that iPads won't crash
            from.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    func clean(){
        if ConstConf.filelogEnabled {
            try? FileManager.default.removeItem(atPath: file!.path)
        }
    }
}
