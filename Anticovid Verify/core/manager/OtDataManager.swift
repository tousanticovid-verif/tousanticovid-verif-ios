//
//  OtDataManager.swift
//  Anticovid Verify
//

import Foundation
import OSLog

enum OtModeSelection {
    case departure(data: GoOutData) // go out
    case arrival(data: GoInData) // go in
}

class OtDataManager {
    
    public static let shared = OtDataManager()
    
    let destinationCountryKey = "destinationCountry"
    let favoritesCountriesKey = "favoritesCountries"
    let departureCountryKey = "departureCountry"
    let currentCheckDateKey = "currentCheckDate"
    let goOutDataKey = "goOutData"
    let goInDataKey = "goInData"
    let selectCountriesKey = "selectCountries"
    let otStateKey = "otState"
     
    var selection: OtModeSelection {
        get {
            switch state { // departure
            case .goOutState:
                return .departure(data: goOutData)
            case .goInState: // arrival
                return .arrival(data: goInData)
            }
        }
    }
    
    var goOutDataDefault = GoOutData(
        departureCountry: Country(name: "", nameCode: "", isNational: true),
        favoritesCountries: [],
        currentCheckDate: .actual
    )
    
    var goOutData: GoOutData {
        get {
            if let goOutDataResult = UserDefaults.standard.object(forKey: goOutDataKey) as? Data,
               let goInData = try? JSONDecoder().decode(GoOutData.self, from: goOutDataResult) {
                return goInData
            }
            return goOutDataDefault
        }
        
        set(value) {
            if let encoded = try? JSONEncoder().encode(value) {
                UserDefaults.standard.set(encoded, forKey: goOutDataKey)
            }
        }
    }
    
    var goInDataDefault = GoInData(
        destinationCountry: Country(name: "", nameCode: "", isNational: true),
        favoritesCountries: [],
        currentCheckDate: .actual
    )
    
    var goInData: GoInData {
        get {
            if let goInDataResult = UserDefaults.standard.object(forKey: goInDataKey) as? Data,
               let goInData = try? JSONDecoder().decode(GoInData.self, from: goInDataResult){
                return goInData
            }
            return goInDataDefault
        }
        
        set(value) {
            do {
                try UserDefaults.standard.setObject(value, forKey: goInDataKey)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    var selectCountries: [Country]? {
        get {
            let selectCountries: [Country] = [Country]()
            do {
                let selectCountries = try UserDefaults.standard.getObject(forKey: selectCountriesKey, castTo: [Country].self)
                return selectCountries.sorted { $0.name.lowercased() < $1.name.lowercased() }
            } catch {
                print(error.localizedDescription)
            }
            
            return selectCountries
        }
        set(value) {
            do {
                try UserDefaults.standard.setObject(value, forKey: selectCountriesKey)
                UserDefaults.standard.synchronize()
            } catch {
                print(error.localizedDescription)
            }
        }
    }


    var state : OtState {
        get {
            UserDefaults.standard.bool(forKey: otStateKey) ? OtState.goOutState : OtState.goInState
        }
        set {
            UserDefaults.standard.setValue(newValue == .goOutState, forKey: otStateKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    func save(goInData: GoInData?, goOutData: GoOutData?) {
        if let outData = goOutData{
            self.goOutData = outData
        }
        if let inData = goInData{
            self.goInData = inData
        }
    }
}
