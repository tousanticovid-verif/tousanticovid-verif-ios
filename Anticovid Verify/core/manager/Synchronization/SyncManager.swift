//
//  SynchronisationManager.swift
//  Anticovid Verify
//

import Foundation
import INCore
import OSLog
import Combine

class SyncManager {
    public static let shared = SyncManager()
    
    lazy private var configSync = ConfigurationSynchronization()
    lazy private var blacklistSync = BlacklistSynchronization()
    lazy private var rulesSync = CertLogicRulesSynchronization()
    lazy private var valueSetsSync = CertLogicValueSetsSynchronization()
    lazy private var checkAppVersionSync = CheckAppVersionSynchronization()
    lazy private var statService = StatService.shared
    private(set) var isSyncingInProgress = false
    
    // If you provide new json files in Bundle, please update the date here
    var localSyncDate: Date {
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatter.date(from: "2022-02-28 10:00:00") ?? Date(timeIntervalSince1970: 0)
        }
    }
    
    /**
     Date at which the last sync was done
     */
    var lastSyncDate : Date? {
        get{
            let timeInSecondes = try? INPersistentData.getLong(forKey: CertificatesManager.certificatesLastUpdateKey,defaultValue: 0)
            guard let timeInsecs = timeInSecondes, timeInsecs > 0 else { return nil }
            return Date(timeIntervalSince1970: TimeInterval(timeInsecs))
        }
        set(value){
            try? INPersistentData.saveLong(key: CertificatesManager.certificatesLastUpdateKey, value: Int64(value?.timeIntervalSince1970 ?? 0))
        }
    }
    
    /**
     actualAppSyncTreshold provides more information about the actual synchronization state of the app with the RemindStep struct.
     Some actions are launched depending on this state.
     */
    func actualAppSyncRemindStep() -> RemindStep {
        if (CertificatesManager.shared.dccCertificates?.count ?? 0) < CertificatesManager.NUMBER_MIN_OF_CERTIFICATE {
            return .high
        } else if let limitDateStep1 = lastSyncDate?.dateByAddingHours(Int(ConfDataManager.shared.remindHoursStep1)),
                  let limitDateStep2 = lastSyncDate?.dateByAddingHours(Int(ConfDataManager.shared.remindHoursStep2)),
                  let limitDateStep3 = lastSyncDate?.dateByAddingHours(Int(ConfDataManager.shared.remindHoursStep3)) {
            
            if(limitDateStep1 < Date() && Date() < limitDateStep2) {
                return .low
            } else if(limitDateStep2 < Date() && Date() < limitDateStep3) {
                return .medium
            } else if(limitDateStep3 < Date()) {
                return .high
            }
        }
        return .none
    }
    
    /**
     shouldSyncApp returns true when the app should be synced (minimum refresh rate to launch requests)
     */
    func shouldSyncApp() -> Bool {
        guard let limitDate = lastSyncDate?.addingTimeInterval(60*60*ConfDataManager.shared.remindFrequency),
              (CertificatesManager.shared.dccCertificates?.count ?? 0) >= CertificatesManager.NUMBER_MIN_OF_CERTIFICATE else {
                  return true
              }
        return limitDate < Date()
    }
    
    
    func shouldStartLocalSync() -> Bool {
        guard let lastSyncDate = lastSyncDate else {
            return true
        }
        
        return !UserDataManager.sharedInstance.hasMigrateToCriticalVersion || localSyncDate > lastSyncDate
    }
    
    /**
     Start to synchronize all functionnalities if needed.
     - Parameter force: True to synchronize only if needed, else forces synchronization
     */
    func start(force: Bool = false) -> AnyPublisher<Void, Error> {
        FileLogger.shared.write(text: "\n" + Date().dateTimeString + " start sync automatically \(!force)")
        
        guard (shouldSyncApp() || force) else {
            FileLogger.shared.write(text: Date().dateTimeString + "sync not needed")
            return Fail(error: SyncError.noNeedToSync).eraseToAnyPublisher()
        }
        
        let isPremium = PremiumManager.shared.isPremium
        
        let text = isPremium ? "true" : "false"
        FileLogger.shared.write(text: Date().dateTimeString + " is premium " + text )
        
        let toSync: [SyncingFunctionnality] = isPremium ?
        [.baseConfiguration(chainWith: .blacklist(chainWith: .checkAppVersion(chainWith: .OTModeConfiguration(chainWith: nil)), force: force))] :
        [.baseConfiguration(chainWith: .blacklist(chainWith: .checkAppVersion(chainWith: nil)))]
        
        return start(toSync: toSync)
    }
    
    /**
     Start local sync. Synchronize all functionnalities from local resources.
     */
    func startLocal() -> AnyPublisher<Void, Error> {
        os_log("%@",type: .info, "Sync Local start \(Date().dateTimeString)")
        
        RulesMotorRepository.shared.deletePreviousRules()
        try? CertLogicMotorRepository.shared.deleteRules()
        try? CertLogicMotorRepository.shared.deleteValueSets()
        
        FileLogger.shared.write(text: "\nSync Local start")
        if let response = CertificatesManager.shared.loadLocalCertificates() {
            CertificatesManager.shared.saveCertificates(certs: response)
            try? RulesService.shared.save(specificValues: response.specificValues)
            UIDataManager.shared.save(specificValues: response.specificValues)
            ConfDataManager.shared.save(specificValues: response.specificValues)
            ConfDataManager.shared.save(remindData: response)
            
            VaccinePassVerifierService.shared.configuration = VaccinePassVerifierService.shared.rulesMotorService.vaccinePassVerifierConfiguration() ?? ConstRules.vaccinePassValidity
            HealthPassVerifierService.shared.configuration = HealthPassVerifierService.shared.rulesMotorService.healthPassVerifierConfiguration() ?? ConstRules.healthPassValidity
            DetailedResultVerifierService.shared.configuration = DetailedResultVerifierService.shared.rulesMotorService.plusValidityConfiguration() ?? ConstRules.plusValidity
        }
        
        if let responseRuleBases = CertLogicMotorManager.shared.loadLocalRulesId() {
            try? _ = CertLogicMotorManager.shared.handleRuleBasesResponse(responseRuleBases)
            
            if let responseRuleDetails = CertLogicMotorManager.shared.loadLocalRules() {
                try? CertLogicMotorManager.shared.handleRulesResponse(responseRuleDetails, withIds: responseRuleBases)
            }
        }
        
        if let responseValueSetBases = CertLogicMotorManager.shared.loadLocalValueSetId() {
            try? _ = CertLogicMotorManager.shared.handleValueSetBasesResponse(responseValueSetBases)
            
            if let responseValueSetsDetails = CertLogicMotorManager.shared.loadLocalValueSet() {
                try? CertLogicMotorManager.shared.handleValueSetsResponse(responseValueSetsDetails, withIds: responseValueSetBases)
            }
        }
        
        try? CountriesManager.shared.loadLocalCountriesFromResourcesAndRules()
        
        if let response = CertLogicMotorManager.shared.loadLocalBL2DDoc() {
            try? _ = blacklistSync.handleBlackList2DDOCResponse(response)
        }
        if let response = CertLogicMotorManager.shared.loadLocalBLDCC() {
            try? _ = blacklistSync.handleBlackListDCCResponse(response)
        }
        
        FileLogger.shared.write(text: "Sync Local End")
        
        lastSyncDate = localSyncDate
        UserDataManager.sharedInstance.hasMigrateToCriticalVersion = true
        
        os_log("%@",type: .info, "Sync local end \(Date().dateTimeString)")
        
        return Just(())
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}

private extension SyncManager {
    /**
     Start to synchronize some functionnalities.
     - Parameter toSync: Array of functionnalities to synchronize
     */
    func start(toSync: [SyncingFunctionnality]) -> AnyPublisher<Void, Error> {
        guard !isSyncingInProgress else {
            return Fail(error: SyncError.syncAlreadyInProgress).eraseToAnyPublisher()
        }
        guard !toSync.isEmpty else {
            return Fail(error: SyncError.unknown).eraseToAnyPublisher()
        }
        
        isSyncingInProgress = true
        
        // Creates publisher for each functionnality to sync
        // Then zip them together
        var publisher: AnyPublisher<Void, Error>!
        publisher = toSync
            .map { start(toSync: $0) }
            .reduce(Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()) { partialResult, newPublisher in
                Publishers.Zip(partialResult, newPublisher)
                    .map { _ in () }
                    .eraseToAnyPublisher()
            }
        
        return publisher
            .tryCatch { [weak self] error -> AnyPublisher<Void, Error>  in
                self?.isSyncingInProgress = false
                return Fail(error: error).eraseToAnyPublisher()
            }
            .map { [weak self] error -> Void in
                self?.isSyncingInProgress = false
                self?.lastSyncDate = Date()
                
                UserDataManager.sharedInstance.previousAppVersionForSync = ConstConf.version
                UserDataManager.sharedInstance.syncBannerViewMaskedDate = nil
                
                if !UserDataManager.sharedInstance.hasMigrateToCriticalVersion {
                    UserDataManager.sharedInstance.hasMigrateToCriticalVersion = true
                }
                NotificationCenter.default.post(name: ConstNotification.updateBannerSyncApp, object: nil)
                
                return ()
            }
            .eraseToAnyPublisher()
    }
    
    func start(toSync: SyncingFunctionnality) -> AnyPublisher<Void, Error> {
        os_log("%@",type: .info, "Sync of \(toSync) started \(Date().dateTimeString)")
        
        var publisher: AnyPublisher<Void, Error>
        var toChain: SyncingFunctionnality?
        
        switch toSync {
        case .baseConfiguration(let chained):
            toChain = chained
            publisher = configSync.fetchConfiguration()
        case .blacklist(let chained, let force):
            toChain = chained
            publisher = Publishers.Zip(blacklistSync.fetchBlacklistDCC(force: force),
                                       blacklistSync.fetchBlacklist2DDOC(force: force))
                .map { _ in () }
                .eraseToAnyPublisher()
        case .OTModeConfiguration(let chained):
            toChain = chained
            publisher = Publishers.Zip(rulesSync.synchronizeRules(),
                                       valueSetsSync.synchronizeValueSets())
                .map { _ in () }
                .eraseToAnyPublisher()
        case .checkAppVersion(let chained):
            toChain = chained
            publisher = checkAppVersionSync.synchronize()
            
        case .stats(let chained):
            toChain = chained
            publisher = statService.sendStat()
                .catch { _ -> AnyPublisher<Void, Error> in
                    Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
                .eraseToAnyPublisher()
        }
        
        if let toChain = toChain {
            publisher = Publishers.Concatenate(prefix: publisher, suffix: start(toSync: toChain))
                .eraseToAnyPublisher()
        }
        
        return publisher
            .map { _ in
                os_log("%@",type: .info, "Sync of \(toSync) ended \(Date().dateTimeString)")
            }
            .eraseToAnyPublisher()
    }
}
