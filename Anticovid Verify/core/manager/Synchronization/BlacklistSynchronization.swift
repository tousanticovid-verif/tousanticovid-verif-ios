//
//  BlacklistSynchronization.swift
//  Anticovid Verify
//


import Foundation
import INCommunication
import INCore
import os.log
import Combine

struct BlacklistService {
    let routerService: ServiceRouter
    let index: Int32
    let isDCC: Bool
    
    init(index: Int32, isDCC: Bool) {
        self.index = index
        self.isDCC = isDCC
        self.routerService = isDCC ? .blacklistDCCHashes(startIndex: index) : .blacklist2DDOCHashes(startIndex: index)
    }
}

class BlacklistSynchronization {
    typealias FetchStartIndex = Int32?
    
    private let blacklistRepository: BlackListRepository
    private var cancellables = Set<AnyCancellable>()
    
    init(blacklistRepository: BlackListRepository = .shared,
         userDataManager: UserDataManager = .sharedInstance) {
        self.blacklistRepository = blacklistRepository
    }
    
    func fetchBlacklist2DDOC(force: Bool = false) -> AnyPublisher<Void, Error> {
        let startIndex = blacklistRepository.lastBlacklist2DDOCIndex()
        FileLogger.shared.write(text: "FetchBlacklist2DDOC from \(startIndex) to \(blacklistRepository.configuration?.lastIndexBlacklist2DDOC ?? -1)")
        if let configIndex = blacklistRepository.configuration?.lastIndexBlacklist2DDOC {
            // blacklist already updated
            guard configIndex != startIndex else {
                return Just(())
                    .setFailureType(to: Error.self)
                    .eraseToAnyPublisher()
            }
        }

        let service = BlacklistService(index: startIndex, isDCC: false)
        
        return fetchBlacklist(service: service, force: force)
            .map { _ in () }
            .tryCatch { error -> AnyPublisher<Void, Error> in
                if let error = error as? SyncError,
                   error == .cannotFetchBlacklistAtCurrentTime {
                    return Just(())
                        .setFailureType(to: Error.self)
                        .eraseToAnyPublisher()
                }
                
                return Fail(error: error)
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func fetchBlacklistDCC(force: Bool = false) -> AnyPublisher<Void, Error> {
        let startIndex = blacklistRepository.lastBlacklistDCCIndex()
        FileLogger.shared.write(text: "FetchBlacklistDCC from \(startIndex) to \(blacklistRepository.configuration?.lastIndexBlacklistDCC ?? -1)")
        if let configIndex = blacklistRepository.configuration?.lastIndexBlacklistDCC {
            // blacklist already updated
            guard configIndex != startIndex else {
                return Just(())
                    .setFailureType(to: Error.self)
                    .eraseToAnyPublisher()
            }
        }
        
        let service = BlacklistService(index: startIndex, isDCC: true)

        return fetchBlacklist(service: service, force: force)
            .map { _ in () }
            .tryCatch { error -> AnyPublisher<Void, Error> in
                if let error = error as? SyncError,
                   error == .cannotFetchBlacklistAtCurrentTime {
                    return Just(())
                        .setFailureType(to: Error.self)
                        .eraseToAnyPublisher()
                }
                
                return Fail(error: error)
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}

 extension BlacklistSynchronization {
    private func fetchBlacklist(service: BlacklistService, force: Bool) -> AnyPublisher<Void,Error> {
        guard canFetchBlacklist(fromIndex: service.index, isDCC: service.isDCC) || force else {
            return Fail(error: SyncError.cannotFetchBlacklistAtCurrentTime).eraseToAnyPublisher()
        }
        
        return INRequestService.shared.executeRequest(service.routerService, type: BlacklistResponse.self,
                                                          timeout: 10, pinCertificates: service.routerService.getPinningCerts())
            .tryMap { [weak self] response -> FetchStartIndex in
                return service.isDCC ?
                try self?.handleBlackListDCCResponse(response) :
                try self?.handleBlackList2DDOCResponse(response)
            }
            .flatMap { [weak self] index -> AnyPublisher<Void,Error> in
                guard let self = self else {
                    return Fail(error: SyncError.referenceLost).eraseToAnyPublisher()
                }
                // Stops when no more blacklist to fetch
                guard let index = index else {
                    return Just(()).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
               
                // Recursive part
                return self.fetchBlacklist(service: BlacklistService(index: index, isDCC: service.isDCC), force: force)
            }
            .eraseToAnyPublisher()
    }
    
    /**
     Handle DCC response.
     Nil result indicates that there is no more blacklist to fetch
     Non nil result indicates the new index from which you should start to fetch.
     */
    public func handleBlackListDCCResponse(_ response: BlacklistResponse) throws -> FetchStartIndex {
        guard let last = response.elements.last else { return nil }
        FileLogger.shared.write(text: "blacklistDCCResponse \(response.elements.count)")
        try blacklistRepository.save(blacklistDCCResponse: response.elements)
        
        return response.lastIndexBlacklist == last.id ? nil : last.id + 1
    }
    
    /**
     Handle 2DDOC response.
     Nil result indicates that there is no more blacklist to fetch
     Non nil result indicates the new index from which you should start to fetch.
     */
    public func handleBlackList2DDOCResponse(_ response: BlacklistResponse) throws -> FetchStartIndex {
        guard let last = response.elements.last else { return nil }
        
        try blacklistRepository.save(blacklist2DDOCResponse: response.elements)
        FileLogger.shared.write(text: "blacklist2DDOCResponse \(response.elements.count)")
        return response.lastIndexBlacklist == last.id ? nil : last.id + 1
    }
    
    private func canFetchBlacklist(fromIndex startIndex: Int32, isDCC: Bool) -> Bool {
        guard !canFetchBlacklistWithQuantityRemaining(fromIndex: startIndex, isDCC: isDCC) else {
            return true
        }
        
        return canFetchBlacklistAtCurrentTime()
    }
    
    private func canFetchBlacklistWithQuantityRemaining(fromIndex startIndex: Int32, isDCC: Bool) -> Bool {
        guard let maxIndex = isDCC ? blacklistRepository.configuration?.lastIndexBlacklistDCC :
                blacklistRepository.configuration?.lastIndexBlacklist2DDOC,
              let exclusionQuantityLimit = blacklistRepository.configuration?.exclusionQuantityLimit else {
                  FileLogger.shared.write(text: "Missing config value, force blacklist sync")
                  return true
              }
        let canFetch = maxIndex - startIndex >= exclusionQuantityLimit
        FileLogger.shared.write(text: "last Index is \(startIndex), max \(maxIndex), the limit \(exclusionQuantityLimit). Limit reach : \(canFetch)")
        return canFetch
    }
    
    private func canFetchBlacklistAtCurrentTime() -> Bool {
        let exclusionTimes = (blacklistRepository.configuration?.exclusionTimes?.allObjects as? [ExclusionTime] ?? [])
        let now = Date.timeFormatter.date(from: Date.timeFormatter.string(from: Date()))
        let result = exclusionTimes.reduce(true) { partialResult, exclusionTime in
            let res1 = Calendar.current.compare(exclusionTime.start!, to: now!, toGranularity: .minute)
            let res2 = Calendar.current.compare(now!, to: exclusionTime.end!, toGranularity: .minute)
            
            return partialResult && !(res1 == .orderedAscending && res2 == .orderedAscending)
        }
        if !result {
            FileLogger.shared.write(text: "Sync in exclusion time, Blacklist won't be called")
        }
        return result
    }
}
