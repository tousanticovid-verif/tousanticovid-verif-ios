//
//  ConfigurationSynchronization.swift
//  Anticovid Verify
//

import Foundation
import INCommunication
import INCore
import os.log
import Combine

class ConfigurationSynchronization {
    private let blackListRepository: BlackListRepository
    private let statService: StatService
    private let rulesService: RulesService
    private let certificatesManager: CertificatesManager
    private let uiDataManager: UIDataManager
    private let confDataManager: ConfDataManager
    private let certLogicMotorManager: CertLogicMotorManager
    private let userDataManager: UserDataManager
    private let statsService: StatService
    
    init(certificatesManager: CertificatesManager = .shared,
         uiDataManager: UIDataManager = .shared,
         statService: StatService = .shared,
         rulesMotorService: RulesService = .shared,
         blackListRepository: BlackListRepository = .shared,
         confDataManager: ConfDataManager = .shared,
         certLogicMotorManager: CertLogicMotorManager = .shared,
         userDataManager: UserDataManager = .sharedInstance,
         statsService: StatService = .shared) {
        self.certificatesManager = certificatesManager
        self.uiDataManager = uiDataManager
        self.statService = statService
        self.rulesService = rulesMotorService
        self.confDataManager = confDataManager
        self.blackListRepository = blackListRepository
        self.certLogicMotorManager = certLogicMotorManager
        self.userDataManager = userDataManager
        self.statsService = statsService
    }
    
    func fetchConfiguration() -> AnyPublisher<Void, Error> {
        let service = ServiceRouter.updateCertificates
        
        return INRequestService.shared.executeRequest(service,
                                                          type: CertificatesUpdateResponse.self,
                                                          typeDateDecoderStrategy: .formatted(Date.dateFormatter),
                                                          timeout: 10,
                                                          pinCertificates: service.getPinningCerts())
            .tryMap { [unowned self] response in
                try self.handleConfigurationResponse(response)
            }
            .eraseToAnyPublisher()
    }
    
    private func handleConfigurationResponse(_ response: CertificatesUpdateResponse) throws {        
        certificatesManager.saveCertificates(certs: response)
        uiDataManager.save(specificValues: response.specificValues)
        confDataManager.save(specificValues: response.specificValues)
        confDataManager.save(remindData: response)
        
        try rulesService.save(specificValues: response.specificValues)
        try blackListRepository.save(configuration: response.specificValues.blacklist)
        
        VaccinePassVerifierService.shared.configuration = VaccinePassVerifierService.shared.rulesMotorService.vaccinePassVerifierConfiguration() ?? ConstRules.vaccinePassValidity
        HealthPassVerifierService.shared.configuration = HealthPassVerifierService.shared.rulesMotorService.healthPassVerifierConfiguration() ?? ConstRules.healthPassValidity
        DetailedResultVerifierService.shared.configuration = DetailedResultVerifierService.shared.rulesMotorService.plusValidityConfiguration() ?? ConstRules.plusValidity
    }
}
