//
//  INCommunication+extension.swift
//  Anticovid Verify
//

import Foundation
import INCore
import INCommunication
import Combine

extension INRequestService {
    func executeRequest<D: Decodable & Encodable>(_ requestConvertible: INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, headers: INHTTPHeaders? = nil, p12: INPKCS12? = nil, httpShouldSetCookies: Bool = true, timeout: Int? = 20, pinCertificates: [SecCertificate] = [SecCertificate]()) -> Future<D, Error> {
        Future { promise in
            let date = Date()
            let text = date.dateTimeString + " " + (requestConvertible.urlRequest?.url?.path ?? " unknown path ") + " start"
            FileLogger.shared.write(text: text)
            
            self.executeRequest(requestConvertible, type: type, typeDateDecoderStrategy: typeDateDecoderStrategy, headers: headers, p12: p12, httpShouldSetCookies: httpShouldSetCookies, timeout: timeout, pinCertificates: pinCertificates) { status, response, error, data in
                guard let response = response else {
                    var text = date.dateTimeString + " " + (requestConvertible.urlRequest?.url?.path ?? " unknown path ")
                    text += " end " + String(Date().timeIntervalSince(date)) + " " + (error?.localizedDescription ?? "unknown error")
                    FileLogger.shared.write(text: text)
                    promise(.failure(error ?? SyncError.unknown))
                    return
                }
                var text = date.dateTimeString + " " + (requestConvertible.urlRequest?.url?.path ?? " unknown path ")
                text += " end " + String(Date().timeIntervalSince(date)) + ", " + String(data?.bytes.count ?? 0)
                text += " octets,  status " + String(status)
                FileLogger.shared.write(text: text)
                promise(.success(response))
            }
        }
    }
}
