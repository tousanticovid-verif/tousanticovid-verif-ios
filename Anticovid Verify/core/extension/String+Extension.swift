// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  String+Extension.swift
//  TousAntiCovid
//
//

import UIKit
import CommonCrypto

extension String {
    
    var camelCased: String {
        if contains("_") {
            let allComponents: [String] = components(separatedBy: "_")
            var words: [String] = [(allComponents.first ?? "").lowercased()]
            words.append(contentsOf: allComponents[1..<allComponents.count].map { $0.lowercased().capitalized })
            return words.joined()
        } else {
            return self
        }
    }
    
    var isUuidCode: Bool { self ~= "^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$" }
    var isShortCode: Bool { self ~= "^[A-Za-z0-9]{6}$" }
    var isPostalCode: Bool { self ~= "^[0-9]{5}$" }

    static func ~= (lhs: String, rhs: String) -> Bool {
        guard let regex = try? NSRegularExpression(pattern: rhs) else { return false }
        let range: NSRange = NSRange(location: 0, length: lhs.utf16.count)
        return regex.firstMatch(in: lhs, options: [], range: range) != nil
    }

    subscript(_ range: ClosedRange<Int>) -> String {
        let startIndex: Index = index(self.startIndex, offsetBy: range.lowerBound)
        return String(self[startIndex..<index(startIndex, offsetBy: range.count)])
    }
    
    subscript(_ range: Range<Int>) -> String {
        let startIndex: Index = index(self.startIndex, offsetBy: range.lowerBound)
        return String(self[startIndex..<index(startIndex, offsetBy: range.count)])
    }
    
    subscript(safe range: ClosedRange<Int>) -> String? {
        guard count > range.upperBound else { return nil }
        let startIndex: Index = index(self.startIndex, offsetBy: range.lowerBound)
        return String(self[startIndex..<index(startIndex, offsetBy: range.count)])
    }
    
    subscript(safe range: Range<Int>) -> String? {
        guard count > range.upperBound else { return nil }
        let startIndex: Index = index(self.startIndex, offsetBy: range.lowerBound)
        return String(self[startIndex..<index(startIndex, offsetBy: range.count)])
    }
    
    func toInt() -> Int {
        if let result = try? Int(value: self) {
            return result
        }
        return 0
    }
    
    func removingEmojis() -> String {
        components(separatedBy: .symbols).filter { !$0.isEmpty }.joined().trimmingCharacters(in: .whitespaces)
    }
    
    func cleaningForCSV(_ commaReplacement: String = ".") -> String {
        replacingOccurrences(of: ",", with: commaReplacement)
    }
    
    func cleaningEscapedCharacters() -> String {
        return replacingOccurrences(of: "\\n", with: "\n").replacingOccurrences(of: "\\r", with: "\r").replacingOccurrences(of: "\\\"", with: "\"")
    }
    
    func cleaningForServerFileName() -> String {
        clearingDiacritics().clearingSpecialCharacters()
    }
    
    func clearingDiacritics() -> String {
        folding(options: .diacriticInsensitive, locale: nil)
    }
    
    func clearingSpecialCharacters() -> String {
        let pattern: String = "[^A-Za-z0-9]+"
        return replacingOccurrences(of: pattern, with: "", options: [.regularExpression])
    }
    
    func formattingValueWithThousandsSeparatorIfPossible() -> String {
        if let numberValue = Int(self) {
            return numberValue.formattedWithThousandsSeparator()
        } else {
            return self
        }
    }
    
    func accessibilityNumberFormattedString() -> String {
        guard let intValue = Int(self) else { return self }
        let numberValue: NSNumber = NSNumber(integerLiteral: intValue)
        return NumberFormatter.localizedString(from: numberValue, number: .spellOut)
    }

    func sha256() -> String {
        if let stringData = self.data(using: String.Encoding.utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return ""
    }
    
    private func digest(input: NSData) -> NSData {
        let digestLength: Int = Int(CC_SHA256_DIGEST_LENGTH)
        var hash: [UInt8] = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
    func cleaningPEMStrings() -> String {
        replacingOccurrences(of: "\n", with: "")
            .replacingOccurrences(of: "-----BEGIN EC PRIVATE KEY-----", with: "")
            .replacingOccurrences(of: "-----END EC PRIVATE KEY-----", with: "")
            .replacingOccurrences(of: "-----BEGIN PUBLIC KEY-----", with: "")
            .replacingOccurrences(of: "-----END PUBLIC KEY-----", with: "")
            .replacingOccurrences(of: "-----BEGIN CERTIFICATE-----", with: "")
            .replacingOccurrences(of: "-----END CERTIFICATE-----", with: "")
    }

    func base64urlToBase64() -> String {
        var base64: String = self
            .replacingOccurrences(of: "-", with: "+")
            .replacingOccurrences(of: "_", with: "/")
        if base64.count % 4 != 0 {
            base64.append(String(repeating: "=", count: 4 - base64.count % 4))
        }
        return base64
    }
    
    func base64ToString() -> String {
        let decodedData = Data(base64Encoded: self) ?? Data()
        return String(data: decodedData, encoding: .utf8) ?? ""
    }
    
    func extractDate() -> Date? {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.date(from: self)
    }
    
    func formatBirthDayStringToDCCDate() -> Date? {
        let dateFormater = Date.certificateDateOfBirthFormatter
        var birth = self
        
        if birth.count == 4 {
            birth += "-12-31"
        } else if birth.count == 7 {
            birth += "-31"
        }
        
        return dateFormater.date(from: birth)
    }

    func formatBirthDayStringTo2DDocDate() -> Date? {
        let dateFormater = Date.dateFrFormatter
        var birth = self
        
        if birth.count == 4 {
            birth = "31/12/" + birth
        } else if birth.count == 7 {
            birth = "31/" + birth
        }
        
        return dateFormater.date(from: birth)
    }
    
    func caseInsensitiveHasPrefix(_ prefix: String) -> Bool {
        return lowercased().hasPrefix(prefix.lowercased())
    }
    
    func checkAndReturnCurrentPrefix(in prefixes: [String]) -> String {
        for current in prefixes {
            if self.caseInsensitiveHasPrefix(current) {
                return current
            }
        }
        return ""
    }
}
