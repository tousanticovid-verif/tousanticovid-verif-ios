//
//  Publisher+extension.swift
//  Anticovid Verify
//

import Combine
import OSLog

extension Publisher {
    /**
     Attaches a subscriber to an empty closure based behavior.
     */
    func sink(receiveCompletion: ((Subscribers.Completion<Self.Failure>) -> Void)? = nil,
              receiveValue: ((Self.Output) -> Void)? = nil) -> AnyCancellable {
        sink { result in
            guard let receiveCompletion = receiveCompletion else {
                switch result {
                case .finished:
                    return
                case .failure:
                    break
                    //os_log("%@",type: .error, "Error: \(error)")
                }
                return
            }
            
            receiveCompletion(result)
        } receiveValue: { result in
            guard let receiveValue = receiveValue else {
                return
            }
            
            receiveValue(result)
        }

    }
}
