//
//  NSAttributedString+Extension.swift
//  Anticovid Verify
//
//

import UIKit

extension NSMutableAttributedString {
    @discardableResult
    func appendText(_ text: String, font: UIFont? = .systemFont(ofSize: 14),
                    alignment: NSTextAlignment = .center, color: UIColor = .black) -> NSMutableAttributedString {
        var attributes = [NSAttributedString.Key: Any]()
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = alignment
        
        attributes[NSAttributedString.Key.paragraphStyle] = paragraph
        attributes[NSAttributedString.Key.foregroundColor] = color
        
        if let font = font {
            attributes[NSAttributedString.Key.font] = font
        }
        
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        self.append(attributedString)
        
        return self
    }
}
