//
//  Dictionary+Extension.swift
//  Anticovid Verify
//
//

import Foundation
extension Dictionary where Key == String {
    func stringValueForKeyInsensitive(key: Key) -> String? {
        let foundKey = self.keys.first { $0.compare(key, options: .caseInsensitive) == .orderedSame } ?? key
        return self[foundKey] as? String
    }
}
