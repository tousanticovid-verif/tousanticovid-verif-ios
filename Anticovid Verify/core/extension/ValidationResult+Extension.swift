//
//  ValidationResult+Extension.swift
//  Anticovid Verify
//

import Foundation
import CertLogic

extension ValidationResult: Hashable {
    public static func == (lhs: ValidationResult, rhs: ValidationResult) -> Bool {
        guard let lhsHash = lhs.rule?.identifier, let rhsHash = rhs.rule?.identifier else {
            return false
        }
        
        return lhsHash == rhsHash
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(rule?.identifier ?? "")
    }
}
