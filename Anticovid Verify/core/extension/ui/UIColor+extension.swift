//
//  UIColor+extension.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

extension UIColor {

    static var greenIn = UIColor(named: "greenIn")!
    
    static var redIn = UIColor(named: "redIn")!
    
    static var lightGrayIn = UIColor(named: "lightGrayIn")!
    
    static var blueIn = UIColor(named: "blueIn")!

    static var secondSubtitle =  UIColor(named: "secondSubtitle")!

    static var goldIn = UIColor(named: "goldIn")!
    
    static var grayIn = UIColor(named: "grayIn")!

    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
