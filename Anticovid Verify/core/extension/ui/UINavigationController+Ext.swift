//
//  UINavigationController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

extension UINavigationController {
    
    static func updateNavigationBarTheme(navigationBar: UINavigationBar? = nil) {
        let mode = PremiumManager.shared.modeSelected
        let shouldShowFullResult = mode == .OT || mode == .tacVerifPlus
        
//        Change navigation bar appearence
        let attributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: INStyles.fontForStyle(style: "navTitle"),
                                                           NSAttributedString.Key.foregroundColor: shouldShowFullResult ? UIColor.white : .blueIn]
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = attributes
        appearance.shadowImage = UIColor.goldIn.image()
        appearance.backgroundColor = shouldShowFullResult ? .blueIn : .white
        
        // UINavigationBar appearance proxy
        UINavigationBar.appearance().tintColor = shouldShowFullResult ? .white : .blueIn
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
        // current  navigation bar if existing
        navigationBar?.titleTextAttributes = attributes
        navigationBar?.barTintColor = shouldShowFullResult ? .blueIn : .white
        navigationBar?.backgroundColor = shouldShowFullResult ? .blueIn : .white
        navigationBar?.tintColor = shouldShowFullResult ? .white : .blueIn
        navigationBar?.standardAppearance = appearance
        navigationBar?.scrollEdgeAppearance = appearance
    }
}
