// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  WalletCertificate.swift
//  TousAntiCovid
//
//

import Foundation

class WalletCertificate {
    
    let id: String
    let value: String
    let type: WalletConstant.CertificateType
    
    var fields: [String: String] = [:]
    
    var authority: String?
    var certificateId: String?
    var issueDate: String?
    var creationDate: String?
    var country: String?
    var message: Data? { fatalError("Must be overriden") }
    var signature: Data? { fatalError("Must be overriden") }
    var isSignatureAlreadyEncoded: Bool { fatalError("Must be overriden") }
    
    var pillTitles: [String] { fatalError("Must be overriden") }
    var shortDescription: String { fatalError("Must be overriden") }
    var fullDescription: String { fatalError("Must be overriden") }
    
    var timestamp: Double { fatalError("Must be overriden") }
    var isOld: Bool {
//        guard let oldCertificateThreshold = ParametersManager.shared.walletOldCertificateThresholdInDays(certificateType: type.rawValue) else { return false }
//        return Date().timeIntervalSince1970 - timestamp >= Double(oldCertificateThreshold) * 86400.0
        return false
    }

    var validityString: String {
        let walletTestCertificateValidityThresholds: [Int] = [48, 72]// ParametersManager.shared.walletTestCertificateValidityThresholds
        let maxValidityInHours: Int = walletTestCertificateValidityThresholds.max() ?? 0
        let timeSinceCreation: Double = Date().timeIntervalSince1970 - timestamp
        let validityThresholdInHours: Int? = walletTestCertificateValidityThresholds.filter { Double($0 * 3600) > timeSinceCreation } .min()
        return String(format: ( validityThresholdInHours == nil ? "wallet.proof.moreThanSpecificHours" : "wallet.proof.lessThanSpecificHours").localized, validityThresholdInHours ?? maxValidityInHours)
    }
    
    var publicKey: String? {
        guard let authority = authority else { return nil }
        guard let certificateId = certificateId else { return nil }
        return CertificatesManager.shared.ddocCertificates?["\(authority)\(certificateId)"]
    }
    
    init(id: String = UUID().uuidString, value: String, type: WalletConstant.CertificateType, needParsing: Bool = false) {
        self.id = id
        self.value = value
        self.type = type
        guard needParsing else { return }
        self.fields = parse(value)
    }
    
    func parse(_ value: String) -> [String: String] { fatalError("Must be overriden") }
    
    func getIssueDate() -> String {
        if let hex = self.issueDate, let decimal = Int(hex, radix: 16) {
            let calendar = Calendar.current
            
            var dateComponents = DateComponents()
            dateComponents.timeZone = calendar.timeZone
            dateComponents.year = 2000
            dateComponents.month = 1
            dateComponents.day = 1
            dateComponents.hour = 0
            dateComponents.minute = 0
            dateComponents.second = 0
            
            return calendar.date(from: dateComponents)?.dateByAddingDays(decimal).shortDateFormatted() ?? ""
        }
        
        return ""
    }
    
    func getCreationDate() -> String {
        if let hex = self.creationDate, let decimal = Int(hex, radix: 16) {
            let calendar = Calendar.current
            
            var dateComponents = DateComponents()
            dateComponents.timeZone = calendar.timeZone
            dateComponents.year = 2000
            dateComponents.month = 1
            dateComponents.day = 1
            dateComponents.hour = 0
            dateComponents.minute = 0
            dateComponents.second = 0
            
            return calendar.date(from: dateComponents)?.dateByAddingDays(decimal).shortDateFormatted() ?? ""
        }
        
        return ""
    }
}

extension WalletCertificate {
    
    static func from(rawCertificate: RawWalletCertificate) -> WalletCertificate? {
        guard let certificateType = WalletManager.certificateType(value: rawCertificate.value) else { return nil }
        switch certificateType {
        case .sanitary:
            return SanitaryCertificate(id: rawCertificate.id, value: rawCertificate.value, type: certificateType, needParsing: true)
        case .vaccination:
            return VaccinationCertificate(id: rawCertificate.id, value: rawCertificate.value, type: certificateType, needParsing: true)
        }
    }
    
    static func from(doc: String) -> WalletCertificate? {
        guard let certificateType = WalletManager.certificateType(value: doc) else { return nil }
        switch certificateType {
        case .sanitary:
            return SanitaryCertificate(value: doc, type: certificateType, needParsing: true)
        case .vaccination:
            return VaccinationCertificate(value: doc, type: certificateType, needParsing: true)
        }
    }
    
    func toRawCertificate() -> RawWalletCertificate {
        RawWalletCertificate(id: id, value: value)
    }
    
}
