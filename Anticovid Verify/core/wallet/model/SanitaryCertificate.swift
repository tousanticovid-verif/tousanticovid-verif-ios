// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
//  SanitaryCertificate.swift
//  TousAntiCovid
//
//

import Foundation
import OrderedCollections

final class SanitaryCertificate: WalletCertificate {
    
    enum FieldName: String, CaseIterable {
        case authority
        case certificateId
        case issueDate
        case creationDate
        case country
        case firstName = "F0"
        case name = "F1"
        case birthDate = "F2"
        case gender = "F3"
        case analysisCode = "F4"
        case analysisResult = "F5"
        case analysisDate = "F6"
    }
    
    override var message: Data? { value.components(separatedBy: WalletConstant.Separator.unit.ascii).first?.data(using: .ascii) }
    override var signature: Data? {
        guard let signatureString = value.components(separatedBy: WalletConstant.Separator.unit.ascii).last else { return nil }
        do {
            return try signatureString.decodeBase32(padded: signatureString.hasSuffix("="))
        } catch {
            return nil
        }
    }
    override var isSignatureAlreadyEncoded: Bool { false }
    
    var firstName: String? { fields[FieldName.firstName.rawValue]?.replacingOccurrences(of: "/", with: ",") }
    var name: String? { fields[FieldName.name.rawValue] }
    var birthDateString: String?
    var gender: String? {
        guard let gender = fields[FieldName.gender.rawValue] else { return nil }
        return "wallet.proof.sanitaryCertificate.\(FieldName.gender.rawValue).\(gender)".localized
    }
    var analysisDate: Date?
    var analysisDateString: String? { analysisDate?.fullDateFormatted() }
    var analysisCode: String? {
        guard let code = fields[FieldName.analysisCode.rawValue] else { return nil }
        let codeDisplayString: String = code
        return String(format: codeDisplayString, code)
    }
    var analysisResult: String? {
        guard let result = fields[FieldName.analysisResult.rawValue] else { return nil }
        return "wallet.proof.sanitaryCertificate.\(FieldName.analysisResult.rawValue).\(result)".localized
    }
    
    override var timestamp: Double { analysisDate?.timeIntervalSince1970 ?? 0.0 }
    
    override var pillTitles: [String] { ["wallet.proof.sanitaryCertificate.pillTitle".localized] }
    override var shortDescription: String { [firstName, name].compactMap { $0 }.joined(separator: " ") }
    override var fullDescription: String {
        var text: String = "wallet.proof.sanitaryCertificate.description".localized
        text = text.replacingOccurrences(of: "<\(FieldName.firstName.rawValue)>", with: firstName ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.name.rawValue)>", with: name ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.birthDate.rawValue)>", with: birthDateString ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.gender.rawValue)>", with: gender ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.analysisCode.rawValue)>", with: analysisCode ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.analysisDate.rawValue)>", with: analysisDateString ?? "N/A")
        text = text.replacingOccurrences(of: "<\(FieldName.analysisResult.rawValue)>", with: analysisResult ?? "N/A")
        
        text += "\n"
        text += validityString
        
        return text
    }
    var sha256: String?
    
    override init(id: String = UUID().uuidString, value: String, type: WalletConstant.CertificateType, needParsing: Bool = false) {
        super.init(id: id, value: value, type: type, needParsing: needParsing)
        guard needParsing else { return }
        self.birthDateString = parseBirthDate()
        self.analysisDate = parseAnalysisDate()
    }
    
    override func parse(_ value: String) -> [String: String] {
        var captures: [String: String] = [:]
        guard let regex = try? NSRegularExpression(pattern: type.validationRegex) else { return captures }
        let matches: [NSTextCheckingResult] = regex.matches(in: value, options: [], range: NSRange(location: 0, length: value.count))
        guard let match = matches.first else { return captures }
        
        FieldName.allCases.forEach {
            let matchRange: NSRange = match.range(withName: $0.rawValue)
            guard let substringRange = Range(matchRange, in: value) else { return }
            let capture = String(value[substringRange])
            switch $0 {
            case .authority:
                authority = capture
            case .certificateId:
                certificateId = capture
            case .issueDate:
                issueDate = capture
            case .creationDate:
                creationDate = capture
            case .country:
                country = capture
            default:
                captures[$0.rawValue] = capture
            }
        }
        let separator = "\u{001F}"
        var components = value.components(separatedBy: separator)
        components = components.dropLast()
        let output = components.joined(separator: separator)
        sha256 = output.sha256?.hexString
        return captures
    }
    
    private func parseBirthDate() -> String? {
        guard let birthDateString = fields[FieldName.birthDate.rawValue], birthDateString.count == 8 else { return nil }
        let dayString: String = birthDateString[0...1]
        let monthString: String = birthDateString[2...3]
        let yearString: String = birthDateString[4...7]
        return "\(dayString)/\(monthString)/\(yearString)"
    }
    
    private func parseAnalysisDate() -> Date? {
        guard let analysisDateString = fields[FieldName.analysisDate.rawValue], analysisDateString.count == 12 else { return nil }
        let dayString: String = analysisDateString[0...1]
        let monthString: String = analysisDateString[2...3]
        let yearString: String = analysisDateString[4...7]
        let hourString: String = analysisDateString[8...9]
        let minuteString: String = analysisDateString[10...11]
        let dateComponents: DateComponents = DateComponents(year: Int(yearString), month: Int(monthString), day: Int(dayString), hour: Int(hourString), minute: Int(minuteString))
        return Calendar.current.date(from: dateComponents)
    }
    
}

extension SanitaryCertificate: BarcodeProtocolObject {
    func getUIInformation(result: CertificateState, shouldShowFullResult: Bool) -> OrderedDictionary<String, [FieldRow]> {
        var results = [FieldRow]()
        if shouldShowFullResult {
            results.append(FieldRow.Field(title: "Date et heure du prélèvement", subtitle: self.analysisDateString ?? "", picto: ResultPicto.none))
            if let firstName = firstName, !firstName.isEmpty {
                results.append(FieldRow.Field(title: "Liste des prénoms", subtitle: firstName,picto: .eye))
            }
            if let name = name, !name.isEmpty {
                results.append(FieldRow.Field(title:"Nom de famille", subtitle: name, picto: .eye))
            }
            results.append(FieldRow.Field(title:"Date de naissance", subtitle: self.birthDateString ?? "", picto: .eye))
            results.append(FieldRow.Field(title:"Résultat", subtitle: Formatter.shared.analysisResultFormatter(analysisResult: self.fields["F5"]),picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Code analyse", subtitle: Formatter.shared.analysisCodeFormatter(analysisCode: self.analysisCode),picto: ResultPicto.none))
            results.append(FieldRow.Field(title: "Genre", subtitle: self.fields["F3"] ?? "",picto: ResultPicto.none))
        
            results.append(FieldRow.Field(title:"Type de document", subtitle: "Test COVID",picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Identifiant de l’autorité de certification", subtitle: self.authority ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Identifiant du certificat", subtitle: self.certificateId ?? "",picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Date d’émission du document", subtitle: self.getIssueDate(),picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Date de création de la signature", subtitle: self.getCreationDate(),picto: ResultPicto.none))
            results.append(FieldRow.Field(title:"Pays émetteur du document", subtitle: Formatter.shared.countryName(from: self.country ?? ""),picto: ResultPicto.none))
            let resultRow = FieldRow.Field(title: "DURÉE DEPUIS LE PRÉVELEMENT", subtitle: self.analysisDate?.formatForTestOT() ?? "?", picto: ResultPicto.none)
            return [
                "Données de validité": [resultRow],
                "Informations du 2D-DOC": results
            ]
        } else {

            if let firstName = firstName, !firstName.isEmpty {
                results.append(FieldRow.Field(title:"Liste des prénoms", subtitle: firstName.uppercased(),picto: .eye))
            }
            if let name = name, !name.isEmpty {
                results.append(FieldRow.Field(title:"Nom de famille", subtitle: name.uppercased(),picto: .eye))
            }
            results.append(FieldRow.Field(title:"Date de naissance", subtitle: self.birthDateString ?? "",picto: .eye))
            
            return ["Informations du 2D-DOC": results]
        }
    }
    
    func getIdHash() -> String {
        return sha256 ?? ""
    }
}
