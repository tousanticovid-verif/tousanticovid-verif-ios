//
//  Const+Notification.swift
//  Anticovid Verify
//
//

import Foundation

struct ConstNotification {
    static let updateBannerSyncApp = NSNotification.Name(rawValue: "UPDATE_BANNER_SYNC_APP")
    static let updateApplicationAvailable = NSNotification.Name(rawValue: "UPDATE_APPLICATION_AVAILABLE")
    static let updateApplicationRequired = NSNotification.Name(rawValue: "UPDATE_APPLICATION_REQUIRED")
    static let navControllerAppareance = NSNotification.Name(rawValue: "UPDATE_NAVIGATION_CONTROLLER_APPERANCE")
}
