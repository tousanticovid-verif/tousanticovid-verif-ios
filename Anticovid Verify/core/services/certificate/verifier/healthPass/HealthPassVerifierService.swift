//
//  HealthPassVerifierService.swift
//  Anticovid Verify
//


import Foundation
import SwiftDGC
import Combine

class HealthPassVerifierService: PassVerifierService {
    static let shared = HealthPassVerifierService()

    lazy var configuration: HealthPassValidity = { rulesMotorService.healthPassVerifierConfiguration() }() ?? ConstRules.healthPassValidity

    func verifyDCC(certificate: HCert, for date: Date) -> VerifierResult? {
        if let res = verifyEuCertificate(certificate: certificate, date: date) {
            return res
        } else if let res = verifyFrCertificate(certificate: certificate, date: date) {
            return res
        } else {
            return nil
        }
    }
    
    func verify2DDOC(certificate: WalletCertificate, for date: Date) -> VerifierResult? {
        if let certificate = certificate as? SanitaryCertificate {
            return verifyTestPCR(certificate: certificate)
        } else if let certificate = certificate as? VaccinationCertificate {
            return verifyVaccin(certificate: certificate)
        } else {
            return nil
        }
    }
}

private extension HealthPassVerifierService {
    func verifyTestPCR(certificate: SanitaryCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil

        guard let result = certificate.fields["F5"] else {
            return nil
        }
        
        guard let samplingDate = certificate.analysisDate else {
            return nil
        }
        
        let today = Date()
        
        let isTest = rulesMotorService.isTest(loinc: certificate.analysisCode ?? "")
        let isPCR = rulesMotorService.isPcr(loinc: certificate.analysisCode ?? "")
        let isPositive = result.uppercased() == "P"
        let isNegative = result.uppercased() == "N"
        
        var startDate: Date? = nil
        var endDate: Date!
        
        if isPositive {
            startDate = samplingDate.dateByAddingDays(isPCR ? configuration.testPositivePcrStartDay :
                                                        configuration.testPositiveAntigenicStartDay)
            endDate = samplingDate.dateByAddingDays(isPCR ? configuration.testPositivePcrEndDay :
                                                        configuration.testPositiveAntigenicEndDay)
        } else {
            endDate = samplingDate.dateByAddingHours(isPCR ? configuration.testNegativePcrEndHour :
                                                        configuration.testNegativeAntigenicEndHour)
        }
        
        if !WalletManager.shared.checkCertificateSignature(certificate) {
            resultState = .nonValid
            globalValidity = .invalid
        } else if !isTest {
            resultState = .nonValid
            globalValidity = .invalid
        } else {
            if isNegative {
                resultState = today <= endDate ? .valid : .nonValid
                globalValidity = today <= endDate ? .valid : .invalid
            } else if isPositive {
                resultState = (today >= startDate! && today <= endDate) ? .valid : .nonValid
                globalValidity = (today >= startDate! && today <= endDate) ? .valid : .invalid
            } else {
                resultState = .nonValid
                globalValidity = .invalid
            }
        }

        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_LITE, ressourceType: ConstStat.D_DOC_B2), certHash: certificate.getIdHash())
    }
    
    func verifyVaccin(certificate: VaccinationCertificate) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil

        // Verifier le Nom du vaccin et le nombre d'injection
        guard let vaccinationDateStart = certificate.lastVaccinationDate?.roundingToBeginningOfDay(),
              let vaccinationDateEnd = certificate.lastVaccinationDate?.roundingToEndOfDay(),
              let birth = certificate.birthDateString,
              let birthDate = birth.formatBirthDayStringTo2DDocDate() else {
            return nil
        }

        let isJanssen = (certificate.vaccineName?.value.lowercased() ?? "").contains("janssen")
        let isFinished = certificate.vaccinationCycleState?.lowercased() == "te"
        let validity = WalletManager.shared.checkCertificateSignature(certificate)

        let today = Date()
        
        let vaccineDelay = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelay)
        let vaccineDelayMax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMax)
        let vaccineBoosterDelayNew = vaccinationDateStart.dateByAddingDays(configuration.vaccineBoosterDelayNew)
        let vaccineDelayJanssen1 = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelayJanssen1)
        let vaccineDelayMaxJanssen1 = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxJanssen1)
        let vaccineBoosterDelayUnderAgeNew = vaccinationDateStart.dateByAddingDays(configuration.vaccineBoosterDelayUnderAgeNew)
        let vaccineDelayMaxRecovery = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxRecovery)
        let vaccineBoosterDelayMax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineBoosterDelayMax)
        let injectionCount = Int(certificate.completeCycleDosesCount?.value ?? "0") ?? 0
        
        let hasVaccineDoseExpiration = (try? hasVaccineDoseExpiration(vaccineBoosterAgeDuration: configuration.vaccineBoosterAgePeriod,
                                                                birthDate: birthDate)) ?? false
        
        if !validity {
            resultState = .nonValid
            globalValidity = .invalid
            // janssen
        } else if isFinished && (injectionCount == 1 && isJanssen) &&
                    (today >= vaccineDelayJanssen1 && today <= vaccineDelayMaxJanssen1) {
            resultState = .valid
            globalValidity = .valid
            // other + age
        } else if isFinished {
            if !hasVaccineDoseExpiration,
               (!isJanssen && injectionCount == 1 && today >= vaccineDelay) ||
                (injectionCount == 2 && today >= vaccineDelay) ||
                (injectionCount >= 3 && today >= vaccineBoosterDelayUnderAgeNew) {
                resultState = .valid
                globalValidity = .valid
            } else if hasVaccineDoseExpiration,
                      (!isJanssen && injectionCount == 1 && today >= vaccineDelay && today <= vaccineDelayMaxRecovery) ||
                        (injectionCount == 2 && today >= vaccineDelay && today <= vaccineDelayMax) ||
                        (injectionCount > 2 && today >= vaccineBoosterDelayNew && today <= vaccineBoosterDelayMax) {
                resultState = .valid
                globalValidity = .valid
            } else {
                resultState = .nonValid
                globalValidity = .invalid
            }
        } else {
            resultState = .nonValid
            globalValidity = .invalid
        }
        
        return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_LITE, ressourceType:  ConstStat.D_DOC_L1), certHash: certificate.getIdHash())
    }
    
    func verifyEuCertificate(certificate: HCert, date: Date) -> VerifierResult? {
        HCert.publicKeyStorageDelegate = self
        
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        switch certificate.type {
        case .vaccine:
            guard let vaccinationDateString = certificate.body["v"][0]["dt"].string,
                  let birth = certificate.body["dob"].string else {
                return nil
            }
            guard let vaccinationDateStart = Date(dateString: vaccinationDateString)?.roundingToBeginningOfDay(),
                  let vaccinationDateEnd = Date(dateString: vaccinationDateString)?.roundingToEndOfDay() else {
                return nil
            }
            
            let today = Date()

            let vaccineDelay = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelay)
            let vaccineDelayMax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMax)
            let vaccineBoosterDelayNew = vaccinationDateStart.dateByAddingDays(configuration.vaccineBoosterDelayNew)
            let vaccineBoosterDelayMax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineBoosterDelayMax)
            
            let vaccineDelayNova  = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelayNovavax)
            let vaccineDelayMaxNovavax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxNovavax)
            let vaccineBoosterDelayMaxNovavax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineBoosterDelayMaxNovavax)
            let vaccineDelayMaxRecoveryNovavax = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxRecoveryNovavax)
            
            let vaccineDelayJanssen1 = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelayJanssen1)
            let vaccineDelayMaxJanssen1 = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxJanssen1)
            let vaccineDelayJanssen2 = vaccinationDateStart.dateByAddingDays(configuration.vaccineDelayJanssen2)
            let vaccineDelayMaxJanssen2 = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxJanssen2)
            let vaccineBoosterDelayMaxJanssen = vaccinationDateEnd.dateByAddingDays(configuration.vaccineBoosterDelayMaxJanssen)
            
            let vaccineDelayMaxRecovery = vaccinationDateEnd.dateByAddingDays(configuration.vaccineDelayMaxRecovery)

            guard let dosesCount = certificate.body["v"][0]["dn"].int,
                  let expectedDosesCount = certificate.body["v"][0]["sd"].int,
                  let vaccineName = certificate.body["v"][0]["mp"].string,
                  let prophylacticAgent = certificate.body["v"][0]["vp"].string,
                  let manufacturer = certificate.body["v"][0]["ma"].string
                   else {
                return nil
            }
            
            let birthDate = birth.formatBirthDayStringToDCCDate()
            var vaccineDoseExpired =  false
            if let birthDate = birthDate {
                vaccineDoseExpired = (try? hasVaccineDoseExpiration(vaccineBoosterAgeDuration: configuration.vaccineBoosterAgePeriod, birthDate: birthDate)) ?? false
            }
            
            if !certificate.cryptographicallyValid {
                resultState = .nonValid
                globalValidity = .invalid
            } else if certificate.iat > today ||
                        (certificate.exp < today &&
                         ConfDataManager.shared.isExpirationDateCheckedForVaccine) {
                resultState = .nonValid
                globalValidity = .invalid
            } else if dosesCount >= expectedDosesCount && rulesMotorService.isVaccineProphylaxis(prophylacticAgent: prophylacticAgent) && rulesMotorService.isVaccineMedicinalProduct(vaccineName: vaccineName) && rulesMotorService.vaccineMahManfContains(manufacturer: manufacturer) {
                if vaccineName == "EU/1/20/1525" && expectedDosesCount == 1 && today >= vaccineDelayJanssen1 && today <= vaccineDelayMaxJanssen1 { // Jansen
                    resultState = .valid
                    globalValidity = .valid
                } else {
                    if !vaccineDoseExpired {
                        if (expectedDosesCount == 1 && today >= vaccineDelay && vaccineName != "EU/1/20/1525") ||
                            (expectedDosesCount == 2 && today >= vaccineDelay) ||
                            (expectedDosesCount > 2 && today >= vaccineDelay) {
                            resultState = .valid
                            globalValidity = .valid
                        } else {
                            resultState = .nonValid
                            globalValidity = .invalid
                        }
                    } else {
                        if (dosesCount == 1 && expectedDosesCount == 1 &&
                            (vaccineName != "EU/1/20/1525" && vaccineName != "EU/1/21/1618") &&
                            today >= vaccineDelay && today <= vaccineDelayMaxRecovery
                        ) {
                            resultState = .valid
                            globalValidity = .valid
                        } else if dosesCount == 2 && expectedDosesCount == 2 &&
                                   vaccineName != "EU/1/20/1525" && vaccineName != "EU/1/21/1618" &&
                                   today >= vaccineDelay && today <= vaccineDelayMax {
                            resultState = .valid
                            globalValidity = .valid
                        } else if (expectedDosesCount > 2 || dosesCount > expectedDosesCount) &&
                                   vaccineName != "EU/1/20/1525" && vaccineName != "EU/1/21/1618" &&
                                   today >= vaccineBoosterDelayNew && today <= vaccineBoosterDelayMax {
                            resultState = .valid
                            globalValidity = .valid
                        } else if dosesCount == 1 && expectedDosesCount == 1 && vaccineName == "EU/1/21/1618" &&
                                today >= vaccineDelayNova && today <= vaccineDelayMaxRecoveryNovavax {
                            resultState = .valid
                            globalValidity = .valid
                        } else if dosesCount == 2 && expectedDosesCount == 2 && vaccineName == "EU/1/21/1618" &&
                                   today >= vaccineDelayNova && today <= vaccineDelayMaxNovavax {
                            resultState = .valid
                            globalValidity = .valid
                        } else if (expectedDosesCount > 2 || dosesCount > expectedDosesCount) && vaccineName == "EU/1/21/1618" &&
                                   today >= vaccineDelayNova && today <= vaccineBoosterDelayMaxNovavax {
                            resultState = .valid
                            globalValidity = .valid
                        } else if dosesCount == 2 && expectedDosesCount == 2 && vaccineName == "EU/1/20/1525" && today >= vaccineDelayJanssen2 && today <= vaccineDelayMaxJanssen2 {
                            resultState = .valid
                            globalValidity = .valid
                        } else if (expectedDosesCount > 2 || dosesCount > expectedDosesCount) && vaccineName == "EU/1/20/1525" && today >= vaccineDelayJanssen2 && today <= vaccineBoosterDelayMaxJanssen {
                            resultState = .valid
                            globalValidity = .valid
                        } else {
                            resultState = .nonValid
                            globalValidity = .invalid
                        }
                    }
                }
            } else {
                resultState = .nonValid
                globalValidity = .invalid
            }
            
            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_LITE, ressourceType: ConstStat.DCC_VACCINATION), certHash: certificate.getIdHash(forCertField: "v"))
        case .test:
            
            guard let testType = certificate.body["t"][0]["tt"].string,
                  let testResult = certificate.body["t"][0]["tr"].string,
                  let samplingDateString = certificate.body["t"][0]["sc"].string,
                  let samplingDate = Date(dateString: samplingDateString) else {
                      return nil
                  }
            
            let today = Date()
        
            let isTest = rulesMotorService.isTest(loinc: testType)
            let isPCR = rulesMotorService.isPcr(loinc: testType)
            let isPositive = testResult == "260373001"
            let isNegative = testResult == "260415000"
            
            var startDate: Date? = nil
            var endDate: Date!
            
            if isPositive {
                startDate = samplingDate.dateByAddingDays(isPCR ? configuration.testPositivePcrStartDay :
                                                            configuration.testPositiveAntigenicStartDay)
                
                endDate = samplingDate.dateByAddingDays(isPCR ? configuration.testPositivePcrEndDay :
                                                            configuration.testPositiveAntigenicEndDay)
            } else {
                endDate = samplingDate.dateByAddingHours(isPCR ? configuration.testNegativePcrEndHour :
                                                            configuration.testNegativeAntigenicEndHour)
            }
            
            if !isTest {
                resultState = .nonValid
                globalValidity = .invalid
            } else {
                if !certificate.cryptographicallyValid {
                    resultState = .nonValid
                    globalValidity = .valid
                } else if certificate.iat > today ||
                            (certificate.exp < today && ConfDataManager.shared.isExpirationDateCheckedForTest) {
                    resultState = .nonValid
                    globalValidity = .invalid
                } else if isPositive && startDate != nil {
                    (resultState, globalValidity) = checkNegativeTestOrRecovery(
                        today: today,
                        samplingDate: samplingDate,
                        startDate: startDate!,
                        configuration: TestOrRecoveryConfigurationChecker(
                            recoveryAcceptanceAgePeriod: configuration.recoveryAcceptanceAgePeriod,
                            recoveryDelayMaxTV: configuration.recoveryDelayMaxTV,
                            recoveryDelayMax: configuration.recoveryDelayMax,
                            recoveryDelayMaxUnderAge: configuration.recoveryDelayMaxUnderAge
                        ),
                        certificate: certificate,
                        certField: "t",
                        certFieldIssuer: "tc"
                    )
                } else if isNegative && today <= endDate {
                    resultState = .valid
                    globalValidity = .valid
                } else {
                    resultState = .nonValid
                    globalValidity = .invalid
                }
            }
            
            return VerifierResult(
                certificateState: resultState!,
                statInformation: StatInformation(
                    globalValidity: globalValidity,
                    controlType: ConstStat.TACV_2DDOC_IOS_LITE,
                    ressourceType: ConstStat.DCC_TEST,
                    result: isPositive ? "TEST POSITIF" : "TEST NEGATIF",
                    resultInfo: certificate.body["t"][0]["tc"].string?.checkAndReturnCurrentPrefix(in: ConstRules.testPrefixes) ?? ""
                ),
                certHash: certificate.getIdHash(forCertField: "t")
            )
            
        case .recovery:
            guard let firstSamplingDateString = certificate.body["r"][0]["fr"].string,
                  let firstSamplingDate = Date(dateString: firstSamplingDateString)?.roundingToEndOfDay() else {
                      return nil
                  }
            
            let today = Date()
                        
            let minimumValidity = firstSamplingDate.dateByAddingDays(configuration.recoveryStartDay)
            
            if !certificate.cryptographicallyValid {
                resultState = .nonValid
                globalValidity = .invalid
            } else if certificate.iat > today ||
                        (certificate.exp < today &&
                         ConfDataManager.shared.isExpirationDateCheckedForRecovery) {
                resultState = .nonValid
                globalValidity = .invalid
            } else {
                (resultState, globalValidity) = checkNegativeTestOrRecovery(
                    today: today,
                    samplingDate: firstSamplingDate,
                    startDate: minimumValidity,
                    configuration: TestOrRecoveryConfigurationChecker(
                        recoveryAcceptanceAgePeriod: configuration.recoveryAcceptanceAgePeriod,
                        recoveryDelayMaxTV: configuration.recoveryDelayMaxTV,
                        recoveryDelayMax: configuration.recoveryDelayMax,
                        recoveryDelayMaxUnderAge: configuration.recoveryDelayMaxUnderAge
                    ),
                    certificate: certificate,
                    certField: "r",
                    certFieldIssuer: "is"
                )
            }
            
            return VerifierResult(
                certificateState: resultState!,
                statInformation: StatInformation(
                    globalValidity: globalValidity,
                    controlType: ConstStat.TACV_2DDOC_IOS_LITE,
                    ressourceType: ConstStat.DCC_RECOVERY,
                    resultInfo: certificate.body["r"][0]["is"].string?.checkAndReturnCurrentPrefix(in: ConstRules.testPrefixes) ?? ""
                ),
                certHash: certificate.getIdHash(forCertField: "r")
            )
        default:
            return nil
        }
    }
    
    func verifyFrCertificate(certificate: HCert, date: Date) -> VerifierResult? {
        var globalValidity: DataTypeValidation = .invalid
        var resultState: CertificateState? = nil
        
        switch certificate.type {
        case .exampted:
            guard let dateValidFromDateString = certificate.body["ex"]["df"].string,
                  let dateValidFrom = Date(dateString: dateValidFromDateString)?.roundingToBeginningOfDay(),
                  let dateValidUntilDateString = certificate.body["ex"]["du"].string,
                  let dateValidUntil = Date(dateString: dateValidUntilDateString)?.roundingToEndOfDay() else {
                return nil
            }
                        
            if !certificate.cryptographicallyValid {
                resultState = .nonValid
                globalValidity = .invalid
            } else if dateValidFrom > date ||
                      dateValidUntil < date ||
                      certificate.iat > date ||
                      (certificate.exp < date && ConfDataManager.shared.isExpirationDateCheckedForExemption) {
                resultState = .nonValid
                globalValidity = .invalid
            } else {
                resultState = .valid
                globalValidity = .valid
            }
            
            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_OT, ressourceType: ConstStat.DCC_EXEMPTION), certHash: certificate.getIdHash(forCertField: "ex"))
        case .activity:
            
            let today = Date()

            if !certificate.cryptographicallyValid {
                resultState = .nonValid
                globalValidity = .invalid
            } else if certificate.exp < today || certificate.iat > today {
                resultState = .nonValid
                globalValidity = .invalid
            } else {
                resultState = .valid
                globalValidity = .valid
            }
            
            return VerifierResult(certificateState: resultState!, statInformation: StatInformation(globalValidity: globalValidity, controlType: ConstStat.TACV_2DDOC_IOS_OT, ressourceType: ConstStat.DCC_EXEMPTION), certHash: certificate.getIdHash(forCertField: "a"))
            
        case .unknown, .recovery, .test, .vaccine:
            return nil
        }
    }
}
