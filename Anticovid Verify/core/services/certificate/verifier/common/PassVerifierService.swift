//
//  PassVerifierService.swift
//  Anticovid Verify
//

import Foundation
import SwiftDGC

protocol PassVerifierCommonRulesConfig {
    var recoveryStartDay: Int { get }
    var vaccineBoosterDelayUnderAgeNew: Int { get }
    var vaccineBoosterDelayNew: Int { get }
}

open class PassVerifierService {    
    let userData: UserDataManager
    let statService: StatService
    let rulesMotorService: RulesService
    let premiumManager: PremiumManager
    
    init(userdata: UserDataManager = .sharedInstance,
         statService: StatService = .shared,
         rulesMotorService: RulesService = .shared,
         premiumManager: PremiumManager = .shared) {
        self.userData = userdata
        self.rulesMotorService = rulesMotorService
        self.statService = statService
        self.premiumManager = premiumManager
    }
    
    func hasVaccineDoseExpiration(vaccineBoosterAgeDuration: String, birthDate: Date) throws -> Bool {
        guard let components = try? DateComponents(ISO8601String: vaccineBoosterAgeDuration).opposite(),
              let vaccineBoosterAge = Calendar.current.date(byAdding: components, to: Date()) else {
                  throw PassVerifierError.webServiceDataBroken
              }
        return birthDate < vaccineBoosterAge
    }
}

extension PassVerifierService: PublicKeyStorageDelegate {
    public func getEncodedPublicKeys(for kid: String) -> [String] {
        guard let certificateString = CertificatesManager.shared.dccCertificates?[kid] else { return [] }
        guard let base64Data = Data(base64Encoded: certificateString) else { return [] }
        guard let base64Decoded = String(data: base64Data, encoding: .utf8) else { return [] }
        return [base64Decoded]
    }
}

extension PassVerifierService {
    
    typealias TestOrRecoveryConfigurationChecker = (
        recoveryAcceptanceAgePeriod: String,
        recoveryDelayMaxTV: Int,
        recoveryDelayMax: Int,
        recoveryDelayMaxUnderAge: Int
    )
    
    func checkNegativeTestOrRecovery(
        today: Date,
        samplingDate: Date,
        startDate: Date,
        configuration: TestOrRecoveryConfigurationChecker,
        certificate: HCert,
        certField: String,
        certFieldIssuer: String
    ) -> (CertificateState, DataTypeValidation) {
        let components = (try? DateComponents(ISO8601String: configuration.recoveryAcceptanceAgePeriod).opposite()) ?? DateComponents()
        let recoveryAcceptanceAgeDate = Calendar.current.date(byAdding: components, to: Date()) ?? Date()
        
        let recoveryDelayMaxUnderAgeDate = samplingDate.roundingToEndOfDay().dateByAddingDays(configuration.recoveryDelayMaxUnderAge)
        let activationDate = startDate.roundingToBeginningOfDay()
        let birth = certificate.body["dob"].string ?? ""
        let birthDate = (birth.formatBirthDayStringToDCCDate() ?? Date()).roundingToBeginningOfDay()
        
        if birthDate <= recoveryAcceptanceAgeDate {
            let recoveryDelayMaxTVDate = samplingDate.roundingToEndOfDay().dateByAddingDays(configuration.recoveryDelayMaxTV)
            let recoveryDelayMaxDate = samplingDate.roundingToEndOfDay().dateByAddingDays(configuration.recoveryDelayMax)

            if certificate.body[certField][0][certFieldIssuer].string?.lowercased().caseInsensitiveHasPrefix("TV") ?? false && activationDate <= today && today <= recoveryDelayMaxTVDate {
                return(.valid, .valid)
            } else if activationDate <= today && today <= recoveryDelayMaxDate {
                return(.valid, .valid)
            }
        } else if birthDate > recoveryAcceptanceAgeDate && activationDate <= today && today <= recoveryDelayMaxUnderAgeDate {
            return (.valid, .valid)
        }
        return (.nonValid, .invalid)
    }
}
