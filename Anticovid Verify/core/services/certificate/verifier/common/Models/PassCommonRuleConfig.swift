//
//  PassCommonRuleConfig.swift
//  Anticovid Verify
//

import Foundation

protocol PassCommonRulesConfig: Codable {
    var recoveryStartDay: Int { get }
    var recoveryEndDay: Int { get }
    var vaccineBoosterDelayUnderAgeNew: Int { get }
    var vaccineBoosterDelayNew: Int { get }
    var vaccineBoosterAgePeriod: DateComponents { get }
}
