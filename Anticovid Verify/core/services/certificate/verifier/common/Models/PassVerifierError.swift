//
//  PassVerifierError.swift
//  Anticovid Verify
//

import Foundation

enum PassVerifierError: String, Error {
    case webServiceDataBroken
}
