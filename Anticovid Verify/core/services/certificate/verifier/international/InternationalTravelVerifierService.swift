//
//  InternationalTravelVerifierService.swift
//  Anticovid Verify
//

import Foundation
import SwiftDGC
import CertLogic
import OSLog

protocol InternationalTravelDeparture: AnyObject {
    func handleDeparture(with data: GoOutData) -> InternationalTravelVerifierResult
    func handleIsDepartureCountryStatusGreen(_ country: Country) -> Bool
    func handleDepartureCountryNotGreen(departureCountry: Country,
                                        destinationCountry: Country) -> OtModeCountryResult
    func handleDepartureCountryGreen(departureCountry: Country,
                                        destinationCountry: Country) -> OtModeCountryResult
    func handleHasRulesForDeparture(withDestinationCountry destinationCountry: Country) -> OtModeCountryResult?
    func handleHasRulesForDeparture(withDepartureCountry departureCountry: Country,
                                 andDestinationCountry destinationCountry: Country) -> OtModeCountryResult?
    func handleHasRuleForDestinationPartOfFrance(departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult?
}

protocol InternationalTravelArrival: AnyObject {
    func handleArrival(with data: GoInData) -> InternationalTravelVerifierResult
    func handleIsDepartureCountryInternational(_ departureCountry: Country) -> Bool
    func handleDepartureCountryNotInternational(departureCountry: Country, destinationCountry: Country,
                                                countriesInCurrentResult: [Country]) -> [OtModeCountryResult]
    func handleDepartureCountryInternational(destinationCountry: Country,
                                             departureCountry: Country,
                                             countriesInCurrentResult: [Country]) -> [OtModeCountryResult]

    func handleAddGenericCountryColorToResultIfNeeded(withCountriesInResult countries: [Country], departureCountry: Country, destinationCountry: Country) -> [OtModeCountryResult]
    func handleHasRulesForArrival(withDepartureCountry departureCountry: Country,
                                 andDestinationCountry destinationCountry: Country) -> OtModeCountryResult?
    func handleHasRuleForDeparturePartOfFrance(departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult?
}

struct InternationalTravelVerifierResult {
    let resultForCountry: [OtModeCountryResult]
}

struct OtModeCountryResult: Hashable {
    let country: Country
    let result: Result
    let validations: [ValidationResult]
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(country)
    }
}

class InternationalTravelVerifierService {
    static let shared = InternationalTravelVerifierService()
    
    let barcodeService: BarcodeService
    let ruleMotorRepo: RulesMotorRepository
    let certLogicMotorManager: CertLogicMotorManager
    let certLogicMotorRepository: CertLogicMotorRepository
    let premiumManager: PremiumManager
    
    private var date: Date!
    private var cert: HCert!
    private let allFranceCountryCode = "_F"
    private let internationalCountrycode = "Inter"
    private let otherCountryCode = "Autre"
    
    static let greenCountry = Country(name: "Pays Vert", nameCode: "", isNational: false)
    static let orangeCountry = Country(name: "Pays Orange", nameCode: "_O", isNational: false)
    static let redCountry = Country(name: "Pays Rouge", nameCode: "_R", isNational: false)
    
    init(barcodeService: BarcodeService = .shared,
         certLogicMotorManager: CertLogicMotorManager = .shared,
         certLogicMotorRepository: CertLogicMotorRepository = .shared,
         premiumManager: PremiumManager = .shared,
         ruleMotorRepo: RulesMotorRepository = .shared) {
        self.barcodeService = barcodeService
        self.certLogicMotorManager = certLogicMotorManager
        self.certLogicMotorRepository = certLogicMotorRepository
        self.premiumManager = premiumManager
        self.ruleMotorRepo = ruleMotorRepo
    }
    
    lazy var countriesSituation: [Country: CountrySituationStatus] = {
        let countriesNotGreen = ruleMotorRepo.getMotorRules()?.notGreenCountries ?? []
        return CountriesManager.shared.countries.reduce([Country: CountrySituationStatus]()) { partialResult, current  in
            var result = partialResult
            let isCountryNotGreen = countriesNotGreen.contains(current.nameCode)
            result[current] = isCountryNotGreen ? .red : .green
            return result
        }
    }()
    
    /**
     Call this functions to verify a pass for multiples country destinations.
     The result is nil if the selected mode isn't OT
     */
    func verify(_ certificate: HCert,
                withSelection selection: OtModeSelection,
                date: Date = Date()) -> InternationalTravelVerifierResult? {
        guard premiumManager.modeSelected == .OT else {
            return nil
        }
        
        var result: InternationalTravelVerifierResult?

        let date = Date()
        var date2: Date
        os_log("%@",type: .info, "🚗 \(NSStringFromClass(type(of: self))) will verify pass with current date: \(date.dateTimeString)")
        
        self.date = date
        cert = certificate
        switch selection {
        case .departure(let data):
            result = handleDeparture(with: data)
        case .arrival(let data):
            result = handleArrival(with: data)
        }
        
        date2 = Date()
        os_log("%@",type: .info, "🚗 \(NSStringFromClass(type(of: self))) did verify pass with current date: \(date2.dateTimeString) ...took \(date.distance(to: date2)) seconds")
        
        return result
    }
}

// MARK: Departure use case
extension InternationalTravelVerifierService: InternationalTravelDeparture {
    func handleDeparture(with data: GoOutData) -> InternationalTravelVerifierResult {
        let resultForCountry = data.favoritesCountries.map { destinationCountry -> OtModeCountryResult in
            let result: OtModeCountryResult
            
            if !handleIsDepartureCountryStatusGreen(data.departureCountry) {
                result = handleDepartureCountryNotGreen(departureCountry: data.departureCountry,
                                                        destinationCountry: destinationCountry)
            } else if let res = handleHasRuleForDestinationPartOfFrance(departureCountry: data.departureCountry, destinationCountry: destinationCountry) {
                result = res
            } else {
                result = handleDepartureCountryGreen(departureCountry: data.departureCountry,
                                                     destinationCountry: destinationCountry)
            }
            
            return result
        }
        
        return .init(resultForCountry: resultForCountry)
    }
    
    func handleIsDepartureCountryStatusGreen(_ country: Country) -> Bool {
        guard country.nameCode.lowercased() != internationalCountrycode.lowercased(),
              country.nameCode.lowercased() != otherCountryCode else {
                  return false
              }
                
        return countriesSituation[country] == .green
    }
    
    func handleDepartureCountryNotGreen(departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult {
        
        var result: OtModeCountryResult
        
        if let res = handleHasRulesForDeparture(withDepartureCountry: departureCountry,
                                    andDestinationCountry: destinationCountry) {
            result = res
        } else if let res = handleHasRuleForDestinationPartOfFrance(departureCountry: departureCountry, destinationCountry: destinationCountry) {
            result = res
        } else {
            result = OtModeCountryResult(country: destinationCountry,
                                         result: .open,
                                         validations: [])
        }
        
        return result
    }
    
    func handleDepartureCountryGreen(departureCountry: Country,
                                     destinationCountry: Country) -> OtModeCountryResult {
        var result: OtModeCountryResult
        if let res = handleHasRulesForDeparture(withDepartureCountry: departureCountry,
                                    andDestinationCountry: destinationCountry) {
            result = res
        } else if let res = handleHasRuleForDestinationPartOfFrance(departureCountry: departureCountry, destinationCountry: destinationCountry) {
            result = res
        } else if let res = handleHasRulesForDeparture(withDestinationCountry: destinationCountry) {
            result = res
        } else {
            result = OtModeCountryResult(country: destinationCountry,
                                         result: .open,
                                         validations: [])
        }
        
        return result
    }
    
    func handleHasRulesForDeparture(withDepartureCountry departureCountry: Country,
                                 andDestinationCountry destinationCountry: Country) -> OtModeCountryResult? {
        let rules = certLogicMotorRepository.getRules(forDepartureCountry: departureCountry.nameCode, destinationCountry: destinationCountry.nameCode)
        
        guard !rules.isEmpty else {
            return nil
        }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: destinationCountry.nameCode.uppercased(), date: date, with: rules)
        
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: destinationCountry,
                                   result: result.0,
                                   validations: result.1)
    }
    
    func handleHasRulesForDeparture(withDestinationCountry destinationCountry: Country) -> OtModeCountryResult? {
        let rules = certLogicMotorRepository.getRules(for: destinationCountry.nameCode)
        
        guard !rules.isEmpty else { return nil }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: destinationCountry.nameCode.uppercased(), date: date, with: rules)
        
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: destinationCountry,
                                   result: result.0,
                                   validations: result.1)
    }

    func handleHasRuleForDestinationPartOfFrance(departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult? {
        guard destinationCountry.isNational else { return nil }
        
        let rules = certLogicMotorRepository.getRules(forDepartureCountry: departureCountry.nameCode, destinationCountry: allFranceCountryCode)
        
        guard !rules.isEmpty else { return nil }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: allFranceCountryCode, date: date, with: rules)
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: destinationCountry,
                                   result: result.0,
                                   validations: result.1)
    }
}

// MARK: Arrival use case
extension InternationalTravelVerifierService: InternationalTravelArrival {
    func handleArrival(with data: GoInData) -> InternationalTravelVerifierResult {
        let resultForCountry = data.favoritesCountries.reduce([OtModeCountryResult]()) { partialResult, departureCountry in
            var newResultElement = partialResult
            let countriesInCurrentResult = partialResult.map { $0.country }

            
            if !handleIsDepartureCountryInternational(departureCountry){
                let res = handleDepartureCountryNotInternational(departureCountry: departureCountry,
                                                                 destinationCountry: data.destinationCountry,
                                                                 countriesInCurrentResult: countriesInCurrentResult)
                newResultElement.append(contentsOf: res)
            } else {
                let res = handleDepartureCountryInternational(destinationCountry: data.destinationCountry, departureCountry: departureCountry, countriesInCurrentResult: countriesInCurrentResult)
                newResultElement.append(contentsOf: res)
            }
            
            return newResultElement
        }
        
        return .init(resultForCountry: resultForCountry)
    }
    
    func handleIsDepartureCountryInternational(_ departureCountry: Country) -> Bool {
        departureCountry.nameCode == internationalCountrycode
    }
    
    func handleDepartureCountryNotInternational(departureCountry: Country, destinationCountry: Country,
                                                countriesInCurrentResult: [Country]) -> [OtModeCountryResult] {
        var result = [OtModeCountryResult]()

        guard !handleIsDepartureCountryInternational(departureCountry) else {
            return []
        }
        
        if let res = handleHasRulesForArrival(withDepartureCountry: departureCountry,
                                              andDestinationCountry: destinationCountry) {
            result = [res]
        } else if let res = handleHasRuleForDeparturePartOfFrance(departureCountry: departureCountry,
                                                                    destinationCountry: destinationCountry) {
            result = [res]
        } else {
            result = handleAddGenericCountryColorToResultIfNeeded(withCountriesInResult: countriesInCurrentResult, departureCountry: departureCountry, destinationCountry: destinationCountry)
        }
        
        return result
    }
    
    func handleAddGenericCountryColorToResultIfNeeded(withCountriesInResult countriesAlreadyInFinalResult: [Country],
                                                      departureCountry: Country, destinationCountry: Country) -> [OtModeCountryResult] {
        guard !countriesAlreadyInFinalResult.containsAtleastOneOf([Self.greenCountry, Self.orangeCountry, Self.redCountry]) else {
            return []
        }
        
        // Start validation for colored countries (not done yet)
        var resultForCountry = Set<OtModeCountryResult>()
        var resultForCountryWithDefaultResult = Set<OtModeCountryResult>() // if no result for specific country this set replace the nil value by a default value
        
        // validates for colored countries
        // green country
        let result = handleHasRulesForArrival(withDepartureCountry: Self.greenCountry,
                                              andDestinationCountry: destinationCountry)
        
        let defaultResult = OtModeCountryResult(country: Self.greenCountry, result: .open, validations: [])
        
        if let result = result {
            resultForCountry.insert(result)
        }
        resultForCountryWithDefaultResult.insert(result ?? defaultResult)
        
        // orange country
        let result2 = handleHasRulesForArrival(withDepartureCountry: Self.orangeCountry,
                                               andDestinationCountry: destinationCountry)
        
        let defaultResult2 = OtModeCountryResult(country: Self.orangeCountry, result: .open, validations: [])
        
        if let result2 = result2 {
            resultForCountry.insert(result2)
        }
        resultForCountryWithDefaultResult.insert(result2 ?? defaultResult2)
        
        // red country
        let result3 = handleHasRulesForArrival(withDepartureCountry: Self.redCountry,
                                               andDestinationCountry: destinationCountry)
        
        let defaultResult3 = OtModeCountryResult(country: Self.redCountry, result: .open, validations: [])
        
        if let result3 = result3 {
            resultForCountry.insert(result3)
        }
        resultForCountryWithDefaultResult.insert(result3 ?? defaultResult3)
        
        // handle case where there is no result for the 3 colored countries
        if resultForCountry.isEmpty {
            resultForCountry.insert(OtModeCountryResult(country: departureCountry, result: .open, validations: []))
        } else {
            resultForCountry = resultForCountryWithDefaultResult
        }
        
        return Array(resultForCountry).sortedWithColoredCountriesLast()
    }
    
    func handleDepartureCountryInternational(destinationCountry: Country,
                                             departureCountry: Country,
                                             countriesInCurrentResult: [Country]) -> [OtModeCountryResult] {
        handleAddGenericCountryColorToResultIfNeeded(withCountriesInResult: countriesInCurrentResult,
                                                     departureCountry: departureCountry,
                                                     destinationCountry: destinationCountry)
    }
    
    func handleHasRuleForArrival(withDepartureCountry departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult? {
        let rules = certLogicMotorRepository.getRules(forDepartureCountry: departureCountry.nameCode, destinationCountry: destinationCountry.nameCode)
        
        guard !rules.isEmpty else {
            return nil
        }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: destinationCountry.nameCode.uppercased(), date: date, with: rules)
        
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: departureCountry,
                                   result: result.0,
                                   validations: result.1)
    }
    
    /**
     Filter cert logic result by checking the response with priority given to fail, passed then open result.
     Open means blue response (no rules applied), fail is a red response (rules failed) and passed is a green response (rules passed).
     */
    func handleCertLogicResult(_ validations: [ValidationResult]) -> (Result, [ValidationResult]) {
        let result: Result
        
        if validations.contains(where: { $0.result == .fail }) {
            result = .fail
        } else if validations.contains(where: { $0.result == .passed }) {
            result = .passed
        } else {
            result = .open
        }
        
        let filteredValidations = validations.filter { $0.result == result }
        
        return (result, filteredValidations)
    }
    
    func handleHasRulesForArrival(withDepartureCountry departureCountry: Country,
                                 andDestinationCountry destinationCountry: Country) -> OtModeCountryResult? {
        let rules = certLogicMotorRepository.getRules(forDepartureCountry: departureCountry.nameCode, destinationCountry: destinationCountry.nameCode)
        
        guard !rules.isEmpty else {
            return nil
        }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: destinationCountry.nameCode.uppercased(), date: date, with: rules)
        
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: departureCountry,
                                   result: result.0,
                                   validations: result.1)
    }
    
    func handleHasRuleForDeparturePartOfFrance(departureCountry: Country, destinationCountry: Country) -> OtModeCountryResult? {
        guard departureCountry.isNational else { return nil }
        
        let rules = certLogicMotorRepository.getRules(forDepartureCountry: allFranceCountryCode, destinationCountry: destinationCountry.nameCode)
        
        guard !rules.isEmpty else { return nil }
        
        let validations = certLogicMotorManager.validate(certificate: cert, country: destinationCountry.nameCode, date: date, with: rules)
        let result = handleCertLogicResult(validations)
        
        return OtModeCountryResult(country: departureCountry,
                                   result: result.0,
                                   validations: result.1)
    }
}

extension Sequence where Iterator.Element : Equatable {
    func containsAtleastOneOf(_ elements: [Iterator.Element]) -> Bool {
        !elements.map { self.contains($0) }.contains(false)
    }
}

extension Sequence where Iterator.Element == OtModeCountryResult {
    func filterColoredCountries() -> [OtModeCountryResult] {
        let greenCountry = InternationalTravelVerifierService.greenCountry
        let orangeCountry = InternationalTravelVerifierService.orangeCountry
        let redCountry = InternationalTravelVerifierService.redCountry
        
        return filter { result in
            result.country == greenCountry ||
            result.country == orangeCountry ||
            result.country == redCountry
        }
    }
    
    func sortedWithColoredCountriesLast() -> [OtModeCountryResult] {
        let greenCountry = InternationalTravelVerifierService.greenCountry
        let orangeCountry = InternationalTravelVerifierService.orangeCountry
        let redCountry = InternationalTravelVerifierService.redCountry

        return sorted(by: { first, second in
            switch first.country {
            case InternationalTravelVerifierService.greenCountry:
                switch second.country {
                case orangeCountry, redCountry:
                    return true
                default:
                    return false
                }
            case orangeCountry:
                switch second.country {
                case redCountry:
                    return true
                case greenCountry:
                    return false
                default:
                    return false
                }
            case redCountry:
                switch second.country {
                case greenCountry, orangeCountry:
                    return false
                default:
                    return true
                }
            default:
                return true
            }
        })
    }
}
