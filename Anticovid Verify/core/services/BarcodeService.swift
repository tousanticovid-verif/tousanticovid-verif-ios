//
//  BarcodeVerifier.swift
//  Anticovid Verify
//
//

import Foundation
import SwiftDGC
import SwiftJWT
import OrderedCollections
import os.log

typealias BarcodeResult = (
    object: BarcodeProtocolObject?,
    state: CertificateState?,
    adminState: CertificateAdminState?,
    
    error: BarcodeError?
)

enum BarcodeError {
    case wrongDocument
}

protocol BarcodeProtocolObject {
    func getUIInformation(result: CertificateState, shouldShowFullResult: Bool) -> OrderedDictionary<String, [FieldRow]>
}

struct OTModeUnlockConfiguration {
    let expirationDate: Date
    let siren: String
    let code: String
}

class BarcodeService {
    
    static let shared = BarcodeService()
    
    private let statService: StatService
    private let userData: UserDataManager
    private let certificateAdminCheckerService: CertificateAdminCheckerService
    private let premiumManager: PremiumManager
    
    init(statService: StatService = .shared,
         userData: UserDataManager = .sharedInstance,
         certificateAdminCheckerService: CertificateAdminCheckerService = .shared,
         premiumManager: PremiumManager = .shared) {
        self.statService = .shared
        self.userData = userData
        self.certificateAdminCheckerService = certificateAdminCheckerService
        self.premiumManager = premiumManager
    }
    
    func analyze(barcode: String, forValidityAt date: Date) -> BarcodeResult {
        guard let (verifierResult, barcodeProtocolObject, isDCC) = analyzeValidity(barcode: barcode,
                                                                            forValidityAt: date) else {
            return (nil, nil, nil, .wrongDocument)
        }
        
        os_log(.info, "%@", "Starting to fetch blacklist information at \(Date().fullDateTimeFormatted())")
        
        let certificateAdminState = certificateAdminCheckerService
            .getCertificateAdminState(for: verifierResult.certHash, isDCC: isDCC)
        
        os_log(.info, "%@", "Finished fetching blacklist information at \(Date().fullDateTimeFormatted())")

        statService.proceedToStatTreatment(
            statInformation: verifierResult.statInformation,
            certificateAdminState: certificateAdminState,
            certHash: verifierResult.certHash
        )
        
        return BarcodeResult(
            barcodeProtocolObject,
            certificateAdminState == nil ? verifierResult.certificateState : .blacklisted,
            certificateAdminState,
            nil
        )
    }
    
    func isValidPassCovid2DDoc(barcode: String) -> Bool {
        if barcode.count < 22 {
            return false
        }
        let start = barcode.index(barcode.startIndex, offsetBy: 20)
        let end = barcode.index(barcode.startIndex, offsetBy: 22)
        let documentType = String(barcode[start..<end])
        return ConstConf.passCovidDocumentTypes.contains(documentType)
    }
    
    func isValidQrCodeToUnlockOTMode(qrcode: String) -> OTModeUnlockConfiguration? {
        guard let publicKey = Data(base64Encoded: ConstConf.publicKey) else {
            return nil
        }
        let jwtVerifier = JWTVerifier.rs256(publicKey: publicKey)
        let jwtDecoder = JWTDecoder(jwtVerifier: jwtVerifier)
        do {
            let jwt = try jwtDecoder.decode(JWT<INClaims>.self, fromString: qrcode)
            let validation = jwt.validateClaims()
            guard validation == .success else {
                return nil
            }
            
            let info = jwt.claims.realm_access?.contains(where: { (key: String, value: [String]) in
                return key == "roles" && value.contains("ROLE_TACV_CONTROL_OT")
            }) ?? false ? jwt : nil
            
            guard let expirationDate = info?.claims.exp, let siren = info?.claims.siren else {
                return nil
            }
            
            return .init(expirationDate: expirationDate, siren: siren, code: qrcode)
        }
        catch {
            os_log("%@",type: .error, error.localizedDescription)
        }
        
        return nil
    }
    
    private func analyzeValidity(barcode: String,
                                 forValidityAt date: Date) -> (VerifierResult, BarcodeProtocolObject, Bool)? {
        var verifierResult: VerifierResult?
        var barcodeProtocolObject: BarcodeProtocolObject!
        var isDCC = false
        
        if let certificate = try? WalletManager.shared.extractCertificateFrom(doc: barcode),
        let object = certificate as? BarcodeProtocolObject {
            barcodeProtocolObject = object
            switch premiumManager.modeSelected {
            case .tacVerif(let passSelected, _):
                switch passSelected {
                case .vaccine:
                    verifierResult = VaccinePassVerifierService.shared.verify2DDOC(certificate: certificate, for: date)
                case .health:
                    verifierResult = HealthPassVerifierService.shared.verify2DDOC(certificate: certificate, for: date)
                }
            case .tacVerifPlus, .OT:
                verifierResult = DetailedResultVerifierService.shared.verify2DDOC(certificate: certificate, for: date)
            }
        } else if let certificate = HCert(from: barcode) {
            isDCC = true
            barcodeProtocolObject = certificate
            
            switch premiumManager.modeSelected {
            case .tacVerif(let passSelected, _):
                switch passSelected {
                case .vaccine:
                    verifierResult = VaccinePassVerifierService.shared.verifyDCC(certificate: certificate, for: date)
                case .health:
                    verifierResult = HealthPassVerifierService.shared.verifyDCC(certificate: certificate, for: date)
                }
            case .tacVerifPlus, .OT:
                verifierResult = DetailedResultVerifierService.shared.verifyDCC(certificate: certificate, for: date)
            }
        }
        
        guard let verifierResult = verifierResult else {
            return nil
        }
        
        return (verifierResult, barcodeProtocolObject, isDCC)
    }
}
