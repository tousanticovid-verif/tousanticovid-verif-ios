//
//  StatService.swift
//  Anticovid Verify
//
//

import Foundation
import INCommunication
import INCore
import Combine

class StatService {
    
    static let shared = StatService()
    
    private let premiumManager: PremiumManager
    
    private let onlineStatService: OnlineStatRepository
    
    private let localStatService: LocalStatRepository
    
    init(onlineStatService: OnlineStatRepository = .shared, localStatService: LocalStatRepository = .shared, premiumManager: PremiumManager = .shared) {
        self.onlineStatService = onlineStatService
        self.localStatService = localStatService
        self.premiumManager = premiumManager
    }

    func proceedToStatTreatment(statInformation: StatInformation, certificateAdminState: CertificateAdminState?,  certHash: String) {
        let onlineStatData = OnlineStatData(
            globalValidity: statInformation.globalValidity,
            controlType: statInformation.controlType,
            certificateAdminState: certificateAdminState?.rawValue ?? "",
            siren: premiumManager.siren,
            ressourceType: statInformation.ressourceType,
            result: statInformation.result,
            resultInfo: statInformation.resultInfo
        )
        let localStatData = LocalStatData(identifiant: certHash, typeValidation: statInformation.globalValidity, certificateAdminState: certificateAdminState)
        
        try? localStatService.removeOldData()
        
        _ = localStatService.write(stat: localStatData)
        _ = onlineStatService.write(stat: onlineStatData)
    }
    
    func sendStat() -> AnyPublisher<Void, Error> {
        let logs = onlineStatService.getStatistics().compactMap({ $0.value })
        
        guard !logs.isEmpty else {
            return Just(())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        
        let service = ServiceRouter.statistic(logs)
        
        return INRequestService.shared.executeRequest(service, type:  EmptyBody.self)
            .map { [weak self] _ in
                guard let self = self else {
                    return
                }
                
                _ = self.onlineStatService.deleteAllStatistics()
            }
            .eraseToAnyPublisher()
    }
    
    func readStat() -> ([OnlineStatistic], [LocalStatistic]) {
        return (onlineStatService.getStatistics(), localStatService.getStatistics())
    }
    
    func eraseAllData() {
        _ = self.onlineStatService.deleteAllStatistics()
        _ = self.localStatService.deleteAllStatistics()
    }
}
