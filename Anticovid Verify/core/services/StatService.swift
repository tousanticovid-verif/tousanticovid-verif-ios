//
//  StatService.swift
//  Anticovid Verify
//
//  Created by Jean-Philippe DESCAMPS on 23/07/2021.
//

import Foundation

class StatService {
    
    static let SEPARATOR = "|"
    static let TACV_2DDOC_IOS_LITE = "TACV_2DDOC_IOS_LITE"
    static let TACV_2DDOC_IOS_OT = "TACV_2DDOC_IOS_OT"
    static let D_DOC_B2 = "2D-DOC_B2"
    static let D_DOC_L1 = "2D-DOC_L1"
    static let DCC_TEST = "DCC_TEST"
    static let DCC_VACCINATION = "DCC_VACCINATION"
    static let DCC_RECOVERY = "DCC_RECOVERY"
    
    static let shared = StatService()
    
    private let onlineStatService: OnlineStatService
    
    private let localStatService: LocalStatService
    
    init(onlineStatService: OnlineStatService = .shared, localStatService: LocalStatService = .shared) {
        self.onlineStatService = onlineStatService
        self.localStatService = localStatService
    }

    func write(stat: Stat) {
        onlineStatService.writeStat(stat: stat)
        
    }
}
