//
//  BackgroundTaskManager.swift
//  Anticovid Verify
//

import Foundation
import BackgroundTasks
import os.log
import Combine

class BackgroundTaskManager {
    static let shared = BackgroundTaskManager()
    
    private let backgroundTaskIdentifier = "com.ingroupe.verify.anticovid.task.background"
    private var cancellables = Set<AnyCancellable>()
    
    private init() { }
    
    func register() {
        BGTaskScheduler.shared.register(forTaskWithIdentifier: backgroundTaskIdentifier,
                                        using: .global(qos: .background),
                                        launchHandler: handleBackgroundTask(_:))
    }
    
    func handleBackgroundTask(_ task: BGTask) {
        task.expirationHandler = { task.setTaskCompleted(success: false) }
        SyncManager.shared.start()
            .sink(receiveCompletion: {
                self.scheduleBackgroundRefresh()
                
                switch $0 {
                case .failure(let error):
                    os_log("%@",type: .error, "baseConfiguration request failed: \(error)")
                    task.setTaskCompleted(success: false)
                    
                case .finished:
                    task.setTaskCompleted(success: true)
                }
            })
            .store(in: &cancellables)
    }    
    
    func scheduleBackgroundRefresh() {
        let request = BGAppRefreshTaskRequest(identifier: backgroundTaskIdentifier)
        request.earliestBeginDate = Date(timeIntervalSinceNow: 5 * 60)
//        request.earliestBeginDate = Date(timeIntervalSinceNow: ConfDataManager.shared.remindFrequency * 3600)

        do {
            try BGTaskScheduler.shared.submit(request)
        } catch {}
    }
}
