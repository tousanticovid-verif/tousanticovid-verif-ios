//
//  LocalStatistic+Ext.swift
//  Anticovid Verify
//
//

import Foundation

extension LocalStatistic {
    
    func initialize(date: Date) {
        self.date = date
        self.valid = 0
        self.invalid = 0
        self.toDecide = 0
        self.double = 0
    }
    
    func calculateNumberOfScan() -> Int {
        return (self.valid + self.invalid + self.toDecide).toInt()
    }
}
