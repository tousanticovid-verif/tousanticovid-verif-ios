//
//  CertLogicRulesMotor+Ext.swift
//  Anticovid Verify
//
//

import Foundation
import CertLogic
import SwiftyJSON

extension CertLogicRulesMotor {
    
    func initialize(rule: CertLogic.Rule) throws {
        let descriptions = rule.description.map { description -> CertLogicRuleDescription in
            let object = CertLogicRuleDescription(context: managedObjectContext!)
            object.desc = description.desc
            object.lang = description.lang
            object.rule = NSSet(object: self)
            
            return object
        }
        self.hashStringValue = rule.hash
        self.identifier = rule.identifier
        self.type = rule.type
        self.version = rule.version
        self.schemaVersion = rule.schemaVersion
        self.engine = rule.engine
        self.engineVersion = rule.engineVersion
        self.certificateType = rule.certificateType
        self.descriptions = NSSet.init(array: descriptions)
        self.validFrom = rule.validFrom
        self.validTo = rule.validTo
        self.affectedString = rule.affectedString
        self.logic = try rule.logic.rawData()
        self.countryCode = rule.countryCode
        self.region = (rule.region?.isEmpty ?? true) ? nil : rule.region
        // transform region "" to nil in order to have Android's logic
        self.modifier = rule.modifier?.rawValue
    }
    
    func toCertLogicRule() throws -> CertLogic.Rule {
        guard let logic = logic else {
            throw SyncError.ruleHasNoLogic
        }
        
        let descriptions = (descriptions?.allObjects as? [CertLogicRuleDescription] ?? [])
            .map { Description(lang: $0.lang ?? "", desc: $0.desc ?? "")}

        let rule = CertLogic.Rule(
            identifier: self.identifier ?? "",
            type: self.type ?? "",
            version: self.version ?? "",
            schemaVersion: self.schemaVersion ?? "",
            engine: self.engine ?? "",
            engineVersion: self.engineVersion ?? "",
            certificateType: self.certificateType ?? "",
            description: descriptions,
            validFrom: self.validFrom ?? "",
            validTo: self.validTo ?? "",
            affectedString: self.affectedString ?? [],
            logic: try JSON(data: logic),
            countryCode: self.countryCode  ?? "",
            region: region)
       
        rule.hash = hashStringValue
        
        if let modifier = self.modifier {
            rule.modifier = .init(rawValue: modifier)
        }
        
        return rule
    }
}

extension Array where Element == Rule {
    func contains(modifier: Modifier) -> Bool {
        contains { rule in
            rule.modifier == modifier
        }
    }
}
