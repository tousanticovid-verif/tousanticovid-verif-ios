//
//  CertLogicValueSetIdentifier+Ext.swift
//  Anticovid Verify
//

import Foundation
import CoreData
import CertLogic

extension CertLogicValueSetIdentifier {
    convenience init(context: NSManagedObjectContext, valueSetBase: ValueSetBaseResponse) {
        self.init(context: context)
        valueSetId = valueSetBase.id
        valueSetHash = valueSetBase.hash
    }
    
    func mapToValueSetBaseResponse() -> ValueSetBaseResponse {
        ValueSetBaseResponse(id: valueSetId ?? "", hash: valueSetHash ?? "")
    }
}
