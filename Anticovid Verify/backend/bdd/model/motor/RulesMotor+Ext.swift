//
//  RulesMotor+Ext.swift
//  Anticovid Verify
//
//

import Foundation

extension RulesMotor {
    
    func initialize(notGreenCountries: [String], validity: Validity, specificValueList: SpecificValueList) throws {
        self.notGreenCountries = notGreenCountries
        
        self.vaccinePassStartDate = validity.vaccinePassActivationDate
        self.healthPassConfiguration = try JSONEncoder().encode(validity.health)
        self.vaccinePassConfiguration = try JSONEncoder().encode(validity.vaccine)
        self.plusValidityConfiguration = try JSONEncoder().encode(validity.plus)
        
        // LIST - TEST
        self.testAntigenicList = specificValueList.testAntigenicList
        self.testPcrList = specificValueList.testPcrList
        self.testManufacturerList = specificValueList.testManufacturerList

        // LIST - Vaccin
        self.vaccineProphylaxisList = specificValueList.vaccineProphylaxisList
        self.vaccineManufacturerList = specificValueList.vaccineManufacturerList
        self.vaccineMedicalProductList = specificValueList.vaccineMedicalProductList
    }
}
