//
//  CertLogicRuleIdentifier+Ext.swift
//  Anticovid Verify
//

import Foundation
import CoreData

extension CertLogicRuleIdentifier {
    convenience init(context: NSManagedObjectContext, ruleId: RuleBaseResponse) {
        self.init(context: context)
        identifier = ruleId.identifier
        version = ruleId.version
        ruleHash = ruleId.hash
        country = ruleId.country
    }
    
    func mapToRuleIdResponse() -> RuleBaseResponse {
        RuleBaseResponse(identifier: identifier ?? "", version: version ?? "",
                       country: country ?? "", hash: ruleHash ?? "")
    }
    
    var departureCountryCode: String? {
        guard isIdentifierFormatValid() else {
            return nil
        }
        
        return identifier?[3...4]
    }
    
    var destinationCountryCode: String? {
        if isIdentifierFormatValid()  {
            return identifier?[5...6]
        } else if isIdentifierFormatValidOnlyDestination() {
            return identifier?[3...4]
        } else {
            return nil
        }
    }
    
    private func isIdentifierFormatValid() -> Bool {
        identifier?.range(of: RuleIdentifierRegex.anyArguments.toString,
                          options: .regularExpression,range: nil, locale: nil) != nil
    }
    
    private func isIdentifierFormatValidOnlyDestination() -> Bool {
        identifier?.range(of: RuleIdentifierRegex.firstArgumentAny("").toString,
                          options: .regularExpression,range: nil, locale: nil) != nil
    }
}
