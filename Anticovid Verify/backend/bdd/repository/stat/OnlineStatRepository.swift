//
//  OnlineStatRepository.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import OSLog
import CoreData

class OnlineStatRepository {
    
    static let shared = OnlineStatRepository()
    
    let bddConfig: BDDConfig
    
    init(bddConfig: BDDConfig = .shared) {
        self.bddConfig = bddConfig
    }
    
    func deleteAllStatistics() -> Bool {
        let context = self.bddConfig.getStatContext()
        let fetchRequest: NSFetchRequest<OnlineStatistic> = OnlineStatistic.fetchRequest()
        guard let result = try? context.fetch(fetchRequest) else { return false }
        result.forEach { context.delete($0) }
        do {
            try context.save()
            return true
        } catch {
             os_log("%@",type: .error, "Could not delete online statistic: \(error)")
            return false
        }
    }

    func write(stat: OnlineStatData) -> Bool {
        let context = self.bddConfig.getStatContext()
        let onlineStatistic = OnlineStatistic(context: context)

        onlineStatistic.value = stat.toString()

        do {
            try context.save()
            return true
        } catch {
             os_log("%@",type: .error, "Could not save online statistic: \(error)")
            return false
        }
    }

    func getStatistics() -> [OnlineStatistic] {
        let context = self.bddConfig.getStatContext()
        let fetchRequest: NSFetchRequest<OnlineStatistic> = OnlineStatistic.fetchRequest()
        do {
            return try context.fetch(fetchRequest)
        } catch {
             os_log("%@",type: .error, "Could not fetch online statistics: \(error)")
            return []
        }
    }
}
