//
//  CertMotorRulesRepository.swift
//  Anticovid Verify
//
//

import Foundation
import CoreData
import OSLog

class CountryRepository {
    
    static let shared = CountryRepository()
    
    let bddConfig: BDDConfig
    
    init(bddConfig: BDDConfig = .shared) {
        self.bddConfig = bddConfig
    }
    
    func getAll() throws -> [Country] {
        var countries = [Country]()
        let context = self.bddConfig.getCountriesContext()
        try context.fetch(CountryDTO.fetchRequest()).forEach {
            let ctr = Country(name: $0.name!, nameCode: $0.nameCode!, isNational: $0.isNational, isArrival: $0.isArrival, isDeparture: $0.isDeparture, isFavorite: false)
            countries.append(ctr)
        }
        
        return countries
    }
    
    func deleteAll() throws {
        let context = self.bddConfig.getCountriesContext()

        try context.fetch(CountryDTO.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.save()
    }
    
    
    func save(countries: [Country]) throws {
        let context = self.bddConfig.getCountriesContext()

        countries.forEach { country in
            let ctr = CountryDTO(context: context)
            ctr.name = country.name
            ctr.nameCode = country.nameCode
            ctr.isArrival = country.isArrival
            ctr.isDeparture = country.isDeparture
            ctr.isNational = country.isNational
        }

        try context.save()

    }
}
