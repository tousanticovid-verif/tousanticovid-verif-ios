//
//  RulesMotorRepository.swift
//  Anticovid Verify
//
//

import Foundation
import CoreData
import OSLog

class RulesMotorRepository {

    static let shared = RulesMotorRepository()
    
    let bddConfig: BDDConfig
    
    init(bddConfig: BDDConfig = .shared) {
        self.bddConfig = bddConfig
    }
    
    @discardableResult
    public func deletePreviousRules() -> Bool {
        let context = self.bddConfig.getMotorContext()

        
        try? context.fetch(RulesMotor.fetchRequest()).forEach {
            context.delete($0)
        }
        
        do {
            try context.save()
            return true
        } catch let error {
            os_log("%@",type: .error, "Could not delete rules: \(error)")
            return false
        }
    }
    
    public func getMotorRules() -> RulesMotor? {
        let context = self.bddConfig.getMotorContext()
        let fetchRequest: NSFetchRequest<RulesMotor> = RulesMotor.fetchRequest()
        do {
            return try context.fetch(fetchRequest).last
        } catch {
             os_log("%@",type: .error, "Could not fetch rules: \(error)")
            return nil
        }
    }
    
    public func save(notGreenCountries: [String], validity: Validity,
                     specificValueList: SpecificValueList) throws -> RulesMotor {
        let context = self.bddConfig.getMotorContext()
        let rules = RulesMotor(context: context)
        
        try rules.initialize(notGreenCountries: notGreenCountries, validity: validity,
                         specificValueList: specificValueList)

        try context.save()
        return rules
    }
}
