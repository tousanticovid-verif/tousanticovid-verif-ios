//
//  BlackListRepository.swift
//  Anticovid Verify
//
//

import Foundation
import OSLog
import CoreData

protocol BlacklistStorable: NSManagedObject {
    var isActive: Bool { get }
}

extension BlacklistDCC: BlacklistStorable {}
extension Blacklist2DDOC: BlacklistStorable {}

class BlackListRepository {
    static let shared = BlackListRepository()
    
    let bddConfig: BDDConfig
    
    init(bddConfig: BDDConfig = .shared) {
        self.bddConfig = bddConfig
    }

    /**
     Return true if a certificate is blacklisted.
     Providing the type of the certificate (isDCC) helps the function return faster.
     - Parameters:
        - certHash: The hash of the certificate
        - isDCC: True if the certificate is DCC, false if 2DDOC
     */
    func isBlackListed(certHash: String, isDCC: Bool? = nil) -> Bool {
        if let isDCC = isDCC {
            let blacklist = isDCC ?
            lastBlacklist(matching: certHash, type: BlacklistDCC.self) :
            lastBlacklist(matching: certHash, type: Blacklist2DDOC.self)

            return blacklist?.isActive ?? false
        } else {
            let blacklistDCC = lastBlacklist(matching: certHash, type: BlacklistDCC.self)
            let blacklist2DDOC = lastBlacklist(matching: certHash, type: Blacklist2DDOC.self)

            return (blacklistDCC?.isActive ?? false) || (blacklist2DDOC?.isActive ?? false)
        }
    }
    
    // MARK: Saving
    /**
     Deletes old configuration then save new given configuration
     */
    func save(configuration: Blacklist) throws {
        try handleBlacklistConfigurationResponse(configuration)
        try deleteConfiguration()
        
        let context = self.bddConfig.getBlacklistContext()

        let entity = BlacklistConfiguration(context: context)
        entity.exclusionQuantityLimit = Int32(configuration.exclusionQuantityLimit)
        entity.lastIndexBlacklist2DDOC = Int32(configuration.lastIndexBlacklist2ddoc)
        entity.lastIndexBlacklistDCC = Int32(configuration.lastIndexBlacklist)
        entity.standbyDelay = Int16(configuration.standbyDelay)
        entity.refreshVersion = Int16(configuration.refreshVersion)
        
        entity.exclusionTimes = NSSet(array: configuration.exclusionTime.map {
            let exclusionEntity = ExclusionTime(context: context)
            exclusionEntity.start = Date.timeFormatter.date(from: $0.start)
            exclusionEntity.end = Date.timeFormatter.date(from: $0.end)
            
            return exclusionEntity
        })
        
        try context.save()
    }
    
    func save(blacklistDCCResponse: [BlacklistHashResponse]) throws -> Void {
        let context = self.bddConfig.getBlacklistContext()

        blacklistDCCResponse.forEach {
            let entity = BlacklistDCC(context: context)
            entity.certHash = $0.hash
            entity.id = $0.id
            entity.isActive = $0.active ?? true
        }

        try context.save()
    }
    
    func save(blacklist2DDOCResponse: [BlacklistHashResponse]) throws -> Void {
        let context = self.bddConfig.getBlacklistContext()

        blacklist2DDOCResponse.forEach {
            let entity = Blacklist2DDOC(context: context)
            entity.certHash = $0.hash
            entity.id = $0.id
            entity.isActive = $0.active ?? true
        }

        try context.save()
    }
    
    // MARK: Helper
    var configuration: BlacklistConfiguration? {
        get {
            let context = bddConfig.getBlacklistContext()
            
            do {
                return try context.fetch(BlacklistConfiguration.fetchRequest()).first
            } catch let error {
                os_log("%@",type: .error, "Could not fetch blacklist configuration: \(error)")
                return nil
            }
        }
    }
    
    func lastBlacklistDCCIndex() -> Int32 {
        let context = bddConfig.getBlacklistContext()
        
        let request = BlacklistDCC.fetchRequest()
        request.fetchLimit = 1
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        return (try? context.fetch(request).first?.id) ?? 0
    }
    
    func lastBlacklist2DDOCIndex() -> Int32 {
        let context = bddConfig.getBlacklistContext()
        
        let request = Blacklist2DDOC.fetchRequest()
        request.fetchLimit = 1
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        return (try? context.fetch(request).first?.id) ?? 0
    }
}

private extension BlackListRepository {
    // MARK: Deletion
    func deleteConfiguration() throws {
        let context = self.bddConfig.getBlacklistContext()
        
        try context.fetch(BlacklistConfiguration.fetchRequest()).forEach {
            context.delete($0)
        }
        
        try context.save()
    }
    
    // MARK: Helper
    func lastBlacklist<T: NSManagedObject>(matching certHash: String, type: T.Type) -> BlacklistStorable? {
        let context = bddConfig.getBlacklistContext()

        let fetchRequest: NSFetchRequest<T> = NSFetchRequest(entityName: NSStringFromClass(T.self))
        fetchRequest.predicate = NSPredicate(format: "certHash ==[c] %@", certHash)
        fetchRequest.fetchLimit = 1
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        
        do {
            return try context.fetch(fetchRequest).first as? BlacklistStorable
        } catch let error {
            os_log("%@",type: .error, "Could not fetch blacklist: \(error)")
            return nil
        }
    }
    
    /**
     Handle blacklist response by removing old data if necessary (refreshVersion)
     Calls to this function should be done before saving the response (to compare response with local old data)
     */
    func handleBlacklistConfigurationResponse(_ response: Blacklist) throws {
        guard let oldRefreshVersion = configuration?.refreshVersion,
              response.refreshVersion != oldRefreshVersion else {
                  return
              }
        
        if let refreshIndexDCC = response.refreshIndex {
            try deleteBlacklists(isDCC: true, startingIndex: refreshIndexDCC)
        }
        if let refreshIndex2DDOC = response.refreshIndex2ddoc {
            try deleteBlacklists(isDCC: false, startingIndex: refreshIndex2DDOC)
        }
    }
    
    func deleteBlacklists(isDCC: Bool, startingIndex: Int) throws {
        let context = bddConfig.getBlacklistContext()

        let type = isDCC ? BlacklistDCC.self : Blacklist2DDOC.self
        let fetchRequest = fetchRequest(type: type)
        fetchRequest.predicate = NSPredicate(format: "id >= %d", startingIndex)

        try context.fetch(fetchRequest).forEach {
            context.delete($0)
        }
        
        try context.save()
    }
    
    private func fetchRequest<T: NSManagedObject>(type: T.Type) -> NSFetchRequest<T> {
        NSFetchRequest(entityName: NSStringFromClass(type))
    }
}

