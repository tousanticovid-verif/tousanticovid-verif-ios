//
//  HealthPathValidity.swift
//  Anticovid Verify
//
import Foundation

struct HealthPassValidity: PassCommonRulesConfig, Codable {
    let testNegativePcrEndHour, testNegativeAntigenicEndHour, testNegativePrimoPcrEndHour, testNegativePrimoAntigenicEndHour: Int
    let testPositivePcrEndDay, testPositivePcrStartDay, testPositiveAntigenicStartDay, testPositiveAntigenicEndDay, recoveryStartDay, recoveryEndDay: Int
    let vaccineDelay, vaccineDelayJanssen, vaccineDelayMax, vaccineDelayMaxJanssen, vaccineBoosterDelay,vaccineBoosterDelayNew, vaccineBoosterDelayMax: Int
    let vaccineBoosterDelayUnderAge,vaccineBoosterDelayUnderAgeNew, vaccineDelayMaxRecovery: Int
    let vaccineBoosterToggleDate: Date
    let vaccineBoosterAgePeriod: DateComponents
    
    private enum CodingKeys: String, CodingKey {
        case testNegativePcrEndHour, testNegativeAntigenicEndHour, testNegativePrimoPcrEndHour, testNegativePrimoAntigenicEndHour, testPositivePcrEndDay, testPositivePcrStartDay, testPositiveAntigenicStartDay, testPositiveAntigenicEndDay, recoveryStartDay, recoveryEndDay, vaccineDelay, vaccineDelayJanssen, vaccineDelayMax, vaccineDelayMaxJanssen, vaccineBoosterDelay,vaccineBoosterDelayNew, vaccineBoosterDelayMax, vaccineBoosterDelayUnderAge,vaccineBoosterDelayUnderAgeNew, vaccineDelayMaxRecovery, vaccineBoosterToggleDate, vaccineBoosterAgePeriod
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.testNegativePcrEndHour = try container.decode(<#T##type: Int.Type##Int.Type#>, forKey: <#T##KeyedDecodingContainer<CodingKeys>.Key#>)
    }
}
