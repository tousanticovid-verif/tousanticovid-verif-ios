//
//  CertificatesUpdateResponse.swift
//  Anticovid Verify
//
//

import Foundation

struct CertificatesUpdateResponse : Codable {
    var period : Period?
    var specificValues: SpecificValue
    var certificates2DDoc, certificatesDCC : [String:String]
}

struct Period: Codable {
    let frequency, step1, step2, step3: Int
}

struct SpecificValue: Codable {
    let notGreenCountries: [String]
    let validity: Validity
    let tutorial: Tutorial
    let useExpirationDate: ExpirationDateActivator
    
    let testPcrList: [String: String]
    let testAntigenicList: [String: String]
    let vaccineMedicalProductList: [String: String]
    let vaccineProphylaxisList: [String: String]
    let vaccineManufacturerList: [String: String]
    let testManufacturerList: [String: String]
    
    let blacklist: Blacklist
    let ruleEngine: RuleEngine?
    
    let labels: Labels
}

struct Labels: Codable {
    let health: PassLabels
    let vaccine: PassLabels
}

struct PassLabels: Codable {
    let description: LabelDescription
    let tutorial: LabelDescription
}

struct LabelDescription: Codable {
    let fr: String
    let en: String
}

struct RuleEngine: Codable {
    let messageOtModeFR: String?
}

struct ExpirationDateActivator: Codable {
    let forVaccine: Bool
    let forTest: Bool
    let forRecovery: Bool
    let forExemption: Bool
}

struct Validity: Codable {
    private let vaccinePassStartDate: String
    var vaccinePassActivationDate: Date? {
        get {
            Date.dateFormatter.date(from: vaccinePassStartDate)
        }
    }
    
    let health: HealthPassValidity
    let vaccine: VaccinePassValidity
    let plus: PlusValidity
}

struct PlusValidity: Codable {
    let recoveryStartDay, recoveryEndDay: Int
}

struct HealthPassValidity: PassVerifierCommonRulesConfig, Codable {
    let testNegativePcrEndHour, testNegativeAntigenicEndHour: Int
    let testPositivePcrEndDay, testPositivePcrStartDay, testPositiveAntigenicStartDay, testPositiveAntigenicEndDay, recoveryStartDay: Int
    
    let vaccineDelay, vaccineDelayMax: Int
    let vaccineBoosterDelayNew, vaccineBoosterDelayMax: Int
    let vaccineDelayJanssen1, vaccineDelayMaxJanssen1, vaccineDelayJanssen2, vaccineDelayMaxJanssen2, vaccineBoosterDelayMaxJanssen: Int
    let vaccineDelayNovavax, vaccineDelayMaxRecoveryNovavax, vaccineDelayMaxNovavax, vaccineBoosterDelayMaxNovavax: Int
    
    let vaccineBoosterDelayUnderAgeNew, vaccineDelayMaxRecovery: Int
    let vaccineBoosterAgePeriod: String
    let recoveryAcceptanceAgePeriod: String
    let recoveryDelayMaxTV, recoveryDelayMax, recoveryDelayMaxUnderAge: Int
}

struct VaccinePassValidity: PassVerifierCommonRulesConfig, Codable {
    let testNegativePcrEndHour, testNegativeAntigenicEndHour: Int
    let testPositivePcrEndDay, testPositivePcrStartDay, testPositiveAntigenicStartDay, testPositiveAntigenicEndDay, recoveryStartDay: Int

    let vaccineDelay, vaccineDelayMax: Int
    let vaccineBoosterDelayNew, vaccineBoosterDelayMax: Int
    let vaccineDelayJanssen1, vaccineDelayMaxJanssen1, vaccineDelayJanssen2, vaccineDelayMaxJanssen2, vaccineBoosterDelayMaxJanssen: Int
    let vaccineDelayNovavax, vaccineDelayMaxRecoveryNovavax, vaccineDelayMaxNovavax, vaccineBoosterDelayMaxNovavax: Int
    
    let vaccineBoosterDelayUnderAgeNew, vaccineDelayMaxRecovery: Int
    let vaccineBoosterAgePeriod: String
    let testAcceptanceAgePeriod: String
    let recoveryAcceptanceAgePeriod: String
    let recoveryDelayMaxTV, recoveryDelayMax, recoveryDelayMaxUnderAge: Int
}

struct Tutorial: Codable {
    let liteFR, liteEN: String
    let otFR, otEN: String?
}

struct Blacklist: Codable {
    let duplicateActivate: Bool
    let duplicateRetentionPeriod: Int
    let messages: BlacklistMessages
    let exclusionQuantityLimit: Int
    let exclusionTime: [ExclusionTimeResponse]
    let refreshVersion: Int
    let refreshIndex: Int?
    let refreshIndex2ddoc: Int?
    let lastIndexBlacklist: Int
    let lastIndexBlacklist2ddoc: Int
    let standbyDelay: Int
}

struct ExclusionTimeResponse: Codable {
    let start: String
    let end: String
}

struct BlacklistMessages: Codable {
    let blacklistLite, blacklistOT, duplicate: BlacklistMessageDetail
}

struct BlacklistMessageDetail: Codable {
    let titleFR, detailFR, titleEN, detailEN: String
}

typealias SpecificValueList = (
    testPcrList: [String: String],
    testAntigenicList: [String: String],
    vaccineMedicalProductList: [String: String],
    vaccineProphylaxisList: [String: String],
    vaccineManufacturerList: [String: String],
    testManufacturerList: [String: String]
)

