//
//  BlacklistResponse.swift
//  Anticovid Verify
//

import Foundation

struct BlacklistResponse: Codable {
    let elements: [BlacklistHashResponse]
    let lastIndexBlacklist: Int
}

struct BlacklistHashResponse: Codable {
    let hash: String
    let id: Int32
    let active: Bool?
}
