//
//  INClaims.swift
//  Anticovid Verify
//
//

import Foundation
import SwiftJWT

class INClaims: Claims {
    
    var iss : String?
    var exp : Date?
    var iat : Date?
    var siren : String?
    var realm_access: [String: [String]]?
    
    init(iss : String?, realm_access: [String: [String]]?, exp : Date?, iat : Date?, siren: String?) {
        self.iss = iss
        self.realm_access = realm_access
        self.exp = exp
        self.iat = iat
        self.siren = siren
    }
}
