//
//  RuleBaseResponse.swift
//  Anticovid Verify
//


import Foundation

struct RuleBaseResponse: Codable, Equatable {
    let identifier: String
    let version: String
    let country: String
    let hash: String
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.identifier == rhs.identifier &&
        lhs.version == rhs.version &&
        lhs.hash == rhs.hash &&
        lhs.country == rhs.country
    }
}
