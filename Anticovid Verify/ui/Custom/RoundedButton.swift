//
//  RoundedButton.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

@IBDesignable
class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 20{
        didSet{
        self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderWidth: CGFloat = 2{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }

    override var intrinsicContentSize: CGSize
    {
        var size = self.titleLabel?.intrinsicContentSize ?? CGSize.zero
        guard #available(iOS 15.0, *) else {
            size.height += 32
            return size
        }
        return size
    }
    override func layoutSubviews() {
            super.layoutSubviews()
            titleLabel?.preferredMaxLayoutWidth = titleLabel?.frame.size.width ?? 0
            super.layoutSubviews()
        }

}
