//
//  INViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit
import Combine

class INViewController: UIViewController, INViewProtocol {
    private var inViewPresenter: INViewPresenter!
    private var cancellables = Set<AnyCancellable>()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        inViewPresenter = INViewPresenter(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let background = UIImageView(image: UIImage(named: "bg"))
        background.contentMode = .scaleAspectFill
        
        view.insertSubview(background,at:0)
        view.backgroundColor = .lightGrayIn
        background.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            background.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            background.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            background.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            background.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        inViewPresenter.checkNeedAppSync()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func displaySynchronizationAlert(for remindStep: RemindStep) {
        let alert = UIAlertController(
            title: "Synchronisation nécessaire",
            message:"Pour garantir la validité des contrôles, il est nécessaire de maintenir à jour l’application. Merci de vous connecter au réseau internet pour permettre la synchronisation ",
            preferredStyle: .alert
        )
        
        if remindStep == .medium {
            alert.addAction(UIAlertAction(title: "Masquer cette alerte pendant 1h", style: .cancel) { _ in
                self.saveDataWhenUserMaskedBanner()
                self.updateBannerSyncApp()
            })
        }
        
        alert.addAction(UIAlertAction(title: "Continuer", style: .default) { _ in
            self.updateBannerSyncApp()
        })

        self.present(alert, animated: true, completion: nil)
    }
        
    func displaySynchronizationAlertIfNeeded(forRemind remindStep: RemindStep, didTapOnBanner: Bool) {
        guard remindStep != .none,
        !UserDataManager.sharedInstance.shouldShowPrivacy,
        !SyncManager.shared.isSyncingInProgress ||
        (SyncManager.shared.isSyncingInProgress && !UserDataManager.sharedInstance.isNetworkConnected) else { return
        }
        
        if let expirationDate = UserDataManager.sharedInstance.syncBannerViewMaskedDate,
            Date() < expirationDate.dateByAddingHours(1) && !didTapOnBanner {
           return
        }
        
        displaySynchronizationAlert(for: remindStep)
    }

    private func saveDataWhenUserMaskedBanner() {
        UserDataManager.sharedInstance.syncBannerViewMaskedDate = Date()
    }

    private func updateBannerSyncApp() {
        SyncManager.shared.start()
            .subscribe(on: DispatchQueue.global(qos: .background))
            .receive(on: DispatchQueue.global(qos: .background))
            .sink()
            .store(in: &cancellables)
    }
}
