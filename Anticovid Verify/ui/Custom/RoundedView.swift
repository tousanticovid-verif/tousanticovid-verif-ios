//
//  RoundedView.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

@IBDesignable
class RoundedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 20{
        didSet{
        self.layer.cornerRadius = cornerRadius
        }
    }



    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    

}
