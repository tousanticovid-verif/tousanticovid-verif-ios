//
//  ResultViewPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import WalletCommunication
import SwiftDGC

enum ResultState {
    case negative, expired, positiveValid, positiveNotValid, undetermined, notConform, notAuthentic, testTypeNonValid, antigen, inprogress, finished, valid, nonValid, testPcrNegativeDetail, testPcrPositiveDetail, testAntigenicNegativeDetail, testAntigenicPositiveDetail, testAntigenicNegativeInvalid, testAntigenicPositiveInvalid, recoveryValid, recoveryNotValid, vaccinalCycleComplet, vaccinalCycleNotConform
}

enum ResultError {
    case wrongDocument
}

protocol ResultDelegate {
    func getResult(barcode:String, typeBarcode: Int)
}

protocol ResultView {
    func fillTableWithResults(results:[String : [ResultRow]])
    func changeResultState(state:ResultState)
    func showError(error:ResultError)
}

class ResultPresenter : ResultDelegate {
    
    private var view: ResultView?
    private var headerFields: [FieldDTO]?
    private var messageFields: [FieldDTO]?
    
    init(view: ResultView) {
        self.view = view
    }
    
    func getResult(barcode:String, typeBarcode: Int) {
        guard let view = self.view else { return }
        
        if let certificate = try? WalletManager.shared.extractCertificateFrom(doc: barcode) as? SanitaryCertificate  {
            // Test Covid
            if UserData.sharedInstance.shouldShowFullResult {
                self.verifyTestPCR(certificate: certificate, view: view)
            } else {
                self.verifyTestPCRLite(certificate: certificate, view: view)
            }
            
        } else if let certificate = try? WalletManager.shared.extractCertificateFrom(doc: barcode) as? VaccinationCertificate {
            // Vaccin Covid
            self.verifyVaccinCovid(certificate: certificate, view: view, liteMode: UserData.sharedInstance.shouldShowFullResult == false)
        } else if let certificate = HCert.init(from: barcode) {
            HCert.publicKeyStorageDelegate = self
            self.verifyEuCertificate(certificate: certificate, view: view, liteMode: UserData.sharedInstance.shouldShowFullResult == false)
        } else {
            view.showError(error:ResultError.wrongDocument)
        }
    }
    
    private func verifyTestPCR(certificate: SanitaryCertificate, view: ResultView) {
        
        guard let result = certificate.fields["F5"] else {
            return
        }
        
        guard let samplingDate = certificate.analysisDate else {
            return
        }
        
            let validity = WalletManager.shared.checkCertificateSignature(certificate)
            if validity == false {
                view.changeResultState(state: .notAuthentic)
            } else if let loinc = certificate.analysisCode, !loinc.contains("943092") && !loinc.contains("945006") && !loinc.contains("948455") && !loinc.contains("945584") {
                // Test non PCR en rouge
                view.changeResultState(state: .testTypeNonValid)
            } else {
                switch result.uppercased(){
                case "N" :
                    if let loinc = certificate.analysisCode, loinc.contains("943092") || loinc.contains("948455") || loinc.contains("945006")  {
                        view.changeResultState(state: .testPcrNegativeDetail)
                    } else {
                        view.changeResultState(state: .testAntigenicNegativeDetail)
                    }
                case "P" :
                    if let loinc = certificate.analysisCode, loinc.contains("943092") || loinc.contains("948455") || loinc.contains("945006")  {
                        view.changeResultState(state: .testPcrPositiveDetail)
                    } else {
                        view.changeResultState(state: .testAntigenicPositiveDetail)
                    }
                case "X" :
                    view.changeResultState(state: .notConform)
                default :
                    view.changeResultState(state: .undetermined)
                }
            }
        
        var results : [ResultRow] = []
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F6", label: "Date et heure du prélèvement", value: certificate.analysisDateString ?? ""),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F0", label: "Liste des prénoms", value: certificate.firstName ?? ""),picto: .eye))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F1", label: "Nom de famille", value: certificate.name ?? ""),picto: .eye))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F2", label: "Date de naissance", value: certificate.birthDateString ?? ""),picto: .eye))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F5", label: "Résultat", value: Formatter.analysisResultFormatter(analysisResult: result)),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F4", label: "Code analyse", value: Formatter.analysisCodeFormatter(analysisCode: certificate.analysisCode)),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F3", label: "Genre", value: certificate.fields["F3"] ?? ""),picto: .none))
    
        results.append(ResultRow(fieldDTO: FieldDTO(name: "00", label: "Type de document", value: "Test COVID"),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "03", label: "Identifiant de l’autorité de certification", value: certificate.authority ?? ""),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "04", label: "Identifiant du certificat", value: certificate.certificateId ?? ""),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "05", label: "Date d’émission du document", value: certificate.getIssueDate()),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "06", label: "Date de création de la signature", value: certificate.getCreationDate()),picto: .none))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "09", label: "Pays émetteur du document", value: Formatter.countryName(from: certificate.country ?? "")),picto: .none))
        
        let resultRow = ResultRow(title: "DURÉE DEPUIS LE PRÉVELEMENT", subtitle: samplingDate.formatForTestOT() ?? "?", picto: .none)
        
        view.fillTableWithResults(results:["Données de validité": [resultRow], "Informations du 2D-DOC": results])
    }
    
    private func verifyTestPCRLite(certificate: SanitaryCertificate, view: ResultView) {
        
        guard let result = certificate.fields["F5"] else {
            return
        }
        
        guard let samplingDate = certificate.analysisDate else {
            return
        }
        
        let today = Date()
        let fortyheightHourLater = Calendar.current.date(byAdding: DateComponents(hour: 48), to: samplingDate)!
        let elevenDayLater = Calendar.current.date(byAdding: DateComponents(day: 11), to: samplingDate)!
        let sixMonthLater = Calendar.current.date(byAdding: DateComponents(day: 183), to: samplingDate)!
        
            let validity = WalletManager.shared.checkCertificateSignature(certificate)
            if validity == false {
                view.changeResultState(state: .nonValid)
            } else if let loinc = certificate.analysisCode, loinc.contains("943092") || loinc.contains("945006") || loinc.contains("948455") || loinc.contains("945584") {
                switch result.uppercased(){
                case "N" :
                    if today <= fortyheightHourLater {
                        view.changeResultState(state: .valid)
                    } else {
                        view.changeResultState(state: .nonValid)
                    }
                case "P" :
                    if today >= elevenDayLater && today <= sixMonthLater {
                        view.changeResultState(state: .valid)
                    } else {
                        view.changeResultState(state: .nonValid)
                    }
                default :
                    view.changeResultState(state: .undetermined)
                }
            } else {
                view.changeResultState(state: .nonValid)
            }
        
        var results : [ResultRow] = []
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F0", label: "Liste des prénoms", value: certificate.firstName ?? ""),picto: .eye))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F1", label: "Nom de famille", value: certificate.name ?? ""),picto: .eye))
        results.append(ResultRow(fieldDTO: FieldDTO(name: "F2", label: "Date de naissance", value: certificate.birthDateString ?? ""),picto: .eye))
        
        
        view.fillTableWithResults(results:["Informations du 2D-DOC": results])
    }
    
    private func verifyVaccinCovid(certificate: VaccinationCertificate, view: ResultView, liteMode: Bool) {

        // Verifier le Nom du vaccin et le nombre d'injection
        guard let lastStateDate = certificate.lastVaccinationDate else {
            view.showError(error: .wrongDocument)
            return
        }
        
        let isJanssen = (certificate.vaccineName?.value.lowercased() ?? "").contains("janssen")
        let isFinished = certificate.vaccinationCycleState?.lowercased() == "te"
        let validity = WalletManager.shared.checkCertificateSignature(certificate)
        
        var results : [ResultRow] = []
        if liteMode {
            let today = Date()
            let tweetyHeightDaysLater = Calendar.current.date(byAdding: DateComponents(day: 28), to: lastStateDate)!
            let forteenDaysLater = Calendar.current.date(byAdding: DateComponents(day: 14), to: lastStateDate)!
            let injectionCount = Int(certificate.completeCycleDosesCount?.value ?? "0") ?? 0
            
            if validity == false {
                view.changeResultState(state: .nonValid)
            } else if isFinished && ((injectionCount == 1 &&  isJanssen && today > tweetyHeightDaysLater) || (injectionCount == 1 && !isJanssen && today > forteenDaysLater)) {
                view.changeResultState(state: .valid)
            } else if isFinished && (injectionCount >= 2 && today > forteenDaysLater) {
                view.changeResultState(state: .valid)
            } else {
                view.changeResultState(state: .nonValid)
            }
            
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L1", label: "Liste des prénoms du patient", value: certificate.firstName ?? ""),picto: .eye))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L0", label: "Nom de famille du patient", value: certificate.name ?? ""),picto: .eye))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L2", label: "Date de naissance du patient", value: certificate.birthDateString ?? ""),picto: .eye))
        } else {
            
            if validity == false {
                view.changeResultState(state: .notAuthentic)
            } else if isFinished  {
                view.changeResultState(state: .vaccinalCycleComplet)
            } else {
                view.changeResultState(state: .inprogress)
            }
            
            results.append(ResultRow(fieldDTO: FieldDTO(name: "LA", label: "Etat du cycle de vaccination", value: certificate.vaccinationCycleState == "TE" ? "Terminé" : "En cours"),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L1", label: "Liste des prénoms du patient", value: certificate.firstName ?? ""),picto: .eye))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L0", label: "Nom de famille du patient", value: certificate.name ?? ""),picto: .eye))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L2", label: "Date de naissance du patient", value: certificate.birthDateString ?? ""),picto: .eye))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L3", label: "Nom de la maladie couverte", value: certificate.diseaseName ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L4", label: "Agent prophylactique", value: certificate.prophylacticAgent ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L5", label: "Nom du vaccin", value: certificate.vaccineName ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L6", label: "Fabriquant du vaccin", value: certificate.vaccineMaker ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L7", label: "Rang du dernier état de vaccination effectué", value: certificate.lastVaccinationStateRank ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L8", label: "Nombre de doses attendues pour un cycle complet", value: certificate.completeCycleDosesCount ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "L9", label: "Date du dernier état du cycle de vaccination", value: certificate.lastVaccinationDateString ?? ""),picto: .none))
            
            results.append(ResultRow(fieldDTO: FieldDTO(name: "00", label: "Type de document", value: "Attestation Vaccinale"),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "05", label: "Date d’émission du document", value: certificate.getIssueDate()),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "06", label: "Date de création de la signature", value: certificate.getCreationDate()),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "03", label: "Identifiant de l’autorité de certification", value: certificate.authority ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "04", label: "Identifiant du certificat", value: certificate.certificateId ?? ""),picto: .none))
            results.append(ResultRow(fieldDTO: FieldDTO(name: "09", label: "Pays émetteur du document", value: Formatter.countryName(from: certificate.country ?? "")),picto: .none))
        }
        
        if liteMode {
            view.fillTableWithResults(results:["Informations du 2D-DOC": results])
        } else {
            let resultRow = ResultRow(title: "DURÉE DEPUIS LA DERNIÈRE INJECTION", subtitle: lastStateDate.formatForVaccinOT() ?? "?", picto: .none)
    
            view.fillTableWithResults(results:["Données de validité": [resultRow], "Informations du 2D-DOC": results])
        }
    }
    
    private func verifyEuCertificate(certificate: HCert, view: ResultView, liteMode: Bool) {
        
        var results: [ResultRow] = []
        var resultRow: ResultRow? = nil
        
        results.append(ResultRow(title: "LISTE DES PRENOMS", subtitle: certificate.body["nam"]["gn"].stringValue, picto: .eye))
        results.append(ResultRow(title: "NOM DE FAMILLE", subtitle: certificate.body["nam"]["fn"].stringValue, picto: .eye))
        
        if let dateOfBirthString = certificate.body["dob"].string {
            if dateOfBirthString.count == 4 {
                results.append(ResultRow(title: "DATE DE NAISSANCE", subtitle: "XX/XX/\(dateOfBirthString)", picto: .eye))
            } else if let dateOfBirth = Date(dateString: dateOfBirthString) {
                results.append(ResultRow(title: "DATE DE NAISSANCE", subtitle: dateOfBirth.dateString, picto: .eye))
            } else {
                results.append(ResultRow(title: "DATE DE NAISSANCE", subtitle: dateOfBirthString, picto: .eye))
            }
        } else {
            results.append(ResultRow(title: "DATE DE NAISSANCE", subtitle: certificate.body["dob"].stringValue, picto: .eye))
        }
            
            switch certificate.type {
            case .vaccine:
                
                guard let vaccinationDateString = certificate.body["v"][0]["dt"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                guard let vaccinationDate = Date(dateString: vaccinationDateString) else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                resultRow = ResultRow(title: "DURÉE DEPUIS LA DERNIÈRE INJECTION", subtitle: vaccinationDate.formatForVaccinOT() ?? "?", picto: .none)
                
                let today = Date()
                let tweetyHeightDaysLater = Calendar.current.date(byAdding: DateComponents(day: 28), to: vaccinationDate)!
                let forteenDaysLater = Calendar.current.date(byAdding: DateComponents(day: 14), to: vaccinationDate)!
                
                guard let dosesCount = certificate.body["v"][0]["dn"].int else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let expectedDosesCount = certificate.body["v"][0]["sd"].int else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let vaccineName = certificate.body["v"][0]["mp"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let prophylacticAgent = certificate.body["v"][0]["vp"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let manufacturer = certificate.body["v"][0]["ma"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                if liteMode {
                    if certificate.cryptographicallyValid == false {
                        view.changeResultState(state: .nonValid)
                    } else if (dosesCount == expectedDosesCount && Formatter.vaccineProphylaxis.contains(prophylacticAgent) && Formatter.vaccineMedicinalProduct.contains(vaccineName) && Formatter.vaccineMahManf.contains(manufacturer)) && ((expectedDosesCount == 1 && vaccineName == "EU/1/20/1525" && today > tweetyHeightDaysLater) ||
                        (expectedDosesCount == 1 && vaccineName != "EU/1/20/1525" && today > forteenDaysLater) ||
                        (expectedDosesCount == dosesCount && dosesCount >= 2 && today > forteenDaysLater))
                        {
                        view.changeResultState(state: .valid)
                    } else {
                        view.changeResultState(state: .nonValid)
                    }
                } else {
                    if certificate.cryptographicallyValid == false {
                        view.changeResultState(state: .notAuthentic)
                    } else if dosesCount == expectedDosesCount && Formatter.vaccineProphylaxis.contains(prophylacticAgent) && Formatter.vaccineMedicinalProduct.contains(vaccineName) && Formatter.vaccineMahManf.contains(manufacturer)
                        {
                        view.changeResultState(state: .vaccinalCycleComplet)
                    } else {
                        view.changeResultState(state: .vaccinalCycleNotConform)
                    }
                    
                    results.append(ResultRow(title: "DATE DE LA VACCINATION", subtitle: vaccinationDate.dateString, picto: .none))
                    
                    results.append(ResultRow(title: "NOMBRE DANS UNE SÉRIE DE VACCIN/DOSES", subtitle: "\(certificate.body["v"][0]["dn"].intValue)/\(certificate.body["v"][0]["sd"].intValue)", picto: .none))
                    
                    results.append(ResultRow(title: "MÉDICAMENT VACCINAL", subtitle: Formatter.vaccineName(from: vaccineName), picto: .none))
                    
                    results.append(ResultRow(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.diseaseName(from: certificate.body["v"][0]["tg"].stringValue), picto: .none))
                    
                    results.append(ResultRow(title: "AGENT PROPHYLACTIQUE", subtitle: Formatter.prophylacticAgentName(from: prophylacticAgent), picto: .none))
                    
                    results.append(ResultRow(title: "FABRICANT", subtitle: Formatter.manufacturerName(from: manufacturer), picto: .none))
                    
                    results.append(ResultRow(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.countryName(from: certificate.body["v"][0]["co"].stringValue), picto: .none))
                    
                    results.append(ResultRow(title: "ÉMETTEUR DU CERTIFICAT", subtitle: certificate.body["v"][0]["is"].stringValue, picto: .none))
                    
                    let kid = certificate.body["v"][0]["ci"].stringValue
                    results.append(ResultRow(title: "IDENTIFIANT DU CERTIFICAT", subtitle: kid, picto: .none))
                }
                
                break
            case .test:
                
                guard let testType = certificate.body["t"][0]["tt"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let testResult = certificate.body["t"][0]["tr"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let samplingDateString = certificate.body["t"][0]["sc"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                guard let samplingDate = Date(dateString: samplingDateString) else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                resultRow = ResultRow(title: "DURÉE DEPUIS LE PRÉLÈVEMENT", subtitle: samplingDate.formatForTestOT() ?? "?", picto: .none)
                
                let today = Date()
                let sixMonthLater = Calendar.current.date(byAdding: DateComponents(day: 183), to: samplingDate)!
                let fifteenDaysLater = Calendar.current.date(byAdding: DateComponents(day: 15), to: samplingDate)!
                
                if liteMode {
                    
                    let fortyHeigtHoursLater = Calendar.current.date(byAdding: DateComponents(hour: 48), to: samplingDate)!
                    
                    if certificate.cryptographicallyValid == false {
                        view.changeResultState(state: liteMode ? .nonValid : .notAuthentic)
                    } else if (testType == "LP6464-4" || testType == "LP217198-3") && ( (testResult == "260415000" && today <= fortyHeigtHoursLater) || (testResult == "260373001" && today >= fifteenDaysLater && today <= sixMonthLater)) {
                        view.changeResultState(state: .valid)
                    } else {
                        view.changeResultState(state: .nonValid)
                    }
                } else {
                    
                    let manufacturer = certificate.body["t"][0]["ma"].stringValue

                    if certificate.cryptographicallyValid == false {
                        view.changeResultState(state: liteMode ? .nonValid : .notAuthentic)
                    } else if testType != "LP6464-4" && testType != "LP217198-3" {
                        view.changeResultState(state: .testTypeNonValid)
                    } else if testType == "LP6464-4" && testResult == "260415000" {
                        view.changeResultState(state: .testPcrNegativeDetail)
                    } else if testType == "LP6464-4" && testResult == "260373001" {
                        view.changeResultState(state: .testPcrPositiveDetail)
                    } else if testType == "LP217198-3" && Formatter.testManf.contains(manufacturer) {
                     
                        if testResult == "260415000" {
                            view.changeResultState(state: .testAntigenicNegativeDetail)
                        } else if testType == "LP217198-3" && Formatter.testManf.contains(manufacturer) && testResult == "260373001" {
                            view.changeResultState(state: .testAntigenicPositiveDetail)
                        }
                    
                    } else if testType == "LP217198-3" && !Formatter.testManf.contains(manufacturer) {
                     
                        if testResult == "260415000" {
                            view.changeResultState(state: .testAntigenicNegativeInvalid)
                        } else if testType == "LP217198-3" && Formatter.testManf.contains(manufacturer) && testResult == "260373001" {
                            view.changeResultState(state: .testAntigenicPositiveInvalid)
                        }
                        
                    } else {
                        view.changeResultState(state: .undetermined)
                    }
                    
                    results.append(ResultRow(title: "DATE ET HEURE DU PRELEVEMENT", subtitle: samplingDate.fullDateFormatted(), picto: .none))
                    results.append(ResultRow(title: "RESULTAT DU TEST", subtitle: Formatter.analysisResultFormatter(analysisResult: testResult), picto: .none))
                    
                    results.append(ResultRow(title: "TYPE DE TEST", subtitle: Formatter.testTypeName(from: certificate.body["t"][0]["tt"].stringValue), picto: .none))
                    
                    results.append(ResultRow(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.diseaseName(from: certificate.body["t"][0]["tg"].stringValue), picto: .none))
                    
                    if let testName = certificate.body["t"][0]["nm"].string {
                        results.append(ResultRow(title: "NOM DU TEST", subtitle: testName, picto: .none))
                    }
                    
                    if let testManufacturer = certificate.body["t"][0]["ma"].string {
                        results.append(ResultRow(title: "FABRICANT DU TEST", subtitle: Formatter.manufacturerName(from: testManufacturer), picto: .none))
                    }
                    
                    results.append(ResultRow(title: "CENTRE DE TEST", subtitle: certificate.body["t"][0]["tc"].stringValue, picto: .none))
                    
                    results.append(ResultRow(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.countryName(from: certificate.body["t"][0]["co"].stringValue), picto: .none))
                    
                    results.append(ResultRow(title: "ÉMETTEUR DU CERTIFICAT", subtitle: certificate.body["t"][0]["is"].stringValue, picto: .none))

                    let kid = certificate.body["t"][0]["ci"].stringValue
                    results.append(ResultRow(title: "IDENTIFIANT DU CERTIFICAT", subtitle: kid, picto: .none))

                }
                
                break
            case .recovery:
                
                guard let firstSamplingDateString = certificate.body["r"][0]["fr"].string else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                guard let firstSamplingDate = Date(dateString: firstSamplingDateString) else {
                    view.showError(error:ResultError.wrongDocument)
                    return
                }
                
                resultRow = ResultRow(title: "DURÉE DEPUIS LE PRÉLÈVEMENT POSITIF", subtitle: firstSamplingDate.formatForRecoveryOT() ?? "?", picto: .none)
                
                let today = Date()
                let minimumDaysLater = Calendar.current.date(byAdding: DateComponents(day: liteMode ? 15 : 11), to: firstSamplingDate)!
                let sixMonthLater = Calendar.current.date(byAdding: DateComponents(day: 183), to: firstSamplingDate)!
                
                if certificate.cryptographicallyValid == false {
                    view.changeResultState(state: liteMode ? .nonValid : .notAuthentic)
                } else if today >= minimumDaysLater && today <= sixMonthLater
                    {
                    view.changeResultState(state: liteMode ? .valid : .recoveryValid)
                } else {
                    view.changeResultState(state: liteMode ? .nonValid : .recoveryNotValid)
                }
                
                if !liteMode {
                    
                    results.append(ResultRow(title: "DATE DU PREMIER PRÉLÈVEMENT POSITIF", subtitle: firstSamplingDate.dateString, picto: .none))
                    
                    if let startOfValidityString = certificate.body["r"][0]["df"].string, let startOfValidity = Date(dateString: startOfValidityString) {
                        results.append(ResultRow(title: "DÉBUT DE VALIDITÉ", subtitle: startOfValidity.dateString, picto: .none))
                    } else {
                        results.append(ResultRow(title: "DÉBUT DE VALIDITÉ", subtitle: certificate.body["r"][0]["df"].stringValue, picto: .none))
                    }
                    
                    if let endOfValidityString = certificate.body["r"][0]["du"].string, let endOfValidity = Date(dateString: endOfValidityString) {
                        results.append(ResultRow(title: "FIN DE VALIDITÉ", subtitle: endOfValidity.dateString, picto: .none))
                    } else {
                        results.append(ResultRow(title: "FIN DE VALIDITÉ", subtitle: certificate.body["r"][0]["du"].stringValue, picto: .none))
                    }
                    
                    results.append(ResultRow(title: "NOM DE LA MALADIE COUVERTE", subtitle: Formatter.diseaseName(from: certificate.body["r"][0]["tg"].stringValue), picto: .none))
                    
                    results.append(ResultRow(title: "ÉTAT ÉMETTEUR", subtitle: Formatter.countryName(from: certificate.body["r"][0]["co"].stringValue), picto: .none))
                    results.append(ResultRow(title: "ÉMETTEUR DU CERTIFICAT", subtitle: certificate.body["r"][0]["is"].stringValue, picto: .none))
                    
                    let kid = certificate.body["r"][0]["ci"].stringValue
                    results.append(ResultRow(title: "IDENTIFIANT DU CERTIFICAT", subtitle: kid, picto: .none))

                }
                
                break
            case .unknown:
                view.showError(error:ResultError.wrongDocument)
                return
        }
        
        if liteMode {
            view.fillTableWithResults(results:["Informations du QR Code": results])
        } else {
            view.fillTableWithResults(results:["Données de validité": [resultRow!], "Informations du QR Code": results])
        }
    }
    
    private func getDurationSinceSample(samplingDate: Date) -> String {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        var calendarComponents: Set<Calendar.Component> = [Calendar.Component.hour, Calendar.Component.minute]
        if Date().timeIntervalSince(samplingDate) > 345600 {
            calendarComponents.insert(Calendar.Component.day)
            calendarComponents.insert(Calendar.Component.month)
        }
        let components = calendar.dateComponents(calendarComponents, from: samplingDate, to: Date())
        
        var message = ""
        if Date().timeIntervalSince(samplingDate) > 345600 {
            if let month = components.month, month > 0 {
                message.append(" \(month)")
                if month > 1 {
                    message.append(" mois")
                } else {
                    message.append(" mois")
                }
            }
            
            if let day = components.day, day > 0 {
                message.append(" \(day)")
                if day > 1 {
                    message.append(" jours")
                } else {
                    message.append(" jour")
                }
            }
        }
        
        if let hour = components.hour, hour > 0 {
            message.append(" \(hour)")
            if hour > 1 {
                message.append(" heures")
            } else {
                message.append(" heure")
            }
        }
        
        if let minute = components.minute, minute > 0 {
            message.append(" \(minute)")
            if minute > 1 {
                message.append(" minutes")
            } else {
                message.append(" minute")
            }
        }
        
        return message
    }
    
    private func getResultPicto(name: String) -> ResultPicto {
        if ["F0", "F1", "F2", "L0", "L1", "L2"].contains(name) {
            return .eye
        }
        
        return .none
    }
    
    private func getNegativeResultState(by samplingDate: Date, maxHours: Int, liteMode: Bool) -> ResultState {
        let today = Date()
        let expiryDate = Calendar.current.date(byAdding: DateComponents(hour: maxHours), to: samplingDate)!
        
        if today > expiryDate {
            return liteMode ? .valid : .expired
        }
        
        return liteMode ? .nonValid : .negative
    }
    
    private func getPositiveResultState(by samplingDate: Date, minDays: Int, maxDays: Int, liteMode: Bool) -> ResultState {
        let today = Date()
        let validDateBegin = Calendar.current.date(byAdding: DateComponents(day: minDays), to: samplingDate)!
        let validDateEnd = Calendar.current.date(byAdding: DateComponents(day: maxDays), to: samplingDate)!
        
        if today >= validDateBegin && today <= validDateEnd {
            return liteMode ? .valid : .positiveValid
        }
        
        return liteMode ? .nonValid : .positiveNotValid
    }
    
}

extension ResultPresenter: PublicKeyStorageDelegate {
    
    func getEncodedPublicKeys(for kid: String) -> [String] {
        guard let certificateString = CertificatesManager.shared.dccCertificates?[kid] else { return [] }
        guard let base64Data = Data(base64Encoded: certificateString) else { return [] }
        guard let base64Decoded = String(data: base64Data, encoding: .utf8) else { return [] }
        return [base64Decoded]
    }
    
}
