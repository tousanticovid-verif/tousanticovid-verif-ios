//
//  MainViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class MainViewController: INViewController {
    @IBOutlet private weak var container: UIView!
    @IBOutlet private weak var bannerView: UILabel!
    @IBOutlet private weak var containerBannerView: UIView!
    @IBOutlet private weak var containerMask: UIView!
    
    private var presenter: MainViewPresenter!
    private var bannerType: BannerType? = .updateBannerSyncApp
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        presenter = MainViewPresenter(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(identifier: "HomeViewController")
        
        let bannerTap = UITapGestureRecognizer(target: self, action: #selector(bannerViewTapped))
        self.bannerView?.addGestureRecognizer(bannerTap)
        
        vc.willMove(toParent: self)
        addChild(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(vc.view)

        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 0),
            vc.view.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: 0),
            vc.view.topAnchor.constraint(equalTo: container.topAnchor, constant: 0),
            vc.view.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: 0),
            vc.view.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 1)
        ])
        
        vc.didMove(toParent: self)
    }
    
    @objc private func bannerViewTapped(){
        presenter.handleDidTapBannerView()
    }
}

extension MainViewController: MainViewProtocol {
    func displaySyncAlert(didTapOnBanner: Bool) {
        displaySynchronizationAlertIfNeeded(forRemind: SyncManager.shared.actualAppSyncRemindStep(),
                                            didTapOnBanner: didTapOnBanner)
    }
    
    func displaySwitchMode() {
        let containedNavController = children.first as? UINavigationController
        UINavigationController.updateNavigationBarTheme(navigationBar: containedNavController?.navigationBar)
    }
    
    func updateBannerView(text: String?) {
        bannerView.text = text
        bannerView.isHidden = false
        containerBannerView.isHidden = text == nil
    }
    
    func displayAlert(_ alert: UIAlertController) {
        present(alert, animated: true)
    }
    
    func displayLockViewsExeptBanner(_ value: Bool) {
        container.isUserInteractionEnabled = !value
        containerMask.isHidden = !value
        containerMask.backgroundColor = .systemGray
        containerMask.alpha = 0.4
    }
}
