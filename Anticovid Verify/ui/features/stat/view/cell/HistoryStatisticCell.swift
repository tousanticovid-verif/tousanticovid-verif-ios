//
//  HistoryStatisticCell.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class HistoryStatisticCell: UITableViewCell {
    
    static let identifier = "historyStatisticCell"
    
    @IBOutlet weak var historyStatisticDateLabel: UILabel!
    
    @IBOutlet weak var historyNumberOfScanStatisticLabel: UILabel!
    
    @IBOutlet weak var historyNumberOfDoubleStatisticLabel: UILabel!

    public func makeCell(stat: HistoryData) {
        historyStatisticDateLabel.text = "Le \(stat.date.dateString)"
        historyNumberOfScanStatisticLabel.text = String(stat.numberOfScan)
        historyNumberOfDoubleStatisticLabel.text = String(stat.double)
    }
}
