//
//  ResultCell.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class ResultCell: UITableViewCell {
    
    @IBOutlet weak var titleResult: UILabel!
    @IBOutlet weak var pictoResult: UIImageView!
    @IBOutlet weak var descriptionResult: UITextField!
    @IBOutlet weak var titleNoImageContraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionNoImageConstraint: NSLayoutConstraint!
    
    var row : FieldRow? {
        didSet{
            if let row = self.row {
                var title = ""
                var subtitle = ""
                var picto = ResultPicto.none
                switch row {
                case .Admin(let aTitle, let content):
                    title = aTitle
                    subtitle = content ?? ""
                case .Field(let aTitle, let aSubtitle, let aPicto):
                    picto = aPicto ?? .none
                    title = aTitle
                    subtitle = aSubtitle
                case .Header(let aLabel):
                    title = aLabel
                case .Rule(let aLabel, _):
                    title = aLabel ?? ""
                }
                
                self.titleResult.text = title.uppercased()

                let isPicto = (picto != ResultPicto.none)
                if isPicto {
                    self.pictoResult.image = UIImage(named: picto.rawValue)
                }
                let rightConstraint = isPicto ? 54 : 16
                addLabel(withOffset: rightConstraint + 32,text: subtitle)
                switch picto {
                case .valid:
                    self.pictoResult.tintColor = .greenIn
                case .notValid:
                    self.pictoResult.tintColor = .redIn
                case .eye:
                    self.pictoResult.tintColor = .goldIn
                case .none:
                    self.pictoResult.tintColor = UIColor.black
                }
                self.pictoResult.isHidden = !isPicto
                self.titleNoImageContraint.constant = CGFloat(rightConstraint)
                self.descriptionNoImageConstraint.constant = CGFloat(rightConstraint)
                self.isUserInteractionEnabled = isPicto
            }
        }
    }
    
    public func maskCell(fromTop margin: CGFloat) {
        layer.mask = visibilityMask(withLocation: margin / frame.size.height)
        layer.masksToBounds = true
    }

    private func visibilityMask(withLocation location: CGFloat) -> CAGradientLayer {
        let mask = CAGradientLayer()
        mask.frame = bounds
        mask.colors = [UIColor.white.withAlphaComponent(0).cgColor, UIColor.white.cgColor]
        let num = location as NSNumber
        mask.locations = [num, num]
        
        return mask
    }
    //var labelConstraintTemp : [NSLayoutConstraint]?
    var textFieldConstraintTemp : NSLayoutConstraint?
    /*  We add a label to the first view of an uitextfield.
     Why ? Because we want to have the secureTextEntry
     capabilities to hide the label while a screenshot
     */
    func addLabel(withOffset offset:Int,text: String){
        var descriptionLabel = self.descriptionResult.subviews.first?.viewWithTag(32) as? UILabel
        /*if let constraint = labelConstraintTemp {
            descriptionLabel?.removeConstraints(constraint)
        }*/
        descriptionLabel?.removeFromSuperview()
        descriptionLabel = UILabel()

            self.descriptionResult.isSecureTextEntry = !ConstConf.canScreen
            descriptionLabel?.translatesAutoresizingMaskIntoConstraints = false
            descriptionLabel?.numberOfLines = 0
            //tag for adding the view once
            descriptionLabel?.tag = 32
            self.descriptionResult?.isUserInteractionEnabled = false
            //the label is add in the firstview of the uitextfield that has the hidden property in a screenshot
            if let parent = self.descriptionResult.subviews.first, let descriptionLabel = descriptionLabel  {
                parent.addSubview(descriptionLabel)
                descriptionLabel.text = text
                descriptionLabel.font = INStyles.fontForStyle(style: "textLittle")
                descriptionLabel.textColor = .secondSubtitle
                parent.topAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: 0).isActive = true
                parent.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor, constant: 0).isActive = true
                parent.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: 0).isActive = true
                let labelWidth = Int(UIScreen.main.bounds.size.width) - offset
                let size = CGSize(width: CGFloat(labelWidth), height: CGFloat.greatestFiniteMagnitude)
                let labelHeight = descriptionLabel.sizeThatFits(size).height
                if let constraint = textFieldConstraintTemp {
                    descriptionResult?.removeConstraints([constraint])
                }
                textFieldConstraintTemp =
                descriptionResult.heightAnchor.constraint(greaterThanOrEqualToConstant: labelHeight)
                textFieldConstraintTemp?.isActive = true
            }
        }
    //}
}
