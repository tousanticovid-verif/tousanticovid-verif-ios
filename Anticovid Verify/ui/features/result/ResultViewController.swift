//
//  ResultViewController.swift
//  Anticovid Verify
//
//

import UIKit
import Foundation
import INCommunication
import OrderedCollections

class ResultViewController: INViewController {
    
    var items: OrderedDictionary<SectionRow, [FieldRow]> = [:] {
        didSet {
            items.keys.forEach {
                switch $0 {
                case .Rules(_, _):
                    self.hiddenSections.insert($0)
                default: break
                }
            }
            guard isViewLoaded else { return }
            self.tableView.reloadData()
        }
    }
    
    var hiddenSections = Set<SectionRow>()
    
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var resumeLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var resultHeaderView: UIView!
    
    @IBOutlet weak var blacklistHeader: UIButton!
    @IBOutlet weak var blacklistPaddingView: UIView!
    
    @IBOutlet weak var blacklistPaddingViewTop: UIView!
    var automaticReturnTimer: Timer?
    
    var currentAlertController: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = true
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        tableView.register(ResultCountryHeaderView.nib, forHeaderFooterViewReuseIdentifier: ResultCountryHeaderView.identifier)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let index = self.navigationController?.viewControllers.firstIndex(where: { type(of: $0) == TutorialResultViewController.self }) {
            self.navigationController?.viewControllers.remove(at: index)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.automaticReturnTimer?.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    @IBAction func rescan() {
        self.automaticReturnTimer?.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
    
    func startSchedule(with time: Int) {
        self.automaticReturnTimer = Timer.scheduledTimer(withTimeInterval: time.toDouble(), repeats: false, block: { timer in
            timer.invalidate()
            self.currentAlertController?.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func displayResultHeader(resultConfiguration: ResultHeaderConfiguration?) {
        changeResultView(icon: resultConfiguration?.image, text: resultConfiguration?.title, color: resultConfiguration?.color)
    }
    var content : String = ""
    func displayBlacklistButton(title:String,content:String){
        let attributedText = NSAttributedString(string: title , attributes: [.font: UIFont(name: "Avenir-Heavy", size: 17)!,
                                                                             .kern: -0.5])
        blacklistHeader.setAttributedTitle(attributedText, for: .normal)
        blacklistHeader.isHidden = false
        blacklistPaddingView.isHidden = false
        blacklistPaddingViewTop.isHidden = false
        blacklistHeader.semanticContentAttribute = .forceRightToLeft
        blacklistHeader.titleLabel?.numberOfLines = 0
        blacklistHeader.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        blacklistHeader.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        blacklistHeader.layoutSubviews()
        blacklistHeader.layoutIfNeeded()
        blacklistHeader.sizeToFit()
        self.content = content
        blacklistHeader.addTarget(self, action: #selector(blacklistPressed), for: .touchUpInside)
    }
    @objc func blacklistPressed(){
        self.present(
            ViewControllerIntanciator.getWKWebViewContainer(
                wkWebViewData: WkWebViewData(value: self.content, type: .HTLM), titleValue: "Attention"),
            animated: true
        )
    }
    private func changeResultView(icon: UIImage?, text: String?, color: UIColor?) {
        resultImage.image = icon
        resultImage.isHidden = icon == nil
        resultImage.tintColor = color
        resumeLabel.textColor = color
        resumeLabel.text = text
        resumeLabel.isHidden = text == nil
        resultHeaderView.isHidden = text == nil && icon == nil
    }
}
extension ResultViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch key(section: section) {
        case .Rules:
            return UITableView.automaticDimension
        case .Label(let title, _, _):
            return title.isEmpty ? 0 : UITableView.automaticDimension
        case .emptyHeader:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        switch key(section: section) {
        case .Rules(let country, _):
            return country.count > 25 ? 70 : 50
        case .Label(let title, _, _):
            return title.isEmpty ? 0 : UITableView.automaticDimension
        case .emptyHeader:
            return 0
        }
    }
}

extension ResultViewController : UITableViewDataSource {
    
    private func key(section: Int) -> SectionRow {
        return Array(items.keys)[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = key(section: section)
        let item = items[key]?.first
        
        switch item {
        case .Admin, .Field:
            return items[key]?.count ?? 0
        case .Rule:
            return self.hiddenSections.contains(key) ? 0 : items[key]?.count ?? 0
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let key = key(section: indexPath.section)
        let item = items[key]?[indexPath.row]
        
        switch item {
        case .Admin(let admin, let content):
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "resultAdmin", for: indexPath) as! ResultAdminCell
            cell.makeCell(information: admin, showDisclosure: !(content?.isEmpty ?? true))
            cell.tintColor = .white
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            return cell
        case .Field:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "resultCell", for: indexPath) as! ResultCell
            cell.row = item
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.white.withAlphaComponent(0.5) :  UIColor.clear
            return cell
        case .Rule(let rule, let color):
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "resultRuleCell", for: indexPath) as! ResultRuleCell
            cell.ruleDescription.text = rule
            cell.columnView.backgroundColor = color
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let key = key(section: section)
        switch key {
        case .Rules(let country, let validation):
            let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: ResultCountryHeaderView.identifier) as? ResultCountryHeaderView
            let collapseIsHidden = items[key]?.count == 0
            
            header?.makeCell(country: country, validation: validation, collapseIsHidden: collapseIsHidden, isCollaspe: self.hiddenSections.contains(key))
            header?.tag = section
            header?.clipsToBounds = true
            header?.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideSection(_:)))
            header?.isUserInteractionEnabled = true
            header?.addGestureRecognizer(tap)
            return header
            
        case .Label(let title, let color, let font):
            if title.isEmpty  {
                return UIView()
            } else {
                let header = self.tableView.dequeueReusableCell(withIdentifier: "resultHeader")!
                header.clipsToBounds = true
                if let label : INLabel = header.contentView.subviews.first(where: { return $0 as? INLabel != nil }) as? INLabel {
                    label.text = title
                    label.textColor = color ?? label.textColor
                    label.font = font ?? label.font
                    label.numberOfLines = 0
                }
                
                return header
            }
        case .emptyHeader:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let key = key(section: indexPath.section)
        let item = items[key]?[indexPath.row]
        
        switch item {
        case .Admin(_, let content):
            guard let content = content else { return }
            self.present(
                ViewControllerIntanciator.getWKWebViewContainer(
                    wkWebViewData: WkWebViewData(value: content, type: .HTLM), titleValue: "Attention"),
                animated: true
            )
        case .Field(_, _, let picto):
            if !(picto == .eye) { return }
            self.currentAlertController = UIAlertController(title: "Information", message: "indique que la donnée est à comparer avec les documents d'identité ou la date actuelle", preferredStyle: .actionSheet)
            self.currentAlertController?.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
            
            if let popoverController = self.currentAlertController?.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.maxY, width: 1, height: 1)
            }
            
            self.present(self.currentAlertController!, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in self.tableView.visibleCells {
            let paddingToDisapear = self.tableView.sectionHeaderHeight
            
            let hiddenFrameHeight = scrollView.contentOffset.y + paddingToDisapear - cell.frame.origin.y
            if (hiddenFrameHeight >= 0 || hiddenFrameHeight <= cell.frame.size.height) {
                if let customCell = cell as? ResultCell {
                    customCell.maskCell(fromTop: hiddenFrameHeight)
                }
            }
        }
    }
    
    @objc
    private func hideSection(_ sender: UITapGestureRecognizer) {
        let section = sender.view!.tag
        let key = key(section: section)
        
        if indexPathsForSection(section).isEmpty { return }
        
        if self.hiddenSections.contains(key) {
            self.hiddenSections.remove(key)
        } else {
            self.hiddenSections.insert(key)
        }
        
        self.tableView.reloadData()
    }
    
    func indexPathsForSection(_ section: Int) -> [IndexPath] {
        var indexPaths = [IndexPath]()
        let key = key(section: section)
        
        for row in 0..<(self.items[key]?.count ?? 0) {
            indexPaths.append(IndexPath(row: row,section: section))
        }
        
        return indexPaths
    }
}
