//
//  ResultHeaderConfiguration.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

struct ResultHeaderConfiguration {
    let title: String
    let image: UIImage?
    let color: UIColor
}
