//
//  InformationViewController.swift
//  Anticovid Verify
//
//

import UIKit

class InformationViewController: INViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onTouchContact(_ sender: UIButton) {
        if let label = sender.titleLabel?.text, let url = URL(string: "mailto:\(label)") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onTouchPrivacyPolicy(_ sender: Any) {
        if let url = URL(string: ConstConf.privacyPolicy) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onTouchCGU(_ sender: Any) {
        if let url = URL(string: ConstConf.cgu) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onTouchDecret(_ sender: Any) {
        if let url = URL(string: ConstConf.decret) {
            UIApplication.shared.open(url)
        }
    }
}
