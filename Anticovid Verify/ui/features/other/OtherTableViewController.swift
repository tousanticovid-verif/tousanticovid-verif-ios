//
//  OtherTableViewController.swift
//  Anticovid Verify
//
//

import UIKit

class OtherTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.delegate = self.parent as! OtherViewController
    }
}
