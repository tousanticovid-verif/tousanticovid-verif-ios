//
//  MenuViewController.swift
//  Anticovid Verify
//
//

import UIKit
import os.log

class OtherViewController: INViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


extension OtherViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "settingSegue", sender: nil)
        case 1:
            performSegue(withIdentifier: "informationSegue", sender: nil)
        case 2:
            performSegue(withIdentifier: "aboutSegue", sender: nil)
        case 3:
            performSegue(withIdentifier: "statisticSegue", sender: nil)
        default:
            os_log("%@",type: .debug, "Out of index")
        }
    }
}
