//
//  ScanViewController.swift
//  Anticovid Verify
//
//

import os.log
import AVFoundation
import UIKit
import ScanditBarcodeCapture

class ScanViewController : INViewController {
    
    private var scanViewPresenter: ScanViewPresenter!
    
    private var camera: Camera?
    private var context: DataCaptureContext!
    private var barcodeCapture: BarcodeCapture?
    
    
    // MARK: Properties
    @IBOutlet var barcodeFrameView: UIView!
    
    @IBOutlet weak var tutorialView: UIVisualEffectView!
    @IBOutlet weak var documentTypeView: UIVisualEffectView!
    @IBOutlet weak var flashButton: RoundedButton!
    @IBOutlet weak var mireImage: UIImageView!
    @IBOutlet weak var loadingView: UIVisualEffectView!

    private var hiddenObserver: NSKeyValueObservation?
    
    
    // MARK: Lifecycle Methods
    enum FlashStatus {
        case on,off,unavailable
    }
    
    var flashStatus : FlashStatus = .off {
        didSet {
            if flashStatus == .unavailable {
                flashButton.isHidden = true
                return
            }
            if let device = AVCaptureDevice.default(for: AVMediaType.video){
                do {
                    try device.lockForConfiguration()
                    device.torchMode = flashStatus == .on ? .on : .off
                    if flashStatus == .on {
                        flashButton.setImage(UIImage(named: "flash_on")!, for: .normal)
                    } else {
                        flashButton.setImage(UIImage(named: "flash_off")!, for: .normal)
                    }
                    device.unlockForConfiguration()
                } catch {
                    self.flashStatus = .unavailable
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        scanViewPresenter = ScanViewPresenter(self)
        context = DataCaptureContext(licenseKey: ConstConf.licSC)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Scan du 2D-Doc/QR Code"
        
        self.configureSDK()
        self.barcodeCapture?.addListener(self)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "questionmark.circle"), style: .plain, target: self, action: #selector(onHelp))
        
        guard let device = AVCaptureDevice.default(for: AVMediaType.video),
              device.hasTorch
        else {
            flashStatus = .unavailable
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let index = self.navigationController?.viewControllers.firstIndex(where: { type(of: $0) == TutorialScanViewController.self }) {
            self.navigationController?.viewControllers.remove(at: index)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.barcodeCapture?.isEnabled = true
        camera?.switch(toDesiredState: .on)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.barcodeCapture?.isEnabled = false
        camera?.switch(toDesiredState: .off)
    }
    
    @objc func onHelp() {
        if let vc = UIStoryboard(name: "Home", bundle: Bundle.main).instantiateViewController(identifier: "TutorialScanViewController") as? TutorialScanViewController {
            vc.fromHelp = true
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func onFlash(_ sender: Any) {
        flashStatus = flashStatus == .off ? .on : .off
    }
    
    // MARK: Private Methods
    private func configureSDK() {
        let settings = BarcodeCaptureSettings()
        settings.set(symbology: .qr, enabled: true)
        settings.set(symbology: .dataMatrix, enabled: true)
        
        barcodeCapture = BarcodeCapture(context: context, settings: settings)
        
        let cameraSettings = BarcodeCapture.recommendedCameraSettings
        
        camera = Camera.default
        camera?.apply(cameraSettings)
        
        context.setFrameSource(camera, completionHandler: nil)
        
        let captureView = DataCaptureView(context: context, frame: barcodeFrameView.bounds)
        captureView.context = context
        captureView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        barcodeFrameView.addSubview(captureView)
    }
    
    @IBAction func pop(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ConstSegue.resultSegue, let resultViewController = segue.destination as? ResultContainerViewController, let encoded2DDoc = sender as? String {
            resultViewController.encodedBarcode = encoded2DDoc
            resultViewController.typeBarcode = 1
        }
        
        if segue.identifier == ConstSegue.tutorialResultSegue, let vc = segue.destination as? TutorialResultViewController, let encoded2DDoc = sender as? String {
            vc.encodedBarcode = encoded2DDoc
            vc.typeBarcode = 1
            vc.modeSelected = PremiumManager.shared.modeSelected
        }
    }
}

extension ScanViewController: BarcodeCaptureListener {
    
    func barcodeCapture(_ barcodeCapture: BarcodeCapture, didScanIn session: BarcodeCaptureSession, frameData: FrameData) {
        guard let barcode = session.newlyRecognizedBarcodes.first else { return }
        self.barcodeCapture?.isEnabled = false
        DispatchQueue.main.async {
            self.scanViewPresenter.proceedToTreatment(with: barcode.data ?? "") {
                self.barcodeCapture?.isEnabled = true
            }
        }
    }
}

extension ScanViewController: ScanView {
    func showLoading() {
        DispatchQueue.main.async {
            self.loadingView.fadeIn()
            self.loadingView.isHidden = false
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.loadingView.fadeOut() {
                self.loadingView.isHidden = true
            }
        }
    }
    
    
    func showAlert(withTitle title: String, withMessage message: String, andAction action: UIAlertAction) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(action)
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showDocumentTypeView(during time: Double) {
        DispatchQueue.main.async {
            if self.documentTypeView.isHidden {
                self.documentTypeView.isHidden = false
                Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(self.hideDocumentTypeView), userInfo: nil, repeats: false)
            }
        }
    }
    
    func goTo(segueIdentifier: String, data: Any? = nil) {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: segueIdentifier, sender: data)
        }
    }
    
    @objc private func hideDocumentTypeView() {
        DispatchQueue.main.async {
            self.documentTypeView.isHidden = true
        }
    }
    func popToHome(){
        DispatchQueue.main.async {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
}
