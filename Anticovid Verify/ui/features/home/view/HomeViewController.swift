//
//  ViewController.swift
//  Anticovid Verify
//
//

import UIKit
import Combine

class HomeViewController: INViewController {
    
    @IBOutlet private weak var scanQrCodeOr2DDocButton: UIButton!
    @IBOutlet weak var passSwitch: UISwitch!
    
    var homeViewPresenter: HomeViewPresenter!
    weak var premiumVC : PremiumViewController?
    final let barcodeSegue = "barcodeSegue"
    final let privacySegue = "privacySegue"
    final let tutorialScanSegue = "tutorialScanSegue"
    final let otherSegue = "otherSegue"
    final let datePickerSegue = "datePickerSegue"
    
    @IBOutlet weak var passTitleLabel: INLabel!
    @IBOutlet weak var passDescriptionLabel: INLabel!
    @IBOutlet weak var vaccineSwitchLabel: INLabel!
    @IBOutlet weak var vaccineFeatureView: UIStackView!
    var datePickerConfiguration: DatePickerConfiguration!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        homeViewPresenter = HomeViewPresenter(self)
    }
    
    @IBAction func passValueChange(_ sender: Any) {
        homeViewPresenter.switchToPassVaccinal(passSwitch.isOn, shouldShowResultTutorial: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Vérification"
        self.scanQrCodeOr2DDocButton.tintColor = .goldIn
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "ellipsis"), style: .plain, target: self, action: #selector(onInfo))
        
        scanQrCodeOr2DDocButton.titleLabel?.numberOfLines = 0
        scanQrCodeOr2DDocButton.tintColor = .goldIn
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        homeViewPresenter.handleWillAppear()
        
        if UserDataManager.sharedInstance.shouldShowPrivacy {
            performSegue(withIdentifier: privacySegue, sender: nil)
        }
        
        if SyncManager.shared.actualAppSyncRemindStep() != .none {
            NotificationCenter.default.post(name: ConstNotification.updateBannerSyncApp, object: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let premiumVC = segue.destination as? PremiumViewController{
            premiumVC.homeVC = self
            self.premiumVC = premiumVC
            premiumVC.view.translatesAutoresizingMaskIntoConstraints = false
        }
    }
}

// MARK: IBActions
private extension HomeViewController {

    @IBAction func scanBarcode(_ sender: Any) {
        guard self.homeViewPresenter.canScanBarcode() else { return }
        if UserDataManager.sharedInstance.shouldShowScanTutorial {
            self.performSegue(withIdentifier: tutorialScanSegue, sender: nil)
        } else {
            self.performSegue(withIdentifier: barcodeSegue, sender: nil)
        }
    }
    
    @objc func onInfo() {
        self.navigationController?.parent?.performSegue(withIdentifier: otherSegue, sender: nil)
    }
}

extension HomeViewController: HomeView {
    func showVaccinalPassFeature(_ value: Bool) {
        self.vaccineFeatureView.isHidden = !value
    }
    
    func showHealthPassUI(description: String) {
        vaccineSwitchLabel.text = "Activer le pass vaccinal"
        vaccineSwitchLabel.textColor = .blueIn
        passTitleLabel.text = "Pass sanitaire :"
        passDescriptionLabel.text = description
        scanQrCodeOr2DDocButton.setAttributedTitle(homeViewPresenter.scanButtonTitle(), for: .normal)
    }
    
    func showVaccinationPassUI(description: String) {
        vaccineSwitchLabel.text = "Désactiver le pass vaccinal"
        vaccineSwitchLabel.textColor = .secondSubtitle
        passTitleLabel.text = "Pass vaccinal :"
        passDescriptionLabel.text = description
        scanQrCodeOr2DDocButton.setAttributedTitle(homeViewPresenter.scanButtonTitle(), for: .normal)
    }
   
    func refreshScanButton() {
        scanQrCodeOr2DDocButton.setAttributedTitle(homeViewPresenter.scanButtonTitle(), for: .normal)
    }
    
    func refreshPassFeature(){
        homeViewPresenter.refreshScanFeature()
    }
    
    func displayCanScan(_ value: Bool) {
        scanQrCodeOr2DDocButton.isEnabled = value
        scanQrCodeOr2DDocButton.tintColor = value ? .goldIn : .black
        refreshScanButton()
    }

    func swithToPassVaccinal(_ value: Bool) {
        passSwitch.setOn(value, animated: true)
    }
}
