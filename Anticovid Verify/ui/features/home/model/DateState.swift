//
//  DateState.swift
//  Anticovid Verify
//
//

import Foundation
public enum DateState: Codable, Equatable {
    case actual
    case custom(Date)
    
    func getDate() -> Date {
        switch self {
        case .actual:
            return Date()
        case .custom(let date):
            return date
        }
    }
    
    public static func == (lhs: DateState, rhs: DateState) -> Bool {
        switch lhs {
        case .actual:
            switch rhs {
            case .actual:
                return true
            case .custom:
                return false
            }
        case .custom(let date):
            switch rhs {
            case .actual:
                return false
            case .custom(let date2):
                return date == date2
            }
        }
    }

}
