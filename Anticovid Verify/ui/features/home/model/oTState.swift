//
//  oTState.swift
//  Anticovid Verify
//
//

import Foundation

@objc enum OtState: Int {
    case goInState
    case goOutState
}
