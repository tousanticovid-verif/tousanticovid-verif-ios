//
//  AboutViewController.swift
//  Anticovid Verify
//
//

import UIKit

class AboutViewController: INViewController {
    
    @IBOutlet weak var versionLabel: INLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.text = ConstConf.version != nil ? "Version " + ConstConf.version! : ""
    }
    
    @IBAction func onTouchLink(_ sender: UIButton) {
        if let label = sender.titleLabel?.text, let url = URL(string: "https://\(label)") {
            UIApplication.shared.open(url)
        }
    }
}
