//
//  WkWebViewContainerController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class WkWebViewContainerController: INViewController {

    var titleValue: String = ""

    var wkWebViewData: WkWebViewData!

    @IBOutlet weak var wkWebViewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        try? initialized()
    }

    func initialized() throws {
        self.title = titleValue
        
        replaceChild()
    }

    func replaceChild() {
        let currentChild = self.children.first

        let appViewControllerChild = wkWebViewData.type == .HTLM ?
            ViewControllerIntanciator.getWKWebViewController(contentOnLoad: wkWebViewData.value, transparent: true) :
            ViewControllerIntanciator.getWKWebViewController(urlOnLoad: wkWebViewData.value, transparent: true)

        currentChild?.willMove(toParent: nil)
        wkWebViewContainer.frame = appViewControllerChild.view.frame
        appViewControllerChild.view.alpha = 0

        self.addChild(appViewControllerChild)
        self.view.isUserInteractionEnabled = false

        self.transition(from: currentChild!, to: appViewControllerChild, duration: 0.25, options: [.curveEaseInOut], animations: {
            appViewControllerChild.view.alpha = 1
            currentChild?.view.alpha = 0
        }, completion: { _ in
           currentChild?.removeFromParent()
           appViewControllerChild.didMove(toParent: self)
           self.view.isUserInteractionEnabled = true
        })
     }

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        setUIDocumentMenuViewControllerSourceViewsIfNeeded(viewControllerToPresent)
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }

    func setUIDocumentMenuViewControllerSourceViewsIfNeeded(_ viewControllerToPresent: UIViewController) {
        if #available(iOS 13, *), viewControllerToPresent is UIDocumentMenuViewController && UIDevice.current.userInterfaceIdiom == .phone {
            viewControllerToPresent.popoverPresentationController?.sourceView = self.view
            viewControllerToPresent.popoverPresentationController?.sourceRect = CGRect(x: self.view.center.x, y: self.view.center.y, width: 1, height: 1)
        }
    }
}
