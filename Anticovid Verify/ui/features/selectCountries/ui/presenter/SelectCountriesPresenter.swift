//
//  SelectCountriesPresenter.swift
//  Anticovid Verify
//
//

import Foundation
import OSLog

protocol SelectCountriesViewProtocol {
    func refresh()
    func close(with configuration: SelectCountriesConfiguration)
    func displayAlertWhenIncorrectLengthFavoriteCountries()
    func displayAlertWhenOneCountrySelectedIshHasFavorited(with stringContent: String, completion: @escaping () -> Void)
}

struct Section {
    let title: String
    let countries: [Country]
}

class SelectCountriesPresenter {
    
    private let countriesManager: CountriesManager
    private let selectCountriesView: SelectCountriesViewProtocol
    private var selectCountriesConfiguration: SelectCountriesConfiguration!
    private var search: String = ""
    private let otDataManager : OtDataManager
    lazy var dataSource: [Section] = {
        getData()
    }()
    
    init(countriesManager: CountriesManager = .shared, countriesSearchView: SelectCountriesViewProtocol, otDataManager: OtDataManager = .shared) {
        self.countriesManager = countriesManager
        self.selectCountriesView = countriesSearchView
        self.otDataManager = otDataManager
    }
    
    func initializeData(selectCountriesConfiguration: SelectCountriesConfiguration) {
        self.selectCountriesConfiguration = selectCountriesConfiguration
    }
    
    func getConfiguration() -> SelectCountriesConfiguration {
        return selectCountriesConfiguration
    }
    
    func updateSearch(with search: String) {
        self.search = search
        self.selectCountriesView.refresh()
    }
    
    func selectCountry(country: Country) {
        switch selectCountriesConfiguration.selectCountryMode {
            
        case .selectOneCountry:
            self.selectCountriesConfiguration.countries = [country]
            
            if(selectCountriesConfiguration.mode == .goInState) {
                if((otDataManager.goInData.favoritesCountries.contains(where: { $0.nameCode == country.nameCode }) == true)) {
                    self.selectCountriesView.displayAlertWhenOneCountrySelectedIshHasFavorited(with: "Le pays de contrôle a été supprimé des pays de départ", completion: {
                        self.otDataManager.goInData.favoritesCountries.removeAll(where: { $0.nameCode == country.nameCode })
                        self.selectCountriesView.close(with: self.selectCountriesConfiguration)
                    })
                } else {
                    self.selectCountriesView.close(with: self.selectCountriesConfiguration)
                }
            } else {
                if((otDataManager.goOutData.favoritesCountries.contains(where: { $0.nameCode == country.nameCode }) == true)) {
                    self.selectCountriesView.displayAlertWhenOneCountrySelectedIshHasFavorited(with: "Le pays de contrôle a été supprimé des destinations", completion: {
                        self.otDataManager.goOutData.favoritesCountries.removeAll(where: { $0.nameCode == country.nameCode })
                        self.selectCountriesView.close(with: self.selectCountriesConfiguration)
                    })
                } else {
                    self.selectCountriesView.close(with: self.selectCountriesConfiguration)
                }
            }
            
        case .selectCountries(let numberMax, _):
            let isAlreadyAFavorite = !self.selectCountriesConfiguration.countries
                .filter({ $0.nameCode == country.nameCode && $0.name == country.name })
                .isEmpty

            if isAlreadyAFavorite {
                self.selectCountriesConfiguration.countries = self.selectCountriesConfiguration.countries.filter { $0.nameCode != country.nameCode && $0.name != country.name }
            } else {
                if selectCountriesConfiguration.countries.count < numberMax {
                    self.selectCountriesConfiguration.countries.append(country)
                } else {
                    self.selectCountriesView.displayAlertWhenIncorrectLengthFavoriteCountries()
                }
            }
            
            dataSource = getData()
            self.selectCountriesView.refresh()
        }
    }
}

private extension SelectCountriesPresenter {
    func getData() -> [Section] {
        
        let configration = getConfiguration()
        
        switch selectCountriesConfiguration.selectCountryMode {
        case .selectOneCountry(let isNational):
            let exceptCountriesCode = selectCountriesConfiguration.exceptCountry.map { $0.nameCode }
            
            return isNational ?
                [Section(
                    title: "Pays - Régions",
                    countries:
                        countriesManager.countries
                        .filter { $0.name.lowercased().contains(search.lowercased()) || search.isEmpty }
                        .filter{ $0.isNational && !exceptCountriesCode.contains($0.nameCode) }
                )]
                : [Section(
                    title: "Pays - Régions",
                    countries: countriesManager.countries
                        .filter { $0.name.lowercased().contains(search.lowercased()) || search.isEmpty }
                        .filter{ !exceptCountriesCode.contains($0.nameCode)}
                )]
        case .selectCountries(_, let isNational):
            let exceptCountriesCode = selectCountriesConfiguration.exceptCountry.map { $0.nameCode }
            
            let favoris = self.selectCountriesConfiguration.countries
                .filter { ($0.name.lowercased().contains(search.lowercased()) || search.isEmpty) }
                .filter { !exceptCountriesCode.contains($0.nameCode)}
                .filter { isNational ? $0.isNational : true }
                .filter { selectCountriesConfiguration.mode == .goInState ? $0.isDeparture : $0.isArrival }
                .map { country -> Country in
                    var countryTmp = country
                    countryTmp.isFavorite = true
                    return countryTmp
                }
            
            let countries = countriesManager.countries
                .filter { ($0.name.lowercased().contains(search.lowercased()) || search.isEmpty) }
                .filter { !exceptCountriesCode.contains($0.nameCode) }
                .filter { isNational ? $0.isNational : true }
                .filter { selectCountriesConfiguration.mode == .goInState ? $0.isDeparture : $0.isArrival }
                .map { country -> Country in
                    var countryTmp = country
                    countryTmp.isFavorite = !favoris.filter({ $0.nameCode == country.nameCode && $0.name == country.name }).isEmpty
                    return countryTmp
                }
            
            return [Section(title: "Favoris", countries: favoris.filter { configration.mode == .goOutState ? $0.nameCode != "AUTRES" : true }),
                Section(title: "Pays - Régions", countries: countries.filter { configration.mode == .goOutState ? $0.nameCode != "AUTRES" : true })
            ]
        }
    }
}
