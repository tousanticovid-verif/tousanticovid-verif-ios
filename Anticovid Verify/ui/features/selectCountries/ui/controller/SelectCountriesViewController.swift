//
//  SelectCountriesViewController.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

protocol SelectCountriesDelegate {
    func closeResult(with configuration: SelectCountriesConfiguration)
}

class SelectCountriesViewController: UIViewController {
    
    @IBOutlet weak var countriesTableView: UITableView!

    private let searchController = UISearchController(searchResultsController: nil)
    private var countriesPresenter: SelectCountriesPresenter!

    var delegate: SelectCountriesDelegate!
    var selectCountriesConfiguration: SelectCountriesConfiguration!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.countriesPresenter = SelectCountriesPresenter(countriesSearchView: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.countriesTableView.delegate = self
        self.countriesTableView.dataSource = self
        self.countriesTableView.register(UINib(nibName: "CountryCell", bundle: Bundle.main), forCellReuseIdentifier: CountryTableViewCell.tableViewCellIdentifier)

        searchController.searchBar.searchTextField.tintColor = .black
        searchController.searchBar.searchTextField.backgroundColor = .white

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Séléctionner votre pays"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        self.configTabBar()
        self.countriesPresenter.initializeData(selectCountriesConfiguration: selectCountriesConfiguration)
    }
    
    private func configTabBar() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Annuler", style: .done, target: self, action: #selector(self.cancelSelectViewController))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Valider", style: .done, target: self, action: #selector(self.doneActionWhenSelect))
    }
}

extension SelectCountriesViewController: UISearchResultsUpdating {
    
  func updateSearchResults(for searchController: UISearchController) {
    self.countriesPresenter.updateSearch(with: searchController.searchBar.text ?? "")
  }
}

extension SelectCountriesViewController: SelectCountriesViewProtocol {
    
    func close(with configuration: SelectCountriesConfiguration) {
        self.resignFirstResponder()
        self.delegate.closeResult(with: configuration)
        self.dismiss(animated: true, completion: nil)
    }
    
    func refresh() {
        self.countriesTableView.reloadData()
    }

    func displayAlertWhenIncorrectLengthFavoriteCountries() {
        let alert = UIAlertController(
            title: "Max. de pays favoris atteint",
            message:"Vous ne pouvez séléctionner que 4 pays favoris maximum",
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayAlertWhenOneCountrySelectedIshHasFavorited(with stringContent: String, completion: @escaping () -> Void) {
        let alert = UIAlertController(
            title: "Sélection d'un pays favori",
            message: stringContent,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { UIAlertAction in
            NSLog("OK Pressed")
            completion()
        })
        self.present(alert, animated: true, completion: nil)
    }
}

extension SelectCountriesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.countriesPresenter.dataSource[section].countries.isEmpty ? 0 : 44
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.countriesPresenter.dataSource[section].title
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.countriesPresenter.dataSource.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countriesPresenter.dataSource[section].countries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let country = self.countriesPresenter.dataSource[indexPath.section].countries[indexPath.row]
        let conf = self.countriesPresenter.getConfiguration()
        guard let countryTableViewCell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.tableViewCellIdentifier) as? CountryTableViewCell else {
                return UITableViewCell()
        }
        countryTableViewCell.selectionStyle = .none
        countryTableViewCell.configureData(country: country, selectCountryMode: conf.selectCountryMode)
        return countryTableViewCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = self.countriesPresenter.dataSource[indexPath.section].countries[indexPath.row]
        self.countriesPresenter.selectCountry(country: country)
    }
}

extension SelectCountriesViewController {
    
    @objc func cancelSelectViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func doneActionWhenSelect() {
        self.delegate.closeResult(with: self.countriesPresenter.getConfiguration())
        self.dismiss(animated: true, completion: nil)
    }
}
