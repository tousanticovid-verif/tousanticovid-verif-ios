//
//  CountryTableViewCell.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

class CountryTableViewCell: UITableViewCell {
    
    static let tableViewCellIdentifier = "countryTableViewCell"
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var flagCountryImageView: UIImageView!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var favoriCountryUIButton: UIButton!
    
    func configureData(country: Country, selectCountryMode: SelectCountryMode) {
        flagCountryImageView.image = country.getFlag()
        countryLabel.text = country.name
        
        switch selectCountryMode {
        case .selectOneCountry:
            self.favoriCountryUIButton.isHidden = true
        default:
            self.favoriCountryUIButton.setImage(UIImage(systemName: country.isFavorite ? "star.fill": "star"), for: .normal)
        }
    }
}
