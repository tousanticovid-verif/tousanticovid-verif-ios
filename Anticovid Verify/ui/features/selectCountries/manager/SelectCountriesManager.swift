//
//  SelectCountriesManager.swift
//  Anticovid Verify
//
//

import Foundation
import UIKit

struct SelectCountriesConfiguration {
    let selectCountryMode: SelectCountryMode
    var countries: [Country]
    var exceptCountry: [Country]
    let mode: OtState
}

class SelectCountriesManager {

    func openSelectedCountries(selectedCountryDelegate: SelectCountriesDelegate, selectCountriesConfiguration: SelectCountriesConfiguration) {
        DispatchQueue.main.async {
            let currentViewController = self.getTopViewController()
            let countriesController = self.getCountriesController()
            
            countriesController.selectCountriesConfiguration = selectCountriesConfiguration
            countriesController.delegate = selectedCountryDelegate
            
            let navigationController = UINavigationController(rootViewController: countriesController)
            currentViewController?.present(navigationController, animated: true, completion: nil)
        }
    }

    func getCountriesController() -> SelectCountriesViewController {
        let storyboard = UIStoryboard(name: "SelectCountries" , bundle: Bundle.main)
        return (storyboard.instantiateViewController(withIdentifier: "SelectCountriesViewController") as? SelectCountriesViewController)!
    }

    private func getTopViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return getTopViewController(controller: navigationController.visibleViewController)
        }
        else if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return getTopViewController(controller: selected)
            }
        }
        else if let presented = controller?.presentedViewController {
            return getTopViewController(controller: presented)
        }
        return controller
    }
}
