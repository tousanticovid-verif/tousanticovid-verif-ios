// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name INCommunication
import Combine
import Foundation
import INCore
import Swift
import SystemConfiguration
import UIKit
import _Concurrency
import os.log
import os
extension Foundation.NSError {
  public static var NoInternet: Foundation.NSError {
    get
  }
}
extension Swift.Error {
  public func isReachabilityError() -> Swift.Bool
}
public enum INErrorApi : Swift.String {
  case BUSI_COMMON_1
  case BUSI_COMMON_2
  case BUSI_COMMON_3
  case BUSI_COMMON_4
  case BUSI_MANAGE_1
  case BUSI_MANAGE_2
  case BUSI_MANAGE_3
  case BUSI_MANAGE_4
  case BUSI_MANAGE_5
  case BUSI_MANAGE_6
  case BUSI_MANAGE_7
  case BUSI_MANAGE_8
  case BUSI_MANAGE_9
  case BUSI_MANAGE_10
  case BUSI_MANAGE_13
  case BUSI_MANAGE_14
  case BUSI_MANAGE_15
  case BUSI_MANAGE_16
  case BUSI_MANAGE_17
  case BUSI_MANAGE_18
  case BUSI_MANAGE_19
  case BUSI_MANAGE_20
  case BUSI_MANAGE_21
  case BUSI_MANAGE_22
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct INHTTPMethod : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable {
  public static let delete: INCommunication.INHTTPMethod
  public static let get: INCommunication.INHTTPMethod
  public static let patch: INCommunication.INHTTPMethod
  public static let post: INCommunication.INHTTPMethod
  public static let put: INCommunication.INHTTPMethod
  public let rawValue: Swift.String
  public init(rawValue: Swift.String)
  public typealias RawValue = Swift.String
}
public enum INHTTPStatus : Swift.Int {
  case jsonDecodingError
  case noDataError
  case transformToRequestError
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public struct INJsonEncoding : INCommunication.INParameterEncoding {
  public static var `default`: INCommunication.INJsonEncoding {
    get
  }
  public static var prettyPrinted: INCommunication.INJsonEncoding {
    get
  }
  public let options: Foundation.JSONSerialization.WritingOptions
  public init(options: Foundation.JSONSerialization.WritingOptions = [])
  public func encode(_ urlRequest: INCommunication.INURLRequestConvertible, with parameters: INCommunication.INParameters?) throws -> Foundation.URLRequest
  public func encode(_ urlRequest: INCommunication.INURLRequestConvertible, withJSONObject jsonObject: Any? = nil) throws -> Foundation.URLRequest
}
public enum ServerTrustError : Swift.Error {
  public enum ServerTrustFailureReason {
    public struct Output {
      public let host: Swift.String
      public let trust: Security.SecTrust
      public let status: Darwin.OSStatus
      public let result: Security.SecTrustResultType
    }
    case settingAnchorCertificatesFailed(status: Darwin.OSStatus, certificates: [Security.SecCertificate])
    case noCertificatesFound
    case certificatePinningFailed(host: Swift.String, trust: Security.SecTrust, pinnedCertificates: [Security.SecCertificate], serverCertificates: [Security.SecCertificate])
    case policyApplicationFailed(trust: Security.SecTrust, policy: Security.SecPolicy, status: Darwin.OSStatus)
    case trustEvaluationFailed(error: Swift.Error?)
    case defaultEvaluationFailed(output: INCommunication.ServerTrustError.ServerTrustFailureReason.Output)
  }
  case serverTrustEvaluationFailed(reason: INCommunication.ServerTrustError.ServerTrustFailureReason)
}
public protocol ServerTrustEvaluating {
  func evaluate(_ trust: Security.SecTrust, forHost host: Swift.String) throws
}
final public class DisabledTrustEvaluator : INCommunication.ServerTrustEvaluating {
  public init()
  final public func evaluate(_ trust: Security.SecTrust, forHost host: Swift.String) throws
  @objc deinit
}
final public class PinnedCertificatesTrustEvaluator : INCommunication.ServerTrustEvaluating {
  public init(certificates: [Security.SecCertificate] = [], acceptSelfSignedCertificates: Swift.Bool = false, defaultValidation: Swift.Bool = true, validateHost: Swift.Bool = true)
  final public func evaluate(_ trust: Security.SecTrust, forHost host: Swift.String) throws
  @objc deinit
}
extension Foundation.Bundle {
  public func getCertificates(withName name: Swift.String) -> [Security.SecCertificate]
}
extension Security.SecTrust {
  public func getCertificates() -> [Security.SecCertificate]
  public func getCertificateData() -> [Foundation.Data]
}
public protocol INURLRequestConvertible {
  func asURLRequest() throws -> Foundation.URLRequest
}
extension INCommunication.INURLRequestConvertible {
  public var urlRequest: Foundation.URLRequest? {
    get
  }
}
public protocol INURLConvertible {
  func asURL() throws -> Foundation.URL
}
extension Swift.String : INCommunication.INURLConvertible {
  public func asURL() throws -> Foundation.URL
}
public typealias INParameters = [Swift.String : Any]
public protocol INParameterEncoding {
  func encode(_ urlRequest: INCommunication.INURLRequestConvertible, with parameters: INCommunication.INParameters?) throws -> Foundation.URLRequest
}
public typealias INRequestModifier = (inout Foundation.URLRequest) throws -> Swift.Void
extension Foundation.URLRequest : INCommunication.INURLRequestConvertible {
  public func asURLRequest() throws -> Foundation.URLRequest
}
extension Foundation.URLRequest {
  public init(url: INCommunication.INURLConvertible, method: INCommunication.INHTTPMethod, headers: INCommunication.INHTTPHeaders? = nil) throws
}
public enum INRequestError : Swift.Error {
  case invalidURL(url: INCommunication.INURLConvertible)
  case parameterEncodingFailed(reason: INCommunication.INRequestError.INParameterEncodingFailureReason)
  public enum INParameterEncodingFailureReason {
    case missingURL
    case jsonEncodingFailed(error: Swift.Error)
    case customEncodingFailed(error: Swift.Error)
  }
}
public typealias INHTTPHeaders = [Swift.String : Swift.String]
public typealias INErrorResult = (Swift.Int, Swift.Error?)
public struct EmptyBody : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
@available(iOS 13.0, *)
extension INCommunication.INRequestService {
  public func executeRequest<D>(_ requestConvertible: INCommunication.INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: Foundation.JSONDecoder.DateDecodingStrategy = .deferredToDate, headers: INCommunication.INHTTPHeaders? = nil, p12: INCore.INPKCS12? = nil, httpShouldSetCookies: Swift.Bool = true, timeout: Swift.Int? = 20, pinCertificates: [Security.SecCertificate] = [SecCertificate]()) -> Combine.Future<D, Swift.Error> where D : Swift.Decodable, D : Swift.Encodable
}
@available(iOS 13, *)
extension INCommunication.INRequestService {
  public func executeRequest<D>(_ requestConvertible: INCommunication.INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: Foundation.JSONDecoder.DateDecodingStrategy = .deferredToDate, headers: INCommunication.INHTTPHeaders? = nil, p12: INCore.INPKCS12? = nil, httpShouldSetCookies: Swift.Bool = true, timeout: Swift.Int? = 10, pinCertificates: [Security.SecCertificate] = [SecCertificate](), tlsProtocolVersionT: Security.tls_protocol_version_t? = nil, completionHandler: @escaping (_ status: Swift.Int, _ result: D?, _ error: Swift.Error?, _ data: Foundation.Data?) -> ()) where D : Swift.Decodable, D : Swift.Encodable
  public func executeRequest<D, E>(_ requestConvertible: INCommunication.INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: Foundation.JSONDecoder.DateDecodingStrategy = .deferredToDate, errorType: E.Type?, headers: INCommunication.INHTTPHeaders? = nil, p12: INCore.INPKCS12? = nil, httpShouldSetCookies: Swift.Bool = true, timeout: Swift.Int? = 10, pinCertificates: [Security.SecCertificate] = [SecCertificate](), useCache: Swift.Bool = false, tlsProtocolVersionT: Security.tls_protocol_version_t? = nil, completionHandler: @escaping (_ status: Swift.Int, _ result: D?, _ error: Swift.Error?, _ data: Foundation.Data?) -> ()) where D : Swift.Decodable, D : Swift.Encodable, E : Swift.Decodable, E : Swift.Encodable
}
@available(iOS 13, *)
extension INCommunication.INRequestService {
  public func executeRequest<D>(_ requestConvertible: INCommunication.INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: Foundation.JSONDecoder.DateDecodingStrategy = .deferredToDate, headers: INCommunication.INHTTPHeaders? = nil, p12: INCore.INPKCS12? = nil, httpShouldSetCookies: Swift.Bool = true, timeout: Swift.Int? = 10, pinCertificates: [Security.SecCertificate] = [SecCertificate](), completionHandler: @escaping (_ status: Swift.Int, _ result: D?, _ error: Swift.Error?, _ data: Foundation.Data?) -> ()) where D : Swift.Decodable, D : Swift.Encodable
  public func executeRequest<D, E>(_ requestConvertible: INCommunication.INURLRequestConvertible, type: D.Type, typeDateDecoderStrategy: Foundation.JSONDecoder.DateDecodingStrategy = .deferredToDate, errorType: E.Type?, headers: INCommunication.INHTTPHeaders? = nil, p12: INCore.INPKCS12? = nil, httpShouldSetCookies: Swift.Bool = true, timeout: Swift.Int? = 10, pinCertificates: [Security.SecCertificate] = [SecCertificate](), useCache: Swift.Bool = false, completionHandler: @escaping (_ status: Swift.Int, _ result: D?, _ error: Swift.Error?, _ data: Foundation.Data?) -> ()) where D : Swift.Decodable, D : Swift.Encodable, E : Swift.Decodable, E : Swift.Encodable
}
@_hasMissingDesignatedInitializers public class INRequestService {
  public static let shared: INCommunication.INRequestService
  final public let decoder: Foundation.JSONDecoder
  public var uRLSessionDataTask: Foundation.URLSessionDataTask?
  @objc deinit
}
extension INCommunication.INErrorApi : Swift.Equatable {}
extension INCommunication.INErrorApi : Swift.Hashable {}
extension INCommunication.INErrorApi : Swift.RawRepresentable {}
extension INCommunication.INHTTPStatus : Swift.Equatable {}
extension INCommunication.INHTTPStatus : Swift.Hashable {}
extension INCommunication.INHTTPStatus : Swift.RawRepresentable {}
