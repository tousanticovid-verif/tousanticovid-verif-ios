// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target arm64-apple-ios11.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name INCore
import CommonCrypto
import CoreTelephony
import Foundation
import Swift
import UIKit
import _Concurrency
import os.log
import os
extension Foundation.Date {
  public func toString() -> Swift.String
  public func toFormattedString(_ format: Swift.String) -> Swift.String
}
extension Swift.String : INCore.Localizable {
  public var localized: Swift.String {
    get
  }
  public func localizedWithIntParameter(int: Swift.Int) -> Swift.String
  public func localizedWithStringParameter(string: Swift.String) -> Swift.String
  public var sha256: Foundation.Data? {
    get
  }
  public var hexToData: Foundation.Data {
    get
  }
  public func timestampToDate() -> Foundation.Date?
  public func toDate(format: Swift.String = "dd/MM/yyyy") -> Foundation.Date?
}
@_hasMissingDesignatedInitializers public class INContainerManager {
  public static let sharedInstance: INCore.INContainerManager
  open func createContainerFile(content: Swift.String)
  open func hasContainer() -> Swift.Bool
  open func getContainerHash() -> Swift.String?
  open func deleteContainer()
  open func getContainer(containerKey: Swift.String) -> INCore.INContainer?
  @objc deinit
}
@_hasMissingDesignatedInitializers open class INPhoneFingerPrintManager {
  public static func getPhoneFingerPrint(additionalData: Swift.String?) throws -> Swift.String
  @objc deinit
}
@_hasMissingDesignatedInitializers open class INContainer : Swift.Codable {
  final public let secret: Swift.String
  final public let p12: [Swift.Int8]
  open func getPKCS12() -> INCore.INPKCS12
  @objc deinit
  open func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
public struct AES {
  public init(keyString: Swift.String) throws
  public init(key: Foundation.Data) throws
}
extension INCore.AES {
  public func encrypt(_ dataToEncrypt: Foundation.Data) throws -> Foundation.Data
  public func decrypt(_ data: Foundation.Data) throws -> Foundation.Data
  public static func generateKey() throws -> Foundation.Data
}
public struct INAES256Cryptor {
  public static func encrypt(_ digest: Foundation.Data, key: Foundation.Data) throws -> Foundation.Data
  public static func decrypt(_ encrypted: Foundation.Data, key: Foundation.Data) throws -> Foundation.Data
  public static func generateKey(size: Swift.Int) throws -> Foundation.Data
}
public enum INCoreKeychainKeys : Swift.String, Swift.CaseIterable {
  case uuid
  case container
  case identifier
  case fingerprintMigrationOK
  case inCarrierName
  case inCarrierCode
  public init?(rawValue: Swift.String)
  public typealias AllCases = [INCore.INCoreKeychainKeys]
  public typealias RawValue = Swift.String
  public static var allCases: [INCore.INCoreKeychainKeys] {
    get
  }
  public var rawValue: Swift.String {
    get
  }
}
open class INPKCS12 {
  open var certificate: Security.SecCertificate? {
    get
  }
  open var privateKey: Security.SecKey? {
    get
  }
  public init(_ data: Foundation.Data)
  public func urlCredential() -> Foundation.URLCredential?
  @objc deinit
}
@_hasMissingDesignatedInitializers open class INPersistentData {
  public static func getString(forKey key: Swift.String, encrypted: Swift.Bool = true, encoding: Swift.String.Encoding = .utf8, defaultValue: Swift.String = "") throws -> Swift.String
  public static func getInt(forKey key: Swift.String, encrypted: Swift.Bool = true, defaultValue: Swift.Int = 0) throws -> Swift.Int
  public static func getLong(forKey key: Swift.String, encrypted: Swift.Bool = true, defaultValue: Swift.Int64 = 0) throws -> Swift.Int64
  public static func getBool(forKey key: Swift.String, encrypted: Swift.Bool = true, defaultValue: Swift.Bool = false) throws -> Swift.Bool
  public static func saveString(key: Swift.String, value: Swift.String, encrypted: Swift.Bool = true, encoding: Swift.String.Encoding = .utf8) throws
  public static func saveInt(key: Swift.String, value: Swift.Int, encrypted: Swift.Bool = true) throws
  public static func saveLong(key: Swift.String, value: Swift.Int64, encrypted: Swift.Bool = true) throws
  public static func saveBool(key: Swift.String, value: Swift.Bool, encrypted: Swift.Bool = true) throws
  public static func remove(key: Swift.String, sensitive: Swift.Bool)
  public static func removeAllInKeychain()
  public static func removeAllInUserDefaults()
  public static func removeAll()
  public static func getKey(keyName: Swift.String) throws -> Foundation.Data
  @objc deinit
}
public protocol Localizable {
  var localized: Swift.String { get }
}
public protocol XIBLocalizable {
  var xibLocKey: Swift.String? { get set }
}
extension INCore.INCoreKeychainKeys : Swift.Equatable {}
extension INCore.INCoreKeychainKeys : Swift.Hashable {}
extension INCore.INCoreKeychainKeys : Swift.RawRepresentable {}
