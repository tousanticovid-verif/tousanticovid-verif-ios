//
//  StatServiceTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
import OHHTTPStubs
import OHHTTPStubsSwift
import Combine
@testable import Anticovid_Verify

class StatServiceTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    private var onlineStatService: MockOnlineStatRepositoryService!
    
    private var localStatService: MockLocalStatRepositoryService!
    
    private var statService: StatService!
    
    private let host = ConstConf.urlWs?.replacingOccurrences(of: "https://", with: "").replacingOccurrences(of: "/", with: "").replacingOccurrences(of: "/api/", with: "") ?? ""
    
    private var cancellables = Set<AnyCancellable>()
    
    override func setUp() {
        super.setUp()
        
        onlineStatService = MockOnlineStatRepositoryService()
        localStatService = MockLocalStatRepositoryService()
        statService = StatService(
            onlineStatService: onlineStatService,
            localStatService: localStatService)
    }
    
    func testProceedToStatTreatmentWhenUserLocalStatDataisticsAgreementIsTrue() {
        statService.proceedToStatTreatment(statInformation: StatInformation(globalValidity:.invalid, controlType: "", ressourceType: ""), certificateAdminState: nil, certHash: "")

        XCTAssertTrue(onlineStatService.callWrite)
    }
    
    func testReadStat() {
        let result = statService.readStat()
        XCTAssert(result.0.isEmpty)
        XCTAssert(result.1.isEmpty)
    }
    
    func testEraseAllData() {
        statService.eraseAllData()
        XCTAssert(onlineStatService.callDeleteAllStatistics)
        XCTAssert(localStatService.callDeleteAllStatistics)
    }
    
    func testSendStatisticsWhenrResultIsEmpty() {
        onlineStatService.statistics = []
        
        statService.sendStat()
            .sink(receiveCompletion: { result in
                XCTAssertFalse(self.onlineStatService.callDeleteAllStatistics)
            }, receiveValue: nil)
            .store(in: &cancellables)
    }
    
    func testSendStatisticsWhenrResultIsNot200() {
        let onlineStatistic = OnlineStatistic(context: mockBDDConfig.getStatContext())
        onlineStatistic.value = "azertyuiop"
        onlineStatService.statistics = [
            onlineStatistic,
            onlineStatistic
        ]
        
        stub(condition: isHost(host)) { _ in
            return HTTPStubsResponse(data: ("".data(using: .utf8) ?? Data()), statusCode: 415, headers: nil)
        }
        
        statService.sendStat()
            .sink(receiveCompletion: { result in
                XCTAssertFalse(self.onlineStatService.callDeleteAllStatistics)
            }, receiveValue: nil)
            .store(in: &cancellables)
    }
    
    func testSendStatisticsWhenrResultIs200() {
        let onlineStatistic = OnlineStatistic(context: mockBDDConfig.getStatContext())
        onlineStatistic.value = "azertyuiop"
        onlineStatService.statistics = [
            onlineStatistic,
            onlineStatistic
        ]
        
        stub(condition: isHost(host)) { _ in
            return HTTPStubsResponse(data: ("".data(using: .utf8) ?? Data()), statusCode: 200, headers: nil)
        }
        
        statService.sendStat()
            .sink(receiveCompletion: { result in
                XCTAssertTrue(self.onlineStatService.callDeleteAllStatistics)
            }, receiveValue: nil)
            .store(in: &cancellables)
    }
}

class MockOnlineStatRepositoryService: OnlineStatRepository {
    
    var callDeleteAllStatistics = false
    override func deleteAllStatistics() -> Bool {
        callDeleteAllStatistics.toggle()
        return callDeleteAllStatistics
    }
    
    var statistics = [OnlineStatistic]()
    override func getStatistics() -> [OnlineStatistic] {
        return statistics
    }
    
    var callWrite = false
    override func write(stat: OnlineStatData) -> Bool {
        callWrite.toggle()
        return callWrite
    }
}

class MockLocalStatRepositoryService: LocalStatRepository {
    
    var callWrite = false
    override func write(stat: LocalStatData) -> Bool {
        callWrite.toggle()
        return callWrite
    }
    
    var callCertificateAlreadyScan = false
    override func certificateAlreadyScan(idHash: String) -> LocalCertStatistic? {
        callCertificateAlreadyScan.toggle()
        return nil
    }
    
    var callDeleteAllStatistics = false
    override func deleteAllStatistics() -> Bool {
        callDeleteAllStatistics.toggle()
        return callDeleteAllStatistics
    }
    
    override func getStatistics() -> [LocalStatistic] {
        return []
    }
}
