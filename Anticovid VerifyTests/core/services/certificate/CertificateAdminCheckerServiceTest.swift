//
//  CertificateAdminCheckerServiceTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest

@testable import Anticovid_Verify

class CertificateAdminCheckerServiceTest: XCTestCase {

    var localStatRepository: MockLocalStatRepository!
    
    var blackListRepository: MockBlackListRepository!
    
    var certificateAdminCheckerService: CertificateAdminCheckerService!
    
    override func setUp() {
        super.setUp()
        
        localStatRepository = MockLocalStatRepository()
        blackListRepository = MockBlackListRepository()
        certificateAdminCheckerService = CertificateAdminCheckerService(
            blackListRepository: blackListRepository,
            localStatRepository: localStatRepository
        )
    }
    
    func testGetCertificateAdminStateReturnBlackListAndDoubleWhenBlackRepositoryIsBlackListedReturnTrueAndCertAlreadyExistReturnLocalCertStatistic() {
        blackListRepository.isBlackListed = true
        localStatRepository.cert = LocalCertStatistic(context: MockBDDConfig.shared.getStatContext())
        
        let result = certificateAdminCheckerService.getCertificateAdminState(for: "hash")
        
        XCTAssertEqual(result, .blackListAndDouble)
    }
    
    func testGetCertificateAdminStateReturnBlackListWhenBlackRepositoryIsBlackListedReturnTrue() {
        blackListRepository.isBlackListed = true
        
        let result = certificateAdminCheckerService.getCertificateAdminState(for: "hash")
        
        XCTAssertEqual(result, .blacklist)
    }
    
    func testGetCertificateAdminStateReturnDoubleWhenBlackRepositoryIsBlackListedReturnFalseAndCertAlreadyExistReturnLocalCertStatistic() {
        blackListRepository.isBlackListed = false
        localStatRepository.cert = LocalCertStatistic(context: MockBDDConfig.shared.getStatContext())

        let result = certificateAdminCheckerService.getCertificateAdminState(for: "hash")
        
        XCTAssertEqual(result, .double)
    }

    func testGetCertificateAdminStateReturnNilWhenBlackRepositoryIsBlackListedReturnFalseAndCertAlreadyExistReturnNil() {
        blackListRepository.isBlackListed = false
        localStatRepository.cert = nil

        let result = certificateAdminCheckerService.getCertificateAdminState(for: "hash")
        
        XCTAssertNil(result)
    }
}

class MockLocalStatRepository: LocalStatRepository {
    
    var cert: LocalCertStatistic? = nil
    override func certificateAlreadyScan(idHash: String) -> LocalCertStatistic? {
        return cert
    }
}

class MockBlackListRepository: BlackListRepository {
    
    var isBlackListed: Bool = false
    override func isBlackListed(certHash: String, isDCC: Bool?) -> Bool {
        return isBlackListed
    }
}
