//
//  BarcodeVerifierTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
@testable import Anticovid_Verify

class BarcodeVerifierTest: XCTestCase {
    
    var barcodeVerifier: BarcodeService!

    override func setUp() {
        super.setUp()
        
        barcodeVerifier = BarcodeService()
    }
    
    func testIsValidPassCovid2DDocReturnFalseBecauseBarcodeLenghtInferiorThan22() {
        let barcode = "111111111111111"
        
        XCTAssertFalse(barcodeVerifier.isValidPassCovid2DDoc(barcode: barcode))
    }
    
    func testIsValidPassCovid2DDocReturnFalseBecauseBarcodeLenghtIsGoodButDocumentTypeIncorrect() {
        let barcode = "1111111111111111111111111111111111111111111111111111111111111111111111111"
        
        XCTAssertFalse(barcodeVerifier.isValidPassCovid2DDoc(barcode: barcode))
    }
    
    func testIsValidPassCovid2DDocReturnTrue() {
        let barcode = "DC04FR0000011E651E66B201FRF0PIERRE ANDRE\u{1D}F1HUART\u{1D}F213061992F3MF4943092\u{1D}F5NF6010120210950\u{1F}6CWIXXC26EEJUMP23SG7NCWD7A7EDLPGJYJDF7IR7OO4WSUXXO3KSXB2ZLGQA3SJTMHC2SBQGRW2N2NGXZDNYXYIZNYN2QXLHRN3ULQ"
        
        XCTAssertTrue(barcodeVerifier.isValidPassCovid2DDoc(barcode: barcode))
    }
}
