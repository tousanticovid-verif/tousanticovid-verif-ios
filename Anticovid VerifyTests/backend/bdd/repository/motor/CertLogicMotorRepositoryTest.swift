//
//  CertLogicMotorRepositoryTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
import CertLogic
import SwiftyJSON

@testable import Anticovid_Verify

class CertLogicMotorRepositoryTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    var context: NSManagedObjectContext!
    
    var certLogicMotorRepository: CertLogicMotorRepository!
    
    override func setUp() {
        super.setUp()
        self.context = mockBDDConfig.getMotorContext()
        try? self.context.save()
        
        certLogicMotorRepository = CertLogicMotorRepository(bddConfig: mockBDDConfig)
    }
    
    // Set Values
    func testDeletePreviousValueSet() {
        let mockData = getMockValueSet()
        
        let context = mockBDDConfig.getMotorContext()
        let certLogicValueSetMotorEntity = CertLogicValueSetMotor(context: context)
        try? certLogicValueSetMotorEntity.initialize(valueSet: mockData.first!)
        
        try! context.save()
        
        XCTAssertNoThrow(try certLogicMotorRepository.deleteValueSets())
        
        let fetchRequest: NSFetchRequest<CertLogicValueSetMotor> = CertLogicValueSetMotor.fetchRequest()
        let result = try! context.fetch(fetchRequest)
        
        XCTAssertNotNil(result)
        XCTAssertTrue(result.count == 0)
    }
    
    func testSave() {
        let mockData = getMockValueSet()
        let setValue = try? certLogicMotorRepository.save(valueSets: mockData)
        
        XCTAssertNotNil(setValue)
        XCTAssertFalse(setValue!.isEmpty)
    }
    
    func testGetSetValue() {
        let mockData = getMockValueSet()
        
        let setValue = try? certLogicMotorRepository.save(valueSets: mockData)
        XCTAssertNotNil(setValue)
        XCTAssertTrue(setValue!.count == 1)
        
        let rulesResult = certLogicMotorRepository.getValueSets()
        XCTAssertFalse(rulesResult.isEmpty)
        XCTAssertTrue(rulesResult.count == 1)
    }
    
    // Rules

    func testSaveReturnRulesMotor() {
        let mockData = getMockRule()
        let rulerMotor = try? certLogicMotorRepository.save(rules: mockData)
        
        XCTAssertNotNil(rulerMotor)
        XCTAssertFalse(rulerMotor!.isEmpty)
    }

    func testDeletePreviousRules() {
        let mockData = getMockRule()
        
        let context = mockBDDConfig.getMotorContext()
        let certRuleEnity = CertLogicRulesMotor(context: context)
        try? certRuleEnity.initialize(rule: mockData.first!)
        
        try! context.save()
        
        XCTAssertNoThrow(try certLogicMotorRepository.deleteRules())
        
        let fetchRequest: NSFetchRequest<CertLogicRulesMotor> = CertLogicRulesMotor.fetchRequest()
        let result = try! context.fetch(fetchRequest)
        
        XCTAssertNotNil(result)
        XCTAssertTrue(result.count == 0)
    }
    
    
    func testGetRules() {
        let mockData = getMockRule()
        
        let certLogicRulesMotor = try? certLogicMotorRepository.save(rules: mockData)
        XCTAssertNotNil(certLogicRulesMotor)
        XCTAssertTrue(certLogicRulesMotor!.count == 1)
        
        let rulesResult = certLogicMotorRepository.getRules()
        XCTAssertFalse(rulesResult.isEmpty)
        XCTAssertTrue(rulesResult.count == 1)
    }
    
    func testGetRulesByCountry() {
        var mockData = getMockRule()
        mockData.append(contentsOf: getMockRule())
        
        mockData.first?.identifier = "xx-PVL-xx"
        
        let certLogicRulesMotor = try? certLogicMotorRepository.save(rules: mockData)
        XCTAssertNotNil(certLogicRulesMotor)
        XCTAssertTrue(certLogicRulesMotor!.count == 2)
        
        let rulesResult = certLogicMotorRepository.getRules(for: "PVL")
        XCTAssertFalse(rulesResult.isEmpty)
        XCTAssertTrue(rulesResult.count == 1)
    }
    
    private func getMockRule() -> [CertLogic.Rule] {
        return [
            CertLogic.Rule(
                identifier: "",
                type: "",
                version: "",
                schemaVersion: "",
                engine: "",
                engineVersion: "",
                certificateType: "",
                description: [],
                validFrom: "",
                validTo: "",
                affectedString: [],
                logic: JSON(parseJSON: "[]"),
                countryCode: ""
            )
        ]
    }
    
    private func getMockValueSet() -> [CertLogic.ValueSet] {
        return [
            CertLogic.ValueSet(valueSetId: "", valueSetDate: "", valueSetValues: [:])
        ]
    }
}
