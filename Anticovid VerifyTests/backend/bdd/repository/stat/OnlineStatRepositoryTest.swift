//
//  OnlineStatRepositoryTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
@testable import Anticovid_Verify

class OnlineStatRepositoryTest: XCTestCase {
    
    let mockBDDConfig = MockBDDConfig()
    
    var context: NSManagedObjectContext!

    var onlineStatRepositoryService: OnlineStatRepository! = nil
    
    override func setUp() {
        super.setUp()
        self.context = mockBDDConfig.getStatContext()
        try? self.context.save()
        
        onlineStatRepositoryService = OnlineStatRepository(bddConfig: mockBDDConfig)
    }
    
    func testDeleteAllStatistics() {
        let onlineStatistic1 = OnlineStatistic(context: context)
        onlineStatistic1.value = "1"
        let onlineStatistic2 = OnlineStatistic(context: context)
        onlineStatistic2.value = "2"

        try? context.save()
        
        let stats = try? context.fetch(OnlineStatistic.fetchRequest())
        
        XCTAssertNotNil(stats)
        XCTAssert(stats!.count == 2)
        
        XCTAssert(onlineStatRepositoryService.deleteAllStatistics())

        let emptyResult = try? context.fetch(OnlineStatistic.fetchRequest())
        XCTAssertNotNil(emptyResult)
        XCTAssert(emptyResult?.count == 0)
    }
    
    func testGetStatistics() {
        let onlineStatistic1 = OnlineStatistic(context: context)
        onlineStatistic1.value = "1"
        let onlineStatistic2 = OnlineStatistic(context: context)
        onlineStatistic2.value = "2"

        try? context.save()

        let stats = onlineStatRepositoryService.getStatistics()
        
        XCTAssertNotNil(stats)
        XCTAssert(stats.count == 2)
    }
    
    func testWriteStatistic() {
        let globalStat = OnlineStatData(globalValidity: .invalid, controlType: "", siren: "", ressourceType: "", msgException: "")
        
        XCTAssert(onlineStatRepositoryService.write(stat: globalStat))
        
        let stats = try? context.fetch(OnlineStatistic.fetchRequest())
        XCTAssertNotNil(stats)
        XCTAssert(stats?.count == 1)
    }
}
