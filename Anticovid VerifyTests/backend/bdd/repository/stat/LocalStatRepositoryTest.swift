//
//  LocalStatRepositoryTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import CoreData
import XCTest
@testable import Anticovid_Verify

class LocalStatRepositoryTest: XCTestCase {

    let mockBDDConfig = MockBDDConfig()
    
    var context: NSManagedObjectContext!

    var localStatRepositoryService: LocalStatRepository!
    
    override func setUp() {
        super.setUp()
        self.context = mockBDDConfig.getStatContext()
        try? self.context.save()
        
        localStatRepositoryService = LocalStatRepository(bddConfig: mockBDDConfig)
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    func testremoveOldData() {
        let localCertStatistic = LocalCertStatistic(context: context)
        localCertStatistic.date = Date().dateByAddingHours(-10)
        localCertStatistic.idHash = "0123456789"
        
        let localCertStatisticNotOld = LocalCertStatistic(context: context)
        localCertStatisticNotOld.date = Date()
        localCertStatisticNotOld.idHash = "0123456789"
        
        try? context.save()
        
        var localCertStatistics = try? context.fetch(LocalCertStatistic.fetchRequest())
        
        XCTAssertNotNil(localCertStatistics)
        XCTAssert(localCertStatistics!.count == 2)
        
        
        localCertStatistics = try? context.fetch(LocalCertStatistic.fetchRequest())
        
        XCTAssertNotNil(localCertStatistics)
        XCTAssert(localCertStatistics!.count == 1)
    }
    
    func testDeleteAllLocalStatistics() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())

        try? context.save()
        
        var localStatisticsResult = try? context.fetch(LocalStatistic.fetchRequest())
        
        XCTAssertNotNil(localStatisticsResult)
        XCTAssert(localStatisticsResult!.count == 1)
        
        XCTAssert(localStatRepositoryService.deleteAllStatistics())

        localStatisticsResult = try? context.fetch(LocalStatistic.fetchRequest())
        
        XCTAssertNotNil(localStatisticsResult)
        XCTAssert(localStatisticsResult!.count == 0)
    }
    
    func testGetStatistics() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())
        
        try? context.save()
        
        let localStatistics = localStatRepositoryService.getStatistics()
        
        XCTAssertNotNil(localStatistics)
        XCTAssert(localStatistics.count == 1)
    }
    
    func testCertificateAlreadyScanReturnLocalCertStatisticIfCertificateAlreadyScan() {
        let localCertStatistic = LocalCertStatistic(context: context)
        localCertStatistic.date = Date()
        localCertStatistic.idHash = "0123456789"
        
        try? context.save()
        
        XCTAssertNotNil(localStatRepositoryService.certificateAlreadyScan(idHash: localCertStatistic.idHash!))
    }

    func testWriteWhenHasNoLocalStatisticForCurrentDay() {
        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "123456789", typeValidation: .valid, certificateAdminState: nil)))
        
        let localStatisticsResult = try? context.fetch(LocalStatistic.fetchRequest())
        
        XCTAssertNotNil(localStatisticsResult)
        XCTAssert(localStatisticsResult!.count == 1)
    }
    
    func testWriteWhenLocalStatisticValForCurrentDayAlreadyExistAndCertValid() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "123456789", typeValidation: .valid, certificateAdminState: nil)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 1)
        XCTAssert(localStatistics.first?.valid == 1)
        XCTAssert(localStatistics.first?.toDecide == 0)
        XCTAssert(localStatistics.first?.invalid == 0)
    }
    
    func testWriteWhenLocalStatisticValForCurrentDayAlreadyExistAndCertInvalid() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "123456789", typeValidation: .invalid, certificateAdminState: nil)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 1)
        XCTAssert(localStatistics.first?.valid == 0)
        XCTAssert(localStatistics.first?.toDecide == 0)
        XCTAssert(localStatistics.first?.invalid == 1)
    }
    
    func testWriteWhenLocalStatisticValForCurrentDayAlreadyExistAndCertToDecide() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "123456789", typeValidation: .to_decide, certificateAdminState: nil)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 1)
        XCTAssert(localStatistics.first?.valid == 0)
        XCTAssert(localStatistics.first?.toDecide == 1)
        XCTAssert(localStatistics.first?.invalid == 0)
    }
    
    func testWriteWhenLocalStatisticForCurrentDayNotExist() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date().dateByAddingDays(-1))
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "123456789", typeValidation: .invalid, certificateAdminState: nil)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 2)
    }
    
    func testWriteWhenLocalStatisticForCurrentDayAlreadyExistAndCertAlreadyExit() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())

        let localCertStatistic = LocalCertStatistic(context: context)
        localCertStatistic.date = Date()
        localCertStatistic.idHash = "0123456789"
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "0123456789", typeValidation: .valid, certificateAdminState: .double)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 1)
        XCTAssert(localStatistics.first?.double == 1)
    }
    
    func testWriteWhenLocalStatisticForCurrentDayAlreadyExistAndCertAlreadyExitAndBlacklist() {
        let localStatistic = LocalStatistic(context: context)
        localStatistic.initialize(date: Date())

        let localCertStatistic = LocalCertStatistic(context: context)
        localCertStatistic.date = Date()
        localCertStatistic.idHash = "0123456789"
        
        try? context.save()

        XCTAssert(localStatRepositoryService.write(stat: LocalStatData(identifiant: "0123456789", typeValidation: .valid, certificateAdminState: .blackListAndDouble)))
        
        let localStatistics = localStatRepositoryService.getStatistics()

        XCTAssert(localStatistics.count == 1)
        XCTAssert(localStatistics.first?.double == 1)
    }
}
