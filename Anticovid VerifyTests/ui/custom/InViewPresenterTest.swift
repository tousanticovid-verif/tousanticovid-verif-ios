//
//  InViewPresenterTest.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import Anticovid_Verify

class InViewPresenterTest: XCTestCase {
    
    var mockSyncManager = MockSyncManager()
    
    var mockInView = MockInView()
    
    var notificationCenter = MockNotificationCenter()
    
    var inViewPresenter: INViewPresenter!
    
    private let host = ConstConf.urlWs?.replacingOccurrences(of: "https://", with: "").replacingOccurrences(of: "/", with: "").replacingOccurrences(of: "/api/", with: "") ?? ""
    
    override func setUp() {
        super.setUp()
        
        inViewPresenter = INViewPresenter(
            mockInView, syncManager: mockSyncManager, notificationCenter: notificationCenter
        )
    }

    // MARK: - Check Need App Update
    func testCheckNeedAppUpdateDoNothingIfResultCodeIsNot200() {
        stub(condition: isHost(host)) { _ in
            return HTTPStubsResponse(data: ("".data(using: .utf8) ?? Data()), statusCode: 300, headers: nil)
        }
        
        inViewPresenter.checkNeedAppSync()
        XCTAssertNil(self.notificationCenter.notificationName)
    }
    
    func testCheckNeedAppUpdateUIIfResultCodeIs200AndMinorVersionIsUperThanCurrentVersionAndMajorVersionIsLowerThanCurrentVersion() {
        stub(condition: isHost(host)) { _ in
            let appConf = AppConf(about: About(iosConfiguration: IosConfiguration(lastMajorVersion: "0", lastMinorVersion: "100000")))
            let json = try? JSONEncoder().encode(appConf)
            return HTTPStubsResponse(data: json ?? Data(), statusCode: 200, headers: nil)
        }
        
        inViewPresenter.checkNeedAppSync()
        XCTAssertTrue(self.notificationCenter.notificationName == ConstNotification.updateApplicationAvailable.rawValue)
    }
    
    func testCheckNeedAppUpdateUIIfResultCodeIs200AndMajorIsUpperThanCurrentVersion() {
        stub(condition: isHost(host)) { _ in
            let appConf = AppConf(about: About(iosConfiguration: IosConfiguration(lastMajorVersion: "100000", lastMinorVersion: "100000")))
            let json = try? JSONEncoder().encode(appConf)
            return HTTPStubsResponse(data: json ?? Data(), statusCode: 200, headers: nil)
        }
        
        inViewPresenter.checkNeedAppSync()
        XCTAssertTrue(self.notificationCenter.notificationName == ConstNotification.updateApplicationAvailable.rawValue)
    }
    
    func testCheckNeedAppUpdateUIIfResultCodeIs200AndMajorIsLowerThanCurrentVersionAndMinorVersionIsLowerThanCurrentVersion() {
        stub(condition: isHost(host)) { _ in
            let appConf = AppConf(about: About(iosConfiguration: IosConfiguration(lastMajorVersion: "0", lastMinorVersion: "0")))
            let json = try? JSONEncoder().encode(appConf)
            return HTTPStubsResponse(data: json ?? Data(), statusCode: 200, headers: nil)
        }
        
        inViewPresenter.checkNeedAppSync()
        XCTAssertNil(self.notificationCenter.notificationName)
    }
    
    // MARK: - Check Need Cert Remind
    
    func testCheckNeedCertRemindReturnLow() {
        mockSyncManager.remingStep = .low
        
        inViewPresenter.checkNeedAppSync()
    
        XCTAssertTrue(notificationCenter.notificationName == ConstNotification.updateBannerSyncApp.rawValue)
    }
    
    func testCheckNeedCertRemindReturnMedium() {
        mockSyncManager.remingStep = .medium
        
        inViewPresenter.checkNeedAppSync()
    
        XCTAssertTrue(notificationCenter.notificationName == ConstNotification.updateBannerSyncApp.rawValue)
    }
    
    func testCheckNeedCertRemindReturnHigh() {
        mockSyncManager.remingStep = .high
        
        inViewPresenter.checkNeedAppSync()
        
        XCTAssertTrue(notificationCenter.notificationName == ConstNotification.updateBannerSyncApp.rawValue)
    }
    
    func testCheckNeedCertRemindReturnNone() {
        mockSyncManager.remingStep = .none
        
        inViewPresenter.checkNeedAppSync()
        
        XCTAssert(mockInView.step == RemindStep.none)
        XCTAssertNil(notificationCenter.notificationName)
    }
}

class MockInView: INViewProtocol {
    
    var step: RemindStep = .none
    func displaySynchronizationAlertIfNeeded(forRemind remindStep: RemindStep, didTapOnBanner: Bool) {
        step = remindStep
    }
}
