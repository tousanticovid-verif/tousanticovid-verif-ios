//
//  ScanViewPresenter.swift
//  Anticovid VerifyTests
//
//

import Foundation
import XCTest
import SwiftJWT
@testable import Anticovid_Verify


class ScanViewPresenterTest: XCTestCase {
    
    var scanViewPresenter: ScanViewPresenter!
    
    var scanView: MockScanView!
    
    var notificationCenter: MockNotificationCenter!
    
    var mockBarcodeVerifier: MockBarcodeVerifier!
    
    var userData: UserDataManager!
    
    var premiumManager: PremiumManager!
    
    override func setUp() {
        super.setUp()
        
        userData = UserDataManager.sharedInstance
        premiumManager = PremiumManager.shared
        mockBarcodeVerifier = MockBarcodeVerifier()
        notificationCenter = MockNotificationCenter()
        scanView = MockScanView()
        
        scanViewPresenter = ScanViewPresenter(
            scanView,
            userDataManager: userData,
            premiumManager: premiumManager,
            barcodeVerifier: mockBarcodeVerifier,
            notificationCenter: notificationCenter
        )
    }
    
    func testProceedToTreatmentWhenIsValidPassCovid2DDocReturnFalseAndIsValidQrCodeReturnFalse() {
        mockBarcodeVerifier.isValidPassCovid2DDoc = false
        mockBarcodeVerifier.isValidPassCovid2DDoc = false
        
        self.scanViewPresenter.proceedToTreatment(with: "", completion: {})
        
        XCTAssertTrue(scanView.time == 3.0)
    }
    
    func testProceedToTreatmentWhenIsValidPassCovid2DDocReturnTrueAndUserdataShouldShowResultTutorialIsTrue() {
        mockBarcodeVerifier.isValidPassCovid2DDoc = true
        userData.shouldShowResultTutorial = true
        
        self.scanViewPresenter.proceedToTreatment(with: "", completion: {})
        
        XCTAssertTrue(scanView.segue == ConstSegue.tutorialResultSegue)
    }
    
    func testProceedToTreatmentWhenIsValidPassCovid2DDocReturnTrueAndUserdataShouldShowResultTutorialIsFalse() {
        mockBarcodeVerifier.isValidPassCovid2DDoc = true
        userData.shouldShowResultTutorial = false
        
        self.scanViewPresenter.proceedToTreatment(with: "", completion: {})
        
        XCTAssertTrue(scanView.segue == ConstSegue.resultSegue)
    }
    
    func testProceedToTreatmentWhenisValidQrCodeReturnObjectAndUserdataShouldShowResultTutorialIsTrue() {
        mockBarcodeVerifier.isValidPassCovid2DDoc = true
        mockBarcodeVerifier.isValidQrCodeToUnlockOTModeResult = nil
        userData.shouldShowResultTutorial = true
        
        self.scanViewPresenter.proceedToTreatment(with: "", completion: {})
        XCTAssertTrue(scanView.segue == ConstSegue.tutorialResultSegue)
    }
    
    
    
    func testProceedToTreatmentWhenisValidQrCodeReturnObjectAndUserdataShouldShowResultTutorialIsFalseAndTokenOtNotEqualBarcode() {
        let jwt = JWT.init(claims:
                            INClaims(
                                iss: "iss",
                                realm_access: [:],
                                exp: Date(),
                                iat: Date(),
                                siren: "siren"
                            )
        )
        mockBarcodeVerifier.isValidPassCovid2DDoc = false
        mockBarcodeVerifier.isValidQrCodeToUnlockOTModeResult = nil
        userData.shouldShowResultTutorial = false
        
        self.scanViewPresenter.proceedToTreatment(with: "", completion: {})
        
        let alertContent = scanView.alertContent
        XCTAssertTrue(alertContent?.0 == "Activation du mode TAC Verif +")
        XCTAssertNotNil(alertContent?.1)
        XCTAssertNotNil(alertContent?.2)

        let uiAlertAction = alertContent?.2
        uiAlertAction?.trigger()

        XCTAssertTrue(self.premiumManager.isPremium)
        XCTAssertTrue(self.userData.shouldShowResultTutorial)
        XCTAssertNotNil(self.premiumManager.expirationDateForOT)
        XCTAssertTrue(self.notificationCenter.notificationName == ConstNotification.navControllerAppareance.rawValue)
    }
    
    func testProceedToTreatmentWhenisValidQrCodeReturnObjectAndUserdataShouldShowResultTutorialIsFalseAndTokenOtEqualBarcode() {
        var unlockConfig: OTModeUnlockConfiguration?
        
        mockBarcodeVerifier.isValidPassCovid2DDoc = false
        mockBarcodeVerifier.isValidQrCodeToUnlockOTModeResult = unlockConfig
        premiumManager.activatePremium(expirationDate: Date(), tokenOT: "AZERTY", siren: "")
        
        self.scanViewPresenter.proceedToTreatment(with: "AZERTY") {
            XCTAssert(true)
        }
    }
    
    func testProceedToTreatmentWhenAllConditionIsFalse() {
        mockBarcodeVerifier.isValidPassCovid2DDoc = false
        mockBarcodeVerifier.isValidQrCodeToUnlockOTModeResult = nil

        self.scanViewPresenter.proceedToTreatment(with: "AZERTY", completion: {})
        
        XCTAssertTrue(scanView.time == 3.0)
    }
    
}

class MockScanView: ScanView {
    func showLoading() {
        
    }
    
    func hideLoading() {
        
    }
    
    func popToHome() {
        
    }
    
    var segue: String?
    func goTo(segueIdentifier: String, data: Any?) {
        self.segue = segueIdentifier
    }
    
    var time: Double?
    func showDocumentTypeView(during time: Double) {
        self.time = time
    }
    
    var alertContent: (String, String, (UIAlertAction))?
    func showAlert(withTitle title: String, withMessage message: String, andAction action: UIAlertAction) {
        self.alertContent = (title, message, action)
    }
}


class MockBarcodeVerifier: BarcodeService {
    
    var isValidPassCovid2DDoc: Bool = false
    override func isValidPassCovid2DDoc(barcode: String) -> Bool {
        return isValidPassCovid2DDoc
    }
    
    var isValidQrCodeToUnlockOTModeResult: OTModeUnlockConfiguration? = nil
    override func isValidQrCodeToUnlockOTMode(qrcode: String) -> OTModeUnlockConfiguration? {
        return isValidQrCodeToUnlockOTModeResult
    }
}
